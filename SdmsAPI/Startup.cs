using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using EltelWebApi.Helpers;
using EltelWebApi.Authorization;
using EltelWebApi.Services;
using EltelWebApi.DataAccess;
using EltelWebApi.Core.BusinessLogic;
using SdmsAPI.Core;
using Microsoft.Extensions.Logging;
using EltelWebApi.Middleware;

namespace SdmsAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers().AddJsonOptions(x => x.JsonSerializerOptions.IgnoreNullValues = true);

            // configure strongly typed settings object
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            // configure DI for application services
            SetDependencyInjectionObjects(services);

            //Json Serializer
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            SetupJWTServices(services);

            services.AddLogging(logBuilder =>
            {
                logBuilder.ClearProviders();
                logBuilder.SetMinimumLevel(LogLevel.Trace);
                logBuilder.AddLog4Net("log4net.config");
                logBuilder.AddConsole();
                
            });

            var dal = new Dal(Configuration.GetConnectionString("DBConnection"));
            var permissionsDt = dal.GetTable(@"select v.id, v.[Name] from dbo.v_permissions v");
            var permissionsList = DalHelper.DataTableToList<PermissionVM>(permissionsDt);

            services.AddAuthorization(options =>
            {
                foreach (var p in permissionsList)
                {
                    options.AddPolicy(p.Name, policy => policy.RequireClaim(p.Name));
                }
                               
            });

        }

        private static void SetDependencyInjectionObjects(IServiceCollection services)
        {
            services.AddScoped<IJwtUtils, JwtUtils>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IApiAuthService, ApiAuthService>();
            services.AddScoped<IDal, Dal>();
            services.AddScoped<ListsDataBLL, ListsDataBLL>();
            services.AddScoped<NotificationBLL, NotificationBLL>();
            services.AddScoped<PermissionsBLL, PermissionsBLL>();
            services.AddScoped<RolesBLL, RolesBLL>();
            services.AddScoped<TypeTablesBLL, TypeTablesBLL>();
            services.AddScoped<UsersBLL, UsersBLL>();
            services.AddScoped<VehiclesBLL, VehiclesBLL>();
            services.AddScoped<WorkStationBLL, WorkStationBLL>();
            services.AddScoped<InspectionBLL, InspectionBLL>();
            services.AddScoped<ApiDeviceEventBLL, ApiDeviceEventBLL>();
            services.AddScoped<AreaBLL, AreaBLL>();
            services.AddScoped<LaneBLL, LaneBLL>();
            services.AddScoped<DeviceBLL, DeviceBLL>();
            services.AddScoped<DriverTagBLL, DriverTagBLL>();
            services.AddScoped<DashboardBLL, DashboardBLL>();
            //services.AddScoped<BaseBLL, BaseBLL>(); 

            services.AddSingleton<IServerSentEventsService>(_ => new ServerSentEventsService());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Global error handler
            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseSSEMiddleware();
            
            app.UseHttpsRedirection();

            app.UseRouting();

            var origins_setting = Configuration.GetValue<string>("AppSettings:Allowed_Origins");
            string[] origins = null;

            if (!string.IsNullOrEmpty(origins_setting))
                origins = origins_setting.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            app.UseCors(options =>
            {
                options.WithOrigins(origins);
                options.AllowAnyMethod();
                options.AllowAnyHeader();
                options.AllowCredentials();

            });

            // Enables HTTPS!! To work as HTTP remove this and change site url to http in Properties > launchSettings.json file

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();


            // custom jwt auth middleware
            app.UseMiddleware<JwtMiddleware>();

            app.UseEndpoints(x => x.MapControllers());

        }

        // For JWT See: https://www.c-sharpcorner.com/article/asp-net-core-web-api-creating-and-validating-jwt-json-web-token/
        private void SetupJWTServices(IServiceCollection services)
        {
            string accessTokenSecert = Configuration.GetSection("AppSettings").GetValue<string>("ACCESS_TOKEN_SECRET");
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(accessTokenSecert));
            var issuer = Configuration.GetSection("AppSettings").GetValue<string>("ACCESS_TOKEN_ISSUER");

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
          .AddJwtBearer(options =>
          {
              options.TokenValidationParameters = new TokenValidationParameters
              {
                  ValidateIssuer = true,
                  ValidateAudience = false,
                  ValidateIssuerSigningKey = true,
                  ValidIssuer = issuer,
                  IssuerSigningKey = key
              };

              options.Events = new JwtBearerEvents
              {
                  OnAuthenticationFailed = context =>
                  {
                      if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                      {
                          context.Response.Headers.Add("Token-Expired", "true");
                      }
                      return Task.CompletedTask;
                  },
                  OnTokenValidated = context =>
                  {
                      return Task.CompletedTask;
                  }
              };
          });
        }
    }

}
