namespace EltelWebApi.Helpers
{
    public class AppSettings
    {
        public int GlobalTopSelect { get; set; }

        public int GlobalSqlTimeout { get; set; }

        // Tokens (random) generated with node command - require('crypto').randomBytes(64).toString('hex'). 
        public string ACCESS_TOKEN_SECRET { get; set; }
        public string REFRESH_TOKEN_SECRET { get; set; }
        public string PASS_ENCRYPTION_SECRET { get; set; }
        // Recommended by internet. See for example: "best-practices-of-using-jwt" (google search)
        public int ACCESS_TOKEN_MINUTS { get; set; }
        public int API_ACCESS_TOKEN_MINUTS { get; set; }
        // Recommended. See for example: https://auth0.com/docs/secure/tokens/refresh-tokens/configure-refresh-token-expiration
        public string ACCESS_TOKEN_ISSUER { get; set; }
        public int REFRESH_TOKEN_DAYS { get; set; }

        // Refresh token time to live (in days), inactive tokens are automatically deleted from the database after this time
        public int RefreshTokenCleanBackDays { get; set; }

        public int Minimal_Qualifier_Value { get; set; }

        public string Allowed_Origins { get; set; }
        public string Allowed_Origin_Notification { get; set; }

        public string BypassSubject { get; set; }
        public string BypassMessage { get; set; }

        public string COANonAllowedLeaveSubject { get; set; }
        public string COANonAllowedLeaveMessage { get; set; }


        public string Nuctech_Target_Path_Prefix { get; set; }
        public string Trucks_Target_Path_Prefix { get; set; }

        public string ERROR_MESSAGE_FORMAT { get; set; }
        /// <summary>
        /// Args - entity, message
        /// </summary>
        public string ERROR_MESSAGE_FORMAT_GET { get; set; }
        /// <summary>
        /// Args - entity, message
        /// </summary>
        public string ERROR_MESSAGE_FORMAT_ADD { get; set; }
        /// <summary>
        /// Args - entity, message
        /// </summary>
        public string ERROR_MESSAGE_FORMAT_UPDATE { get; set; }
        /// <summary>
        /// Args - entity, message
        /// </summary>
        public string ERROR_MESSAGE_FORMAT_DELETE { get; set; }
        /// <summary>
        /// Args - entity, message
        /// </summary>public string ERROR_MESSAGE_FORMAT_DELETE { get; set; }
        public string ERROR_DETAILS_DB { get; set; }


        public string OverrideImportedFiles { get; set; }

        public string TagPrintUrl_Checkin { get; set; }
        public string TagPrintUrl_Checkout { get; set; }
        public string ManifestScanUrl { get; set; }

    }
}