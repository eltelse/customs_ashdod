using EltelWebApi.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace EltelWebApi.Helpers
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddleware> _logger;

        public ErrorHandlerMiddleware(RequestDelegate next, ILogger<ErrorHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            string userGlobalMessage = "Something went wrong";
            var response = context.Response;
            response.ContentType = "application/json";
            dynamic obj = new ExpandoObject();
            obj.title = "Api Exception";
            obj.status = ResultStatuses.Error.ToString();
            var errors = new List<ApiError>();
            ApiError err = null;
            switch (ex)
            {
                case AppException:
                    var e = ex as AppException;
                    response.StatusCode = (int)e.AppExceptionHttpStatusCode;
                    _logger.LogError(e.AppExceptionLogMessage, ex);
                    err = new ApiError(e.AppExceptionCode, userGlobalMessage, e.AppExceptionUserFriendlyDetails) { };
                    errors.Add(err);
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    _logger.LogError(ex?.Message, ex);
                    err = new ApiError(-1, userGlobalMessage, "Unhandled exception") { };
                    errors.Add(err);
                    break;
            }
            obj.errors = errors;
            string json = JsonConvert.SerializeObject(obj);
            await response.WriteAsync(json);
        }
    }
}