using System;
using System.Globalization;
using System.Net;

namespace EltelWebApi.Helpers
{
    /// <summary>
    /// Custom exception class for throwing application specific exceptions that can be caught and handled within the application.
    /// </summary>
    public class AppException : Exception
    {
        public int AppExceptionCode { get; }
        public string AppExceptionLogMessage { get; }
        public HttpStatusCode AppExceptionHttpStatusCode { get; }
        public string AppExceptionUserFriendlyDetails { get; }

        public AppException() : base() { }

        public AppException(string loggerMessage, HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError) : base(loggerMessage)
        {
            AppExceptionHttpStatusCode = httpStatusCode;
            AppExceptionLogMessage = loggerMessage;
            AppExceptionCode = 0;
        }

        public AppException(string loggerMessage, string userFriendlyDetiels, HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError) : base(loggerMessage)
        {
            AppExceptionHttpStatusCode = httpStatusCode;
            AppExceptionLogMessage = loggerMessage;
            AppExceptionUserFriendlyDetails = userFriendlyDetiels;
            AppExceptionCode = 0;
        }


        public AppException(string loggerMessage, int internalCode, HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError) : base(loggerMessage)
        {
            AppExceptionHttpStatusCode = httpStatusCode;
            AppExceptionLogMessage = loggerMessage;
            AppExceptionCode = internalCode;
        }

        public AppException(string loggerMessage, string userFriendlyDetiels, int internalCode, HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError) : base(loggerMessage)
        {
            AppExceptionHttpStatusCode = httpStatusCode;
            AppExceptionLogMessage = loggerMessage;
            AppExceptionUserFriendlyDetails = userFriendlyDetiels;
            AppExceptionCode = internalCode;
        }


        public AppException(string loggerMessage, int internalCode, HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError, params object[] fmtMessageArgs)
            : base(String.Format(CultureInfo.CurrentCulture, loggerMessage, fmtMessageArgs))
        {
            AppExceptionHttpStatusCode = HttpStatusCode.OK;
            AppExceptionLogMessage = String.Format(CultureInfo.CurrentCulture, loggerMessage, fmtMessageArgs);
            AppExceptionCode = internalCode;
        }

        public AppException(string loggerMessage, string userFriendlyDetielsails, int internalCode, HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError, params object[] fmtMessageArgs)
            : base(String.Format(CultureInfo.CurrentCulture, loggerMessage, fmtMessageArgs))
        {
            AppExceptionHttpStatusCode = httpStatusCode;
            AppExceptionLogMessage = String.Format(CultureInfo.CurrentCulture, loggerMessage, fmtMessageArgs);
            AppExceptionUserFriendlyDetails = userFriendlyDetielsails;
            AppExceptionCode = internalCode;
        }
    }
}