using EltelWebApi.Core;

namespace EltelWebApi.Helpers
{
    public class ApiError
    {
        /// <summary>
        /// Custom EltelWebApi error
        /// </summary>
        /// <param name="errorCode">Internal error code</param>
        /// <param name="errorMessage">Message</param>
        /// <param name="errorDetails">Additional details</param>
        public ApiError(int errorCode, string errorMessage, string errorDetails = null)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
            this.ErrorDetails = errorDetails ?? string.Empty;
        }

        /// <summary>
        /// Internal error code
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Additional details
        /// </summary>
        public string ErrorDetails { get; set; }
    }
}
