using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using EltelWebApi.Models.Auth;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using EltelWebApi.Core;
using EltelWebApi.Helpers;
using EltelWebApi.Models;
using Microsoft.Extensions.Configuration;
using EltelWebApi.DataAccess;
using EltelWebApi.Core.BusinessLogic;

namespace EltelWebApi.Authorization
{
    public interface IJwtUtils
    {
        public JwtToken GenerateJwtToken(UserAuthVM userAuthVM);
        public int? ValidateJwtToken(string token);
        public RefreshToken GenerateRefreshToken(string ip);
        public bool HasRoleInToken(string token, string roleName);
    }

    public class JwtUtils : IJwtUtils
    {
        private readonly AppSettings _appSettings;
        private readonly PermissionsBLL _permissionsBll;

        public JwtUtils(IOptions<AppSettings> appSettings, PermissionsBLL permissionsBll)
        {
            _appSettings = appSettings.Value;
            _permissionsBll = permissionsBll;
        }

        public JwtToken GenerateJwtToken(UserAuthVM userAuthVM)
        {
            int id = userAuthVM.Details.Id;
            string accessTokenSecert = _appSettings.ACCESS_TOKEN_SECRET;
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(accessTokenSecert));

            string issuer = _appSettings.ACCESS_TOKEN_ISSUER;

            int minutsToAdd = userAuthVM.UserType == UserType.User ? _appSettings.ACCESS_TOKEN_MINUTS : _appSettings.API_ACCESS_TOKEN_MINUTS;
            var expiration = DateTime.UtcNow.AddMinutes(minutsToAdd);

            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, id.ToString()),
                    // These are preferred
                    //JwtRegisteredClaimNames.NameId
                    //JwtRegisteredClaimNames.Iss
                    // ClaimTypes.Name ==> User.Identity.Name
                }),
                Expires = expiration, // Note that it is not possiable to ignore expiration. It will allways defaut to 60 minuts if no expiration is specified.
                Issuer = issuer,
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            // Roles & Permissions Authorize clains to be used in Autorize attribute
            foreach (var role in userAuthVM.Roles)
            {
                tokenDescriptor.Subject.AddClaim(new Claim(ClaimTypes.Role, role.Name));

            }

            // Note: This approach of adding permissions as claims can cause large token.
            // In the next version it will be replaces with some enum hopfully. Also with granolar permission authorization
            // See this artical: https://www.jerriepelser.com/blog/creating-dynamic-authorization-policies-aspnet-core/
            // Also this: https://www.thereformedprogrammer.net/a-better-way-to-handle-authorization-in-asp-net-core/
            // For now I use the simplest way here that with the use of [Authorize("canEdit")] above the action works out of the box
            
            var permissionsDt = _permissionsBll.GetPermissionNames();
            var policyItems = DalHelper.DataTableToList<PermissionVM>(permissionsDt);

            foreach (var item in userAuthVM.Permissions)
            {
                // Take only what the user has. Will actually be attached to controller CRUD actions.
                if (!policyItems.Any(p => p.Name == item.Name)) continue;

                tokenDescriptor.Subject.AddClaim(new Claim(item.Name, "true"));
            }

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenResult = tokenHandler.WriteToken(token);
            return new JwtToken() { Token = tokenResult, ExpirationSeconds = minutsToAdd * 60 };
        }

        public int? ValidateJwtToken(string token)
        {
            if (token == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.ACCESS_TOKEN_SECRET);
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id" || x.Type == "nameid").Value);

                // return user id from JWT token if validation successful
                return userId;
            }
            catch
            {
                // return null if validation fails
                return null;
            }
        }

        public RefreshToken GenerateRefreshToken(string ip)
        {
            // generate token that is valid for 7 days
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            var refreshToken = new RefreshToken
            {
                Token = Convert.ToBase64String(randomBytes),
                Expires = DateTime.UtcNow.AddDays(_appSettings.REFRESH_TOKEN_DAYS),
                Created = DateTime.UtcNow,
                CreatedByIp = ip
            };

            return refreshToken;
        }

        /// <summary>
        /// Gets claim value from a JWT token.
        /// See: https://dotnetcoretutorials.com/2020/01/15/creating-and-validating-jwt-tokens-in-asp-net-core/
        /// </summary>
        /// <param name="token"></param>
        /// <param name="claimType"></param>
        /// <returns></returns>
        public string GetClaimFromJWTToken(string token, string claimType)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

            var stringClaimValue = securityToken.Claims.First(claim => claim.Type == claimType).Value;
            return stringClaimValue;
        }

        public bool HasRoleInToken(string token, string roleName)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.ReadToken(token) as JwtSecurityToken;
            return securityToken.Claims.Any(claim => claim.Type == "role" && claim.Value == roleName);
        }

        // see: https://code-maze.com/using-refresh-tokens-in-asp-net-core-authentication/
        public ClaimsPrincipal GetPrincipalFromToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.ACCESS_TOKEN_SECRET)),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");
            return principal;
        }


    }
}