﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SdmsAPI.Helpers;
using System;
using System.Linq;

namespace EltelWebApi.Authorization
{

    // See: https://www.craftedforeveryone.com/adding-your-own-custom-authorize-attribute-to-asp-net-core-2-2-and-above/
    // Licensed under the MIT License. See LICENSE file in the project root for full license information.  
    // Also how get a logger here: https://stackoverflow.com/questions/67102996/generic-logging-in-asp-net-core-5-api-attribute

    [AttributeUsage(AttributeTargets.Method)]
    public class AppAuthorizePermission : Attribute, IAuthorizationFilter
    {
        /// <summary>
        /// Comma separated string of permission names
        /// </summary>
        public string Permissions { get; set; } //Permission string to get from controller

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            ILogger<ControllersLogger> logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<ControllersLogger>>();

            if (string.IsNullOrEmpty(Permissions))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            ;
            var permissionNames = Permissions.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string p in permissionNames)
            {
                var permission = context.HttpContext.User.Claims.Where(c => c.Type == p.Trim()).Select(p => p.Type).FirstOrDefault();
                var isAuthorized = !string.IsNullOrEmpty(permission);
                if (isAuthorized)
                    return; //User Authorized. Wihtout setting any result value and just returning is sufficent for authorizing user
            }

            var msg = $"Unauthorized action attempt. Missing permission/s '{Permissions}'.";
            logger.LogError(msg);
            context.Result = new ForbidResult();
            return;
        }
    }
}

/*


// See: https://ignas.me/tech/custom-unauthorized-response-body/
public class AppAuthorizeAttribute : Attribute, IAsyncAuthorizationFilter
{
    public AuthorizationPolicy Policy { get; }

    public AppAuthorizeAttribute(AuthorizationPolicy policy)
    {
        Policy = policy ?? throw new ArgumentNullException(nameof(policy));
    }

    public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
    {
        if (context == null)
        {
            throw new ArgumentNullException(nameof(context));
        }

        // Allow Anonymous skips all authorization
        if (context.Filters.Any(item => item is IAllowAnonymousFilter))
        {
            return;
        }

        var policyEvaluator = context.HttpContext.RequestServices.GetRequiredService<IPolicyEvaluator>();
        var authenticateResult = await policyEvaluator.AuthenticateAsync(Policy, context.HttpContext);
        var authorizeResult = await policyEvaluator.AuthorizeAsync(Policy, authenticateResult, context.HttpContext, context);

        if (authorizeResult.Challenged)
        {
            // Return custom 401 result
            context.Result = new CustomUnauthorizedResult("Authorization failed.");
        }
        else if (authorizeResult.Forbidden)
        {
            // Return default 403 result
            context.Result = new ForbidResult(Policy.AuthenticationSchemes.ToArray());
        }
    }
}

public class CustomUnauthorizedResult : JsonResult
{
    public CustomUnauthorizedResult(string message)
        : base(new CustomError(message))
    {
        StatusCode = StatusCodes.Status401Unauthorized;
    }
}

public class CustomError
{
    public string Error { get; }

    public CustomError(string message)
    {
        Error = message;
    }
}
}
*/