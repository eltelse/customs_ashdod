﻿using Microsoft.Extensions.Configuration;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Core.BusinessLogic
{
    public class TypeTablesBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public TypeTablesBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<TypeTablesBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public DataTable GetTypeTableEntries(string schema, string tableName, bool include_deleted = false)
        {
            SqlParameter[] pArr =
                    {
                new SqlParameter("schema", SqlDbType.VarChar, 20) { Value = schema },
                new SqlParameter("table_name", SqlDbType.VarChar, 50) { Value = tableName },
                new SqlParameter("include_deleted", SqlDbType.Bit) { Value = include_deleted },
            };

            return _dal.GetTable("sp_get_type_table_entries", pArr);

        }

        public string GetTypeTablePagedEntries(PagedDataRequest req, bool include_deleted = false)
        {
            string schema = req.Schema;
            string tableName = req.TableName;
            int pageNumber = req.PageNumber.HasValue && req.PageNumber > 0 ? req.PageNumber.Value : 1;
            int pageSize = req.PageSize.HasValue && req.PageSize >= 5 ? req.PageSize.Value : 10;
            string sortCol = !string.IsNullOrEmpty(req.SortCol) ? req.SortCol : "Id";
            SortType sortType = req.SortType; // The default is desc

            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("schema", SqlDbType.VarChar, 20) { Value = schema });
            dbParams.Add(new SqlParameter("tableName", SqlDbType.VarChar, 50) { Value = tableName });
            dbParams.Add(new SqlParameter("include_deleted", SqlDbType.Bit) { Value = include_deleted });

            dbParams.Add(new SqlParameter("pageNumber", SqlDbType.Int) { Value = pageNumber });
            dbParams.Add(new SqlParameter("pageSize", SqlDbType.Int) { Value = pageSize });
            dbParams.Add(new SqlParameter("sortCol", SqlDbType.VarChar, 100) { Value = sortCol });
            dbParams.Add(new SqlParameter("sortType", SqlDbType.VarChar, 10) { Value = sortType.ToString() });
            dbParams.Add(new SqlParameter("filterTerm", SqlDbType.VarChar, 50) { Value = req.FilterTerm });

            dbParams.Add(new SqlParameter("json", SqlDbType.NVarChar, -1) { Direction = ParameterDirection.Output });
            SqlParameter[] pArr = dbParams.ToArray();

            var resParams = _dal.ExecuteSPGetResalutValues("sp_get_type_table_paged_entries", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
            return json;

            

        }


        public string AddEntry(TypeTable m)
        {
            string tblName = $"{m.Schema}.{m.TableName}";


            SqlParameter[] pArr =
            {
                new SqlParameter("schema", SqlDbType.VarChar, 20) { Value = m.Schema },
                new SqlParameter("table_name", SqlDbType.VarChar, 50) { Value = m.TableName },
                new SqlParameter("name", SqlDbType.VarChar, 100) { Value = m.Name },
                new SqlParameter("description", SqlDbType.VarChar, 250) { Value = m.Description},
            };

            var affectedRows = _dal.Execute("sp_type_entry_add", pArr);

            ValidateCRUDResults(affectedRows, tblName, CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateEntry(TypeTable m)
        {
            string tblName = $"{m.Schema}.{m.TableName}";

            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = m.Id });
            dbParams.Add(new SqlParameter("schema", SqlDbType.VarChar, 20) { Value = m.Schema });
            dbParams.Add(new SqlParameter("table_name", SqlDbType.VarChar, 50) { Value = m.TableName });
            if (!string.IsNullOrEmpty(m.Name)) dbParams.Add(new SqlParameter("name", SqlDbType.VarChar, 100) { Value = m.Name });
            dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = m.Description });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_type_entry_update", pArr);

            ValidateCRUDResults(affectedRows, tblName, CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteEntry(TypeTable m)
        {
            string tblName = $"{m.Schema}.{m.TableName}";

            string sql = $"update {tblName} SET isDeleted = 1 WHERE id=" + m.Id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, tblName, CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }
    }
}
