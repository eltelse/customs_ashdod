﻿using EltelWebApi.DataAccess;
using EltelWebApi.Helpers;
using EltelWebApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SdmsAPI.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace EltelWebApi.Core.BusinessLogic
{
    public class BaseBLL
    {
        private readonly AppSettings _appSettings;
        private readonly IDal _dal;

        public BaseBLL(IOptions<AppSettings> appSettings, IDal dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
        }

        public void ValidateCRUDResults(int affectedRows, string entityTitle, CRUDType crudType)
        {

            if (affectedRows > 0) return;

            string fmt = string.Empty;
            int errCode = 0;

            switch (crudType)
            {
                case CRUDType.CREATE:
                    fmt = _appSettings.ERROR_MESSAGE_FORMAT_ADD;
                    errCode = (int)InternalErrorCodes.DataAddError;
                    break;
                case CRUDType.UPDATE:
                    fmt = _appSettings.ERROR_MESSAGE_FORMAT_UPDATE;
                    errCode = (int)InternalErrorCodes.DataUpdateError;
                    break;
                case CRUDType.DELETE:
                    fmt = _appSettings.ERROR_MESSAGE_FORMAT_DELETE;
                    errCode = (int)InternalErrorCodes.DataDeleteError;
                    break;

            }
            var msg = string.Format(fmt, entityTitle, $"Message: No affected rows.");
            throw new AppException(msg, _appSettings.ERROR_DETAILS_DB, errCode, HttpStatusCode.BadRequest);
        }

        public DataTable GetDisplayColumns(string entityName)
        {
            string sql = $"SELECT * from fn_GetTableColumns('{entityName}')";
            return _dal.GetTable(sql);
        }

        public DataTable GetEntities(string entityName, string sortCol = "Id", SortType sortType = SortType.DESC)
        {
            var top = _appSettings.GlobalTopSelect.ToString();
            string sql = $"select top {top} * from dbo.v_{entityName}s ORDER BY {sortCol} {sortType}";
            return _dal.GetTable(sql);
        }

        public DataTable GetEntitiesById(string entityName, string Id)
        {
            string sql = $"select * from dbo.v_{entityName}s  WHERE [id] = {Id}";
            return _dal.GetTable(sql);
        }

        public string GetEntitiesPagedData(string entityName, PagedDataRequest req)
        {
            int pageNumber = req.PageNumber.HasValue && req.PageNumber > 0 ? req.PageNumber.Value : 1;
            int pageSize = req.PageSize.HasValue && req.PageSize >= 5 ? req.PageSize.Value : 10;
            string sortCol = !string.IsNullOrEmpty(req.SortCol) ? req.SortCol : "Id";
            SortType sortType = req.SortType; // The default is desc

            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("tableName", SqlDbType.VarChar, 100) { Value = entityName });
            dbParams.Add(new SqlParameter("isAdmin", SqlDbType.Bit) { Value = req.IsAdmin });
            dbParams.Add(new SqlParameter("pageNumber", SqlDbType.Int) { Value = pageNumber });
            dbParams.Add(new SqlParameter("pageSize", SqlDbType.Int) { Value = pageSize });
            dbParams.Add(new SqlParameter("sortCol", SqlDbType.VarChar, 100) { Value = sortCol });
            dbParams.Add(new SqlParameter("sortType", SqlDbType.VarChar, 10) { Value = sortType.ToString() });
            dbParams.Add(new SqlParameter("filterTerm", SqlDbType.VarChar, 50) { Value = req.FilterTerm });

            dbParams.Add(new SqlParameter("json", SqlDbType.NVarChar, -1) { Direction = ParameterDirection.Output });
            SqlParameter[] pArr = dbParams.ToArray();
            var resParams = _dal.ExecuteSPGetResalutValues("sp_all_obj_paged_get", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
            return json;
        }

        /// <summary>
        /// This method is used to post request to Trucks peripheral services like primter service (check-in / checkout driver tag and manifest scan)
        /// </summary>
        /// <param name="json"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public string PostWorkflowsRequests(string json, string url)
        {
            string res = ResultStatuses.Success.ToString();

            try
            {
                using (var client = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(HttpMethod.Post, url))
                    {
                        var content = new StringContent(json);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        var response = client.PostAsync(url, content).Result;

                        response.EnsureSuccessStatusCode();

                        res = response.Content.ReadAsStringAsync().Result;
                    }
                }

                if (res != ResultStatuses.Success.ToString())
                    throw new AppException($"Error while trying to post to a peripheral service. URL: {url}" + res, "Error while trying to post to a peripheral service");
            }
            catch (Exception ex)
            {
                string freindlyMessage = "Error while trying to post to a peripheral service";
                string message = $"{freindlyMessage}. Url: {url}. Message: {ex.Message}.";
                if (ex.InnerException != null) message += $" {ex.InnerException.Message}.";
                throw new AppException(message, freindlyMessage);
            }

            return res;
        }
    }
}
