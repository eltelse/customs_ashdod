﻿using EltelWebApi.DataAccess;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Core.BusinessLogic
{
    public class DriverTagBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public DriverTagBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<DriverTagBLL> logger)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public string GetDriverTagPopupData(string tagCode)
        {
            SqlParameter[] pArr =
                 {
                    new SqlParameter("tagCode", SqlDbType.VarChar, 20) { Value = tagCode },
                    new SqlParameter("json", SqlDbType.VarChar, -1) { Direction = ParameterDirection.Output } // -1 is max
                 };
            var resParams = _dal.ExecuteSPGetResalutValues("sp_get_drievr_tag_data_json_object", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
            return json;

        }
    }
}
