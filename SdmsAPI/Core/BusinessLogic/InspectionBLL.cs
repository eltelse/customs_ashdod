﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using EltelWebApi.Controllers;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using EltelWebApi.Helpers;
using SdmsAPI.Models.ViewModels;
using EltelWebApi.Services;
using EltelWebApi.Authorization;
using Microsoft.Extensions.Logging;
using System.Net;
using SdmsAPI.Models;

namespace EltelWebApi.Core.BusinessLogic
{
    public class InspectionBLL : BaseBLL
    {
        private readonly IDal _dal;
        private readonly AppSettings _appSettings;
        private readonly NotificationBLL _notificationsBll;
        private readonly RolesBLL _rolesBll;
        private IServerSentEventsService _sseSvc;
        private IJwtUtils _jwtUtils;
        ILogger _logger;

        public InspectionBLL(IOptions<AppSettings> appSettings, IDal dal, NotificationBLL notificationBll, IServerSentEventsService sseSvc, IJwtUtils jwtUtils, RolesBLL rolesBLL, ILogger<InspectionBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _notificationsBll = notificationBll;
            _sseSvc = sseSvc;
            _jwtUtils = jwtUtils;
            _rolesBll = rolesBLL;
            _logger = logger;
        }

        public DataTable GetInspectionEventLog()
        {
            string sql = "select * from dbo.InspectionEventLog";
            return _dal.GetTable(sql);
        }

        public string GetCheckinDataById(string id)
        {
            SqlParameter[] pArr =
                       {
                        new SqlParameter("id", SqlDbType.Int) { Value = id },
                        new SqlParameter("json", SqlDbType.VarChar, -1) { Direction = ParameterDirection.Output } // -1 is max
                    };
            var resParams = _dal.ExecuteSPGetResalutValues("sp_get_checkin_data_json_object", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
            return json;
        }

        public string GetCheckinManifestDataById(string id)
        {
            SqlParameter[] pArr =
                       {
                        new SqlParameter("id", SqlDbType.Int) { Value = id },
                        new SqlParameter("json", SqlDbType.VarChar, -1) { Direction = ParameterDirection.Output } // -1 is max
                    };
            var resParams = _dal.ExecuteSPGetResalutValues("sp_get_checkin_manifest_data_json_object", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
            return json;
        }

        public DataTable GetInspectionTypes()
        {
            string sql = @"select * from dbo.v_inspection_types";
            return _dal.GetTable(sql);
        }

        public string AskForBypass(string inspectionId, String token)
        {
            try
            {
                string sql = "select * from dbo.v_inspections_raw WHERE id=" + inspectionId;
                var tbl = _dal.GetTable(sql);

                if (tbl.Rows.Count == 0)
                {
                    string msg = $"Failure. Message: InspectionId {inspectionId} was not found in the database.";
                    throw new AppException(msg, _appSettings.ERROR_DETAILS_DB, (int)InternalErrorCodes.DataGetError, HttpStatusCode.InternalServerError);
                }

                var insp = tbl.Rows[0].DataRowToObject<Inspection>();
                int? srcUserId = _jwtUtils.ValidateJwtToken(token);
                int uid = srcUserId.HasValue ? srcUserId.Value : 0;
                Notification n = PrepareBypassNotification(insp, uid);

                if (n.TargetId == default)
                    return "Error while Asking for bypass for site managers. Could not assign a role id to publish to.";

                _notificationsBll.AddNotification(n);

            }
            catch (Exception ex)
            {
                throw new AppException("Something went wrong while asking for bypass. {0}.", _appSettings.ERROR_DETAILS_DB, (int)InternalErrorCodes.DataGetError, HttpStatusCode.InternalServerError, $"Message: {ex.Message}");
            }

            return ResultStatuses.Success.ToString();
        }

        public string AskForManifestScan(string identifier)
        {
            dynamic data = new ExpandoObject();
            data.InspectionIdentifier = identifier;
            string json = JsonConvert.SerializeObject(data);

            return PostWorkflowsRequests(json, _appSettings.ManifestScanUrl);
        }

        public string AddInspection(Inspection i)
        {
            SqlParameter[] pArr =
            {
                    new SqlParameter("vehicleId", SqlDbType.Int) { Value = i.VehicleId },
                    new SqlParameter("inspectionTypeId", SqlDbType.Int) { Value = i.WorkflowTypeId },
                    new SqlParameter("lpr", SqlDbType.VarChar, 20) { Value = i.LPR},
                    new SqlParameter("lprCorrectionTypeId", SqlDbType.Int) { Value = i.LPRCorrectionTypeId },
                    new SqlParameter("ccr", SqlDbType.VarChar, 20) { Value = i.CCR},
                    new SqlParameter("ccrCorrectionTypeId", SqlDbType.Int) { Value = i.CCRCorrectionTypeId },
                    new SqlParameter("manifestCode", SqlDbType.VarChar, 20) { Value = i.ManifestCode},
                    new SqlParameter("driverTagCode", SqlDbType.VarChar, 20) { Value = i.DriverTagCode},
                    new SqlParameter("remark", SqlDbType.VarChar, 300) { Value = i.Remark},
                };

            var affectedRows = _dal.Execute("sp_inspection_add", pArr);

            ValidateCRUDResults(affectedRows, "inspection", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateInspection(Inspection i)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = i.Id });

            if (i.WorkflowTypeId != default(int)) dbParams.Add(new SqlParameter("inspectionTypeId", SqlDbType.Int) { Value = i.WorkflowTypeId });
            if (!string.IsNullOrEmpty(i.LPR)) dbParams.Add(new SqlParameter("lpr", SqlDbType.VarChar, 20) { Value = i.LPR });
            if (i.LPRCorrectionTypeId != default(int)) dbParams.Add(new SqlParameter("lprCorrectionTypeId", SqlDbType.Int) { Value = i.LPRCorrectionTypeId });
            if (!string.IsNullOrEmpty(i.CCR)) dbParams.Add(new SqlParameter("ccr", SqlDbType.VarChar, 20) { Value = i.CCR });
            if (i.CCRCorrectionTypeId != default(int)) dbParams.Add(new SqlParameter("ccrCorrectionTypeId", SqlDbType.Int) { Value = i.CCRCorrectionTypeId });
            if (!string.IsNullOrEmpty(i.ManifestCode)) dbParams.Add(new SqlParameter("manifestCode", SqlDbType.VarChar, 20) { Value = i.ManifestCode });
            if (!string.IsNullOrEmpty(i.DriverTagCode)) dbParams.Add(new SqlParameter("driverTagCode", SqlDbType.VarChar, 20) { Value = i.DriverTagCode });
            if (i.ConclusionTypeId != default(int)) dbParams.Add(new SqlParameter("conclusionTypeId", SqlDbType.Int) { Value = i.ConclusionTypeId });
            dbParams.Add(new SqlParameter("remark", SqlDbType.VarChar, 300) { Value = i.Remark });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_inspection_update", pArr);

            ValidateCRUDResults(affectedRows, "inspection", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string StartInspection(CheckinVM vm)
        {
            SqlParameter[] pArr =
            {
                    new SqlParameter("json", SqlDbType.VarChar, -1) { Value = JsonConvert.SerializeObject(vm) }, // -1 is MAX
                    new SqlParameter("shouldCreateVehicle", SqlDbType.Bit) { Value = true },
                };

            var resTbl = _dal.GetTable("sp_inspection_update_start", pArr);

            int affectedRows = resTbl != null ? resTbl.Rows.Count : 0;

            ValidateCRUDResults(affectedRows, "start inspection", CRUDType.UPDATE);

            var resMsg = resTbl.Rows[0]["Message"].ToString();

            if (resMsg != "Success")
                throw new AppException(resMsg, _appSettings.ERROR_DETAILS_DB, (int)InternalErrorCodes.StartInspectionError, HttpStatusCode.InternalServerError);

            PrintCheckinTag(vm.id);

            NotificationVM n = new();
            n.NotificationTypeId = (int)NotificationType.Inspections;
            n.Subject = "Inspections table refresh data";
            _sseSvc.SendEventAsync(n);

            n.NotificationTypeId = (int)NotificationType.Dashboard;
            n.Subject = "Dashboard table refresh data";
            _sseSvc.SendEventAsync(n);

            // TODO: POST TO SDMS

            return ResultStatuses.Success.ToString();
        }

        private string PrintCheckinTag(int inspectionId)
        {
            string sql = $"SELECT * FROM v_inspections_Checkin_Tag_Data WHERE id = {inspectionId}";
            var dt = _dal.GetTable(sql);
            if (dt.Rows.Count == 0) throw new AppException("Error printing dtiver tag at checkin start process", $"Did not finf inspection checkin tag data for inspection id {inspectionId}");

            var insp = DalHelper.DataRowToObject<CheckinTagPrintVm>(dt.Rows[0]);
            dynamic data = new ExpandoObject();
            data.Barcode = insp.DriverTagCode;
            data.Lines = new List<TagPrintObjItem>()
            {
                new TagPrintObjItem(){ Title = "מספר בדיקה", Value = insp.Identifier },
                new TagPrintObjItem(){ Title = "מספר רישוי", Value = insp.LPR },
                new TagPrintObjItem(){ Title = "מספר מכולה", Value = insp.CCR },
                new TagPrintObjItem(){ Title = "יעד", Value = insp.NextAreaDescription, IsValueHeb = true },
                new TagPrintObjItem(){ Title = "סוג בדיקה", Value = insp.WorkflowType },
                new TagPrintObjItem(){ Title = "מספר תור", Value = insp.QueueNumber, IsValueBold = true },
                new TagPrintObjItem(){ Title = "זמן כניסה", Value = insp.Arrival }
            };

            string json = JsonConvert.SerializeObject(data);

            return PostWorkflowsRequests(json, _appSettings.TagPrintUrl_Checkin);
        }

        public string DeleteInspection(string id)
        {
            string sql = "update dbo.Inspection SET isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "inspection", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }

        private Notification PrepareBypassNotification(Inspection insp, int sourceUserId)
        {
            Notification n = new();
            n.NotificationTypeId = (int)NotificationType.Bypass;
            n.NotificationPublishTypeId = (int)NotificationPublishType.Role;
            n.Subject = $"{_appSettings.BypassSubject} for LPR: {insp.LPR}";
            n.Message = _appSettings.BypassMessage;
            n.SourceUserId = sourceUserId;
            var role = _rolesBll.GetSiteManagerRole();
            n.TargetId = role != null ? role.Id : 6; // 6 should be SiteManager
            n.IsReplyable = true;

            var sb = new StringBuilder();
            sb.Append("{ ");
            sb.Append($"\"LPR\": \"{insp.LPR}\", ");
            sb.Append($"\"CCR\": \"{insp.CCR}\", ");
            string arrival = insp.Arrival.ToString("yyyy-MM-dd HH:mm:ss:fff");
            sb.Append($"\"Arrival\": \"{arrival}\", ");
            sb.Append($"\"InspectionId\": \"{insp.Id}\" ");
            sb.Append(" }");
            n.JsonData = sb.ToString();


            return n;
        }
    }
}
