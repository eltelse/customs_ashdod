﻿using EltelWebApi.DataAccess;
using EltelWebApi.Helpers;
using EltelWebApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;

namespace EltelWebApi.Core.BusinessLogic
{
    public class DeviceBLL : BaseBLL
    {
        private readonly IDal _dal;
        private readonly ILogger _logger;
        private readonly AppSettings _appSettings;

        public DeviceBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<DeviceBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public DataTable GetDeviceByIdentifier(string identifier)
        {
            SqlParameter[] pArr = { new SqlParameter("identifier", SqlDbType.VarChar, 20) { Value = identifier } };
            return _dal.GetTable("sp_get_deviceByIdentifier", pArr);
        }

        public string AddDevice(Device obj)
        {

            SqlParameter[] pArr =
            {
                    new SqlParameter("name", SqlDbType.VarChar, 100) { Value = obj.Name},
                    new SqlParameter("description", SqlDbType.VarChar, 250) { Value = obj.Description },
                    new SqlParameter("identifier", SqlDbType.VarChar, 20) { Value = obj.Identifier },
                    new SqlParameter("hostIp", SqlDbType.VarChar, 20) { Value = obj.HostIp },
                    new SqlParameter("deviceIp", SqlDbType.VarChar, 50) { Value = obj.DeviceIp},
                    new SqlParameter("directoryPattern_source", SqlDbType.VarChar, 250) { Value = obj.DirectoryPatternSource },
                    new SqlParameter("directoryPattern_target", SqlDbType.VarChar, 250) { Value = obj.DirectoryPatternTarget },
                    new SqlParameter("laneId", SqlDbType.Int) { Value = obj.LaneId},
                    new SqlParameter("deviceTypeId", SqlDbType.Int) { Value = obj.DeviceTypeId},
                };

            var affectedRows = _dal.Execute("sp_device_add", pArr);

            ValidateCRUDResults(affectedRows, "device", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateDevice(Device obj)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = obj.Id });

            if (!string.IsNullOrEmpty(obj.Name)) dbParams.Add(new SqlParameter("name", SqlDbType.VarChar, 100) { Value = obj.Name });
            dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = obj.Description });
            if (!string.IsNullOrEmpty(obj.Identifier)) dbParams.Add(new SqlParameter("Identifier", SqlDbType.VarChar, 20) { Value = obj.Identifier });
            if (!string.IsNullOrEmpty(obj.HostIp)) dbParams.Add(new SqlParameter("hostIp", SqlDbType.VarChar, 20) { Value = obj.HostIp });
            if (!string.IsNullOrEmpty(obj.DeviceIp)) dbParams.Add(new SqlParameter("deviceIp", SqlDbType.VarChar, 20) { Value = obj.DeviceIp });
            if (!string.IsNullOrEmpty(obj.DirectoryPatternSource)) dbParams.Add(new SqlParameter("directoryPattern_source", SqlDbType.VarChar, 250) { Value = obj.DirectoryPatternSource });
            if (!string.IsNullOrEmpty(obj.DirectoryPatternTarget)) dbParams.Add(new SqlParameter("directoryPattern_target", SqlDbType.VarChar, 250) { Value = obj.DirectoryPatternTarget });
            if (obj.LaneId > -1) dbParams.Add(new SqlParameter("laneId", SqlDbType.Int) { Value = obj.LaneId });
            if (obj.DeviceTypeId > -1) dbParams.Add(new SqlParameter("deviceTypeId", SqlDbType.Int) { Value = obj.DeviceTypeId });
            if (obj.IsActive.HasValue) dbParams.Add(new SqlParameter("isActive", SqlDbType.Bit) { Value = obj.IsActive.Value });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_device_update", pArr);

            ValidateCRUDResults(affectedRows, "device", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteDevice(string id)
        {
            string sql = "update dbo.device set isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "device", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }
    }
}
