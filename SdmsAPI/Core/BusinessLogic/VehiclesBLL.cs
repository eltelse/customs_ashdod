﻿using Microsoft.Extensions.Configuration;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;

namespace EltelWebApi.Core.BusinessLogic
{
    public class VehiclesBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public VehiclesBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<VehiclesBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public string AddVehicle(Vehicle v)
        {
            SqlParameter[] pArr =
            {
                new SqlParameter("licensePlateNumber", SqlDbType.VarChar, 20) { Value = v.LicensePlateNumber},
                new SqlParameter("vehicleTypeId", SqlDbType.Int) { Value = v.VehicleTypeId},
                new SqlParameter("remark", SqlDbType.VarChar, 250) { Value = v.Remark },

            };

            var affectedRows = _dal.Execute("sp_vehicle_add", pArr);

            ValidateCRUDResults(affectedRows, "vehicle", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateVehicle(Vehicle v)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = v.Id });

            if (!string.IsNullOrEmpty(v.LicensePlateNumber)) dbParams.Add(new SqlParameter("licensePlateNumber", SqlDbType.VarChar, 20) { Value = v.LicensePlateNumber });
            if (v.VehicleTypeId != default(int)) dbParams.Add(new SqlParameter("vehicleTypeId", SqlDbType.Int) { Value = v.VehicleTypeId });
            dbParams.Add(new SqlParameter("remark", SqlDbType.VarChar, 250) { Value = v.Remark });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_vehicle_update", pArr);

            ValidateCRUDResults(affectedRows, "vehicle", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteVehicle(string id)
        {
            string sql = "update dbo.Vehicle set isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "vehicle", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }
    }
}
