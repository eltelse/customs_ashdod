﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Net;

namespace EltelWebApi.Core.BusinessLogic
{
    public class RolesBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public RolesBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<RolesBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public RoleVM GetRoleByName(string name)
        {
            string sql = $"select * from dbo.v_roles v where v.[Name] = '{name}'";
            var tbl = _dal.GetTable(sql);
            if (tbl.Rows.Count > 0)
                return DalHelper.DataRowToObject<RoleVM>(tbl.Rows[0]);

            return new RoleVM();
        }

        public RoleVM GetSiteManagerRole()
        {
            string sql = $"SELECT * FROM fn_GetSiteManagerRole()";
            var tbl = _dal.GetTable(sql);
            if (tbl.Rows.Count > 0)
                return DalHelper.DataRowToObject<RoleVM>(tbl.Rows[0]);

            return new RoleVM();
        }

        public string AddRole(Role r)
        {
            SqlParameter[] pArr =
            {
                    new SqlParameter("name", SqlDbType.VarChar, 100) { Value = r.Name},
                    new SqlParameter("description", SqlDbType.VarChar, 250) { Value = r.Description }
            };

            var affectedRows = _dal.Execute("sp_role_add", pArr);

            ValidateCRUDResults(affectedRows, "role", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateRole(Role r)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = r.Id });

            if (!string.IsNullOrEmpty(r.Name)) dbParams.Add(new SqlParameter("name", SqlDbType.VarChar, 100) { Value = r.Name });
            dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = r.Description });


            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_role_update", pArr);

            ValidateCRUDResults(affectedRows, "role", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteRole(string id)
        {
            string sql = "update [auth].[role] Set isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "role", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }

        public string GetRolePermissions(int id)
        {
            try
            {
                SqlParameter[] pArr =
                          {
                        new SqlParameter("role_id", SqlDbType.Int) { Value = id },
                        new SqlParameter("json", SqlDbType.VarChar, -1) { Direction = ParameterDirection.Output } // -1 is max
                    };
                var resParams = _dal.ExecuteSPGetResalutValues("sp_get_role_permissions_data_json_object", pArr, ParameterDirection.Output);
                string json = resParams[0].Value.ToString();
                return json;
            }
            catch (Exception ex)
            {
                throw new AppException(_appSettings.ERROR_MESSAGE_FORMAT_GET, _appSettings.ERROR_DETAILS_DB, (int)InternalErrorCodes.DataGetError, HttpStatusCode.InternalServerError, $"role permissions", $"Message: {ex.Message}");
            }
        }

        public string UpdateRolePermissions(RolePermissionVM vm)
        {
            try
            {
                SqlParameter[] pArr =
                {
                        new SqlParameter("json", SqlDbType.VarChar) {  Value = JsonConvert.SerializeObject(vm) },
                };

                var resTbl = _dal.GetTable("sp_update_role_permissions_data_by_json_object", pArr);

                if (resTbl.Rows.Count < 1 || resTbl.Rows[0]["Message"].ToString() != "Success")
                {
                    return "Failure. Message: Something went wrong while update permissions.";

                }

                return ResultStatuses.Success.ToString();
            }
            catch (Exception ex)
            {

            }
            return ResultStatuses.Success.ToString();
        }
    }
}
