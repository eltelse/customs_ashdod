﻿using EltelWebApi.DataAccess;
using EltelWebApi.Helpers;
using EltelWebApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;

namespace EltelWebApi.Core.BusinessLogic
{
    public class LaneBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public LaneBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<LaneBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }
        
        public string AddLane(Lane obj)
        {
            SqlParameter[] pArr =
            {
                    new SqlParameter("name", SqlDbType.VarChar, 100) { Value = obj.Name},
                    new SqlParameter("description", SqlDbType.VarChar, 250) { Value = obj.Description },
                    new SqlParameter("maxQueueQuantity", SqlDbType.Int) { Value = obj.MaxQueueQuantity},
                    new SqlParameter("areaId", SqlDbType.Int) { Value = obj.AreaId},

             };

            var affectedRows = _dal.Execute("sp_lane_add", pArr);

            ValidateCRUDResults(affectedRows, "lane", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateLane(Lane obj)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = obj.Id });

            if (!string.IsNullOrEmpty(obj.Name)) dbParams.Add(new SqlParameter("name", SqlDbType.VarChar, 100) { Value = obj.Name });
            dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = obj.Description });
            if (obj.MaxQueueQuantity != default(int)) dbParams.Add(new SqlParameter("maxQueueQuantity", SqlDbType.Int) { Value = obj.MaxQueueQuantity });
            if (obj.AreaId > -1) dbParams.Add(new SqlParameter("areaId", SqlDbType.Int) { Value = obj.AreaId });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_lane_update", pArr);

            ValidateCRUDResults(affectedRows, "lane", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteLane(string id)
        {
            string sql = "update dbo.lane set isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "lane", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }
    }
}
