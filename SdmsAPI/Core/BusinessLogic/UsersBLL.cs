﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Net;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Core.BusinessLogic
{
    public class UsersBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public UsersBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<UsersBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public DataTable GetUsers(bool isDevAdmin = false)
        {
            string sql = @"select * from dbo.v_users";
            if (!isDevAdmin)
                sql += " where TypeId = 1"; // Only regular users

            return _dal.GetTable(sql);
        }

        public DataTable GetUserByEmail(string email)
        {
            string sql = "select * from dbo.v_users WHERE email=''  " + email + "''";
            return _dal.GetTable(sql);
        }

        public string AddUser(User u)
        {
            SqlParameter[] pArr =
               {
                    new SqlParameter("firstName", SqlDbType.VarChar, 50) { Value = u.FirstName},
                    new SqlParameter("lastName", SqlDbType.VarChar, 50) { Value = u.LastName },
                    new SqlParameter("email", SqlDbType.VarChar, 250) { Value = u.Email},
                    new SqlParameter("password", SqlDbType.VarChar, 100) { Value = BCrypt.Net.BCrypt.HashPassword(u.Password) },
                    new SqlParameter("description", SqlDbType.VarChar, 250) { Value = u.Email},
                    new SqlParameter("type_id", SqlDbType.Int) { Value = u.TypeId},
                    new SqlParameter("role_id", SqlDbType.Int) { Value = u.RoleId},
                };

            var affectedRows = _dal.Execute("sp_user_add", pArr);

            ValidateCRUDResults(affectedRows, "user", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateUser(User u)
        {

            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = u.Id });

            if (!string.IsNullOrEmpty(u.FirstName)) dbParams.Add(new SqlParameter("firstName", SqlDbType.VarChar, 50) { Value = u.FirstName });
            if (!string.IsNullOrEmpty(u.LastName)) dbParams.Add(new SqlParameter("lastName", SqlDbType.VarChar, 50) { Value = u.LastName });
            if (!string.IsNullOrEmpty(u.Email)) dbParams.Add(new SqlParameter("email", SqlDbType.VarChar, 250) { Value = u.Email });
            if (!string.IsNullOrEmpty(u.Password)) dbParams.Add(new SqlParameter("password", SqlDbType.VarChar, 100) { Value = BCrypt.Net.BCrypt.HashPassword(u.Password) });
            if (!string.IsNullOrEmpty(u.Description)) dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = u.Description });
            if (u.TypeId != default) dbParams.Add(new SqlParameter("type_id", SqlDbType.Int) { Value = u.TypeId });
            if (u.RoleId != default) dbParams.Add(new SqlParameter("role_id", SqlDbType.Int) { Value = u.RoleId });
            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_user_update", pArr);

            ValidateCRUDResults(affectedRows, "user", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteUser(string id)
        {
            string sql = "update [auth].[user] Set isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "user", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }

    }
}
