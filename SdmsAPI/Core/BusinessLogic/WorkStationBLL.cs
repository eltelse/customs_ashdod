﻿using Microsoft.Extensions.Configuration;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;

namespace EltelWebApi.Core.BusinessLogic
{
    public class WorkStationBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public WorkStationBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<WorkStationBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public DataTable GetWorkStationByIp(string ip)
        {
            string sql = "select * from dbo.v_work_stations WHERE [IP]=''" + ip + "''";
            try
            {
                return _dal.GetTable(sql);
            }
            catch (Exception ex)
            {
                var msg = string.Format(_appSettings.ERROR_MESSAGE_FORMAT, "getting", "a", $"workstation by ip: {ip}", $"Message: {ex.Message}");
                _logger.LogError(msg);
                return new DataTable();
            }
        }

        public string AddWorkStation(WorkStation u)
        {
            SqlParameter[] pArr =
            {
                new SqlParameter("Name", SqlDbType.VarChar, 100) { Value = u.Name },
                new SqlParameter("Description", SqlDbType.VarChar, 250) { Value = u.Description },
                new SqlParameter("IP", SqlDbType.VarChar, 20) { Value = u.IP},
                new SqlParameter("MAC", SqlDbType.VarChar, 20) { Value = u.MAC},
                new SqlParameter("WorkStationTypeId", SqlDbType.Int) { Value = u.WorkStationTypeId },
            };

            var affectedRows = _dal.Execute("sp_work_station_add", pArr);

            ValidateCRUDResults(affectedRows, "work_station", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateWorkStation(WorkStation u)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = u.Id });

            if (!string.IsNullOrEmpty(u.Name)) dbParams.Add(new SqlParameter("Name", SqlDbType.VarChar, 100) { Value = u.Name });
            dbParams.Add(new SqlParameter("Description", SqlDbType.VarChar, 250) { Value = u.Description });
            dbParams.Add(new SqlParameter("IP", SqlDbType.VarChar, 20) { Value = u.IP });
            dbParams.Add(new SqlParameter("MAC", SqlDbType.VarChar, 20) { Value = u.MAC });
            if (u.WorkStationTypeId != default(int)) dbParams.Add(new SqlParameter("workStationTypeId", SqlDbType.Int) { Value = u.WorkStationTypeId });
            if (u.IsActive.HasValue) dbParams.Add(new SqlParameter("isActive", SqlDbType.Bit) { Value = u.IsActive.Value });
            if (u.IsDeleted.HasValue) dbParams.Add(new SqlParameter("isDeleted", SqlDbType.Bit) { Value = u.IsDeleted.Value });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_work_station_update", pArr);

            ValidateCRUDResults(affectedRows, "work_station", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteWorkStation(string id)
        {
            string sql = "update dbo.WorkStation set isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "work_station", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }

    }
}
