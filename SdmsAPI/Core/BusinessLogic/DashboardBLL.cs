﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using EltelWebApi.Controllers;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using EltelWebApi.Helpers;
using SdmsAPI.Models.ViewModels;
using EltelWebApi.Services;
using EltelWebApi.Authorization;
using Microsoft.Extensions.Logging;

namespace EltelWebApi.Core.BusinessLogic
{
    public class DashboardBLL : BaseBLL
    {
        private readonly IDal _dal;
        private readonly AppSettings _appSettings;
        private readonly NotificationBLL _notificationsBll;
        private IServerSentEventsService _sseSvc;
        private IJwtUtils _jwtUtils;
        ILogger _logger;


        public DashboardBLL(IOptions<AppSettings> appSettings, IDal dal, NotificationBLL notificationBll, IServerSentEventsService sseSvc, IJwtUtils jwtUtils, ILogger<DashboardBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _notificationsBll = notificationBll;
            _sseSvc = sseSvc;
            _jwtUtils = jwtUtils;
            _logger = logger;
        }

        public string GetDashboardData()
        {
            SqlParameter[] pArr =
                   {
                        new SqlParameter("json", SqlDbType.NVarChar, -1) { Direction = ParameterDirection.Output } // -1 is max
                    };
            var resParams = _dal.ExecuteSPGetResalutValues("sp_get_dashboard_json_object", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
			return json;        }

    }
}
