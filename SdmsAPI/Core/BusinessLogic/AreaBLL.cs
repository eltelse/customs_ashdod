﻿using EltelWebApi.DataAccess;
using EltelWebApi.Helpers;
using EltelWebApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;

namespace EltelWebApi.Core.BusinessLogic
{
    public class AreaBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public AreaBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<AreaBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public DataTable GetCheckinAreas()
        {
            string sql = @"SELECT * FROM v_checkin_areas Order By [AreaName]";
            try
            {
                return _dal.GetTable(sql);
            }
            catch (Exception ex)
            {
                throw new AppException(_appSettings.ERROR_MESSAGE_FORMAT_GET, _appSettings.ERROR_DETAILS_DB, (int)InternalErrorCodes.DataGetError, HttpStatusCode.InternalServerError, $"checkin areas", $"Message: {ex.Message}");
            }
        }

        public string AddArea(Area obj)
        {
            SqlParameter[] pArr =
            {
                    new SqlParameter("name", SqlDbType.VarChar, 100) { Value = obj.Name},
                    new SqlParameter("description", SqlDbType.VarChar, 250) { Value = obj.Description},
                    new SqlParameter("areaTypeId", SqlDbType.Int) { Value = obj.AreaTypeId},

                };

            var affectedRows = _dal.Execute("sp_area_add", pArr);

            ValidateCRUDResults(affectedRows, "area", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateArea(Area obj)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = obj.Id });

            if (!string.IsNullOrEmpty(obj.Name)) dbParams.Add(new SqlParameter("name", SqlDbType.VarChar, 100) { Value = obj.Name });
            dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = obj.Description });
            if (obj.AreaTypeId > -1) dbParams.Add(new SqlParameter("areaTypeId", SqlDbType.Int) { Value = obj.AreaTypeId });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_area_update", pArr);

            ValidateCRUDResults(affectedRows, "area", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteArea(string id)
        {
            string sql = "update dbo.area set isDeleted = 1 WHERE id=" + id;
            
            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "area", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }
    }
}
