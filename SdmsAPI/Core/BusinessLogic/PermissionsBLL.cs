﻿using Microsoft.Extensions.Configuration;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Net;

namespace EltelWebApi.Core.BusinessLogic
{
    public class PermissionsBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public PermissionsBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<PermissionsBLL> logger) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public DataTable GetPermissionNames()
        {
            string sql = @"select v.id, v.[Name] from dbo.v_permissions v";
            return _dal.GetTable(sql);
        }

        public string AddPermission(Permission p)
        {
            SqlParameter[] pArr =
{
                    new SqlParameter("name", SqlDbType.VarChar, 100) { Value = p.Name},
                    new SqlParameter("description", SqlDbType.VarChar, 250) { Value = p.Description },
                    new SqlParameter("permission_subject_id", SqlDbType.VarChar, 250) { Value = p.PermissionSubjectId },
                };

            var affectedRows = _dal.Execute("sp_permission_add", pArr);

            ValidateCRUDResults(affectedRows, "permission", CRUDType.CREATE);

            return ResultStatuses.Success.ToString();
        }

        public string UpdatePermission(Permission p)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = p.Id });

            if (!string.IsNullOrEmpty(p.Name)) dbParams.Add(new SqlParameter("name", SqlDbType.VarChar, 100) { Value = p.Name });
            dbParams.Add(new SqlParameter("description", SqlDbType.VarChar, 250) { Value = p.Description });
            if (p.PermissionSubjectId != default) dbParams.Add(new SqlParameter("permission_subject_id", SqlDbType.Int) { Value = p.PermissionSubjectId });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_permission_update", pArr);

            ValidateCRUDResults(affectedRows, "permission", CRUDType.UPDATE);

            return ResultStatuses.Success.ToString();
        }

        public string DeletePermission(string id)
        {
            string sql = "update [auth].[permission] Set isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "permission", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }
    }
}
