﻿using Microsoft.Extensions.Configuration;
using EltelWebApi.Controllers;
using EltelWebApi.DataAccess;
using EltelWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using EltelWebApi.Services;
using SdmsAPI.Models.ViewModels;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Net;

namespace EltelWebApi.Core.BusinessLogic
{
    public class NotificationBLL : BaseBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;
        private IServerSentEventsService _sseSvc;

        public NotificationBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<NotificationBLL> logger, IServerSentEventsService sseSvc) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
            _sseSvc = sseSvc;
        }

        public DataTable GetUserNotification(string userId)
        {
            string sql = $"select * from dbo.fn_GetUserNotifications ({userId})";
            return _dal.GetTable(sql);
        }

        public DataTable GetNotificationById(string id)
        {
            string sql = "select * from dbo.v_notification_display WHERE id=" + id;
            return _dal.GetTable(sql);
        }

        /// <summary>
        /// This method adds anotification to database and publishes it to clients
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public string AddNotification(Notification n)
        {
            var resObj = AddNotificationGetData(n);
            int identity = resObj.Id;

            // publish notification
            var tbl = GetNotificationById(identity.ToString());
            var nvm = DalHelper.DataRowToObject<NotificationVM>(tbl.Rows[0]);
            nvm.AutoPopup = resObj.AutoPopup;
            nvm.IsDanger = resObj.IsDanger;
            _sseSvc.SendEventAsync(nvm);

            return ResultStatuses.Success.ToString();
        }

        public string UpdateNotification(Notification n)
        {
            List<SqlParameter> dbParams = new List<SqlParameter>();

            dbParams.Add(new SqlParameter("id", SqlDbType.Int) { Value = n.Id });

            if (n.NotificationTypeId != default(int)) dbParams.Add(new SqlParameter("notification_type_id", SqlDbType.Int) { Value = n.NotificationTypeId });
            if (n.NotificationPublishTypeId != default(int)) dbParams.Add(new SqlParameter("publish_type_id", SqlDbType.Int) { Value = n.NotificationPublishTypeId });
            if (!string.IsNullOrEmpty(n.Subject)) dbParams.Add(new SqlParameter("subject", SqlDbType.VarChar, 50) { Value = n.Subject });
            if (!string.IsNullOrEmpty(n.Message)) dbParams.Add(new SqlParameter("message", SqlDbType.VarChar, 250) { Value = n.Message });
            if (n.SourceUserId != default(int)) dbParams.Add(new SqlParameter("src_user_id", SqlDbType.Int) { Value = n.SourceUserId });
            if (!string.IsNullOrEmpty(n.JsonData)) dbParams.Add(new SqlParameter("json_data", SqlDbType.VarChar, -1) { Value = n.JsonData });
            if (n.TargetId != default(int)) dbParams.Add(new SqlParameter("target_id", SqlDbType.Int) { Value = n.TargetId });
            if (!string.IsNullOrEmpty(n.Reply)) dbParams.Add(new SqlParameter("reply", SqlDbType.VarChar, 250) { Value = n.Reply });
            if (n.IsReplyable.HasValue) dbParams.Add(new SqlParameter("is_replyable", SqlDbType.Bit) { Value = n.IsReplyable.Value });
            if (n.IsHandled.HasValue) dbParams.Add(new SqlParameter("is_handled", SqlDbType.Bit) { Value = n.IsHandled.Value });

            SqlParameter[] pArr = dbParams.ToArray();

            var affectedRows = _dal.Execute("sp_notification_update", pArr);

            ValidateCRUDResults(affectedRows, "notification", CRUDType.UPDATE);

            NotificationVM nUpd = new();
            nUpd.NotificationTypeId = (int)NotificationType.Inspections;
            nUpd.Subject = "Inspections table refresh data after notification update";
            _sseSvc.SendEventAsync(nUpd);

            return ResultStatuses.Success.ToString();
        }

        public string DeleteNotification(string id)
        {
            string sql = "update dbo.Notification SET isDeleted = 1 WHERE id=" + id;

            int affectedRows = _dal.Execute(sql);

            ValidateCRUDResults(affectedRows, "notification", CRUDType.DELETE);

            return ResultStatuses.Success.ToString();
        }

        private AddNotificationResultVM AddNotificationGetData(Notification n)
        {

            SqlParameter[] pArr =
            {
                    new SqlParameter("notification_type_id", SqlDbType.Int) { Value = n.NotificationTypeId},
                    new SqlParameter("publish_type_id", SqlDbType.Int) { Value = n.NotificationPublishTypeId},
                    new SqlParameter("subject", SqlDbType.VarChar, 50) { Value = n.Subject},
                    new SqlParameter("message", SqlDbType.VarChar, 250) { Value = n.Message},
                    new SqlParameter("src_user_id", SqlDbType.Int) { Value = n.SourceUserId},
                    new SqlParameter("target_id", SqlDbType.Int) { Value = n.TargetId},
                    new SqlParameter("json_data", SqlDbType.VarChar, -1) { Value = n.JsonData},
                    new SqlParameter("is_handled", SqlDbType.Bit) { Value = n.IsHandled.HasValue? n.IsHandled.Value : false},
                    new SqlParameter("is_replyable", SqlDbType.Bit) { Value = n.IsReplyable.HasValue? n.IsReplyable.Value: true},
                };

            var resTbl = _dal.GetTable("sp_notification_add_return_data", pArr);

            int affectedRows = resTbl != null ? resTbl.Rows.Count : 0;

            ValidateCRUDResults(affectedRows, "add notification - get data", CRUDType.CREATE);

            var resMsg = resTbl.Rows[0]["Message"].ToString();

            if (resMsg != "Success")
                throw new AppException(resMsg, _appSettings.ERROR_DETAILS_DB, (int)InternalErrorCodes.StartInspectionError, HttpStatusCode.InternalServerError);


            Int32.TryParse(resTbl.Rows[0]["Id"].ToString(), out int notificationId);
            bool.TryParse(resTbl.Rows[0]["AutoPopup"].ToString(), out bool autoPopup);
            bool.TryParse(resTbl.Rows[0]["IsDanger"].ToString(), out bool isDanger);

            return new AddNotificationResultVM() { Message = resTbl.Rows[0]["Message"].ToString(), Id = notificationId, AutoPopup = autoPopup, IsDanger = isDanger };

        }
    }
}
