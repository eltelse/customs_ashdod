﻿using Microsoft.Extensions.Configuration;
using EltelWebApi.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace EltelWebApi.Core.BusinessLogic
{
    public class ListsDataBLL
    {
        private readonly IDal _dal;
        ILogger _logger;
        private readonly AppSettings _appSettings;

        public ListsDataBLL(IOptions<AppSettings> appSettings, IDal dal, ILogger<ListsDataBLL> logger)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _logger = logger;
        }

        public DataTable GetInspectionTypes()
        {
            string sql = @"select * from dbo.v_inspection_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetConclusionTypes()
        {
            string sql = @"select * from dbo.v_conclusion_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetWorkStationTypes()
        {
            string sql = @"select * from dbo.v_work_station_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetNotificationTypes()
        {
            string sql = @"select * from dbo.v_notification_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetNotificationPublishTypes()
        {
            string sql = @"select * from dbo.v_notification_publish_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetUsersList()
        {
            string sql = @"select * from dbo.v_users_list";
            return _dal.GetTable(sql);
        }

        public DataTable GetUserTypes()
        {
            string sql = @"select * from dbo.v_user_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetVehicleTypes()
        {
            string sql = @"select * from dbo.v_vehicle_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetLPRCorrectionTypes()
        {
            string sql = @"select * from dbo.v_lpr_correction_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetCCRCorrectionTypes()
        {
            string sql = @"select * from dbo.v_ccr_correction_types";
            return _dal.GetTable(sql);
        }

        public DataTable GetAreaTypes()
        {
            string sql = @"select * from [dbo].[v_area_types]";
            return _dal.GetTable(sql);
        }

        public DataTable GetDeviceTypes()
        {
            string sql = @"select * from [dbo].[v_device_types]";
            return _dal.GetTable(sql);
        }

        public DataTable GetDeviceEventTypes()
        {
            string sql = @"select * from [dbo].[v_device_event_types]";
            return _dal.GetTable(sql);
        }


    }
}
