﻿using EltelWebApi.DataAccess;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SdmsAPI.Core;
using EltelWebApi.Core;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using EltelWebApi.Models;
using System.Text;
using EltelWebApi.Controllers;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.Extensions.Logging;
using EltelWebApi.Services;
using SdmsAPI.Models.ViewModels;
using System.IO;
using System.Dynamic;
using SdmsAPI.Models;

namespace SdmsAPI.Core
{
    public class ApiDeviceEventBLL : BaseBLL
    {
        private readonly IDal _dal;
        private readonly AppSettings _appSettings;
        private readonly NotificationBLL _notificationsBll;
        private readonly InspectionBLL _inspectionsBll;
        private readonly DeviceBLL _deviceBll;
        ILogger _logger;
        private IServerSentEventsService _sseSvc;

        private bool _overrideImportedFiles = false;

        public ApiDeviceEventBLL(IOptions<AppSettings> appSettings, IDal dal, NotificationBLL notificationBll, DeviceBLL deviceBll, ILogger<ApiDeviceEventBLL> logger, IServerSentEventsService sseSvc, InspectionBLL inspectionBll) : base(appSettings, dal)
        {
            _appSettings = appSettings.Value;
            _dal = dal;
            _notificationsBll = notificationBll;
            _deviceBll = deviceBll;
            _logger = logger;
            _sseSvc = sseSvc;
            _inspectionsBll = inspectionBll;

            bool.TryParse(_appSettings.OverrideImportedFiles, out _overrideImportedFiles);
        }

        public string AddDeviceEvent(DeviceEventInfo ev)
        {
            _logger.LogTrace($"Adding device event for device {ev.DeviceIdentifier}");
            string res = ResultStatuses.Success.ToString();

            _logger.LogTrace($"Importing device event files.");
            res = ImportFiles(ev);
            if (res != ResultStatuses.Success.ToString())
            {
                var freindlyMessage = "Something went wrong while importing device event files.";
                var msg = $"Importing files did not return a success status. Quiting 'AddDeviceEvent'. Message: {res}";
                throw new AppException(msg, freindlyMessage);
            }

            _logger.LogTrace($"Saving device event.");
            // Note! not logging errors here. It is done internally in the below method
            SaveDeviceEvent(ev);

            return ResultStatuses.Success.ToString(); ;
        }

        private string ImportFiles(DeviceEventInfo ev)
        {
            var res = ResultStatuses.Success.ToString();
            bool hasLPR = ev.Files != null && ev.Files.LPR != null && ev.Files.LPR.Count > 0;
            bool hasCCR = ev.Files != null && ev.Files.CCR != null && ev.Files.CCR.Count > 0;
            bool hasIAW = ev.Files != null && ev.Files.IAW != null && ev.Files.IAW.Count > 0;
            bool hasManifest = ev.Files != null && ev.Files.Manifest != null && ev.Files.Manifest.Count > 0;

            if (!hasCCR && !hasLPR && !hasIAW && !hasManifest) return res;

            string targetPattern = GetDeviceTargetDirectoryPattern(ev.DeviceIdentifier);
            if (targetPattern.Contains(ResultStatuses.Error.ToString()))
            {
                string errMessage = targetPattern;
                _logger.LogError("Error while Importing device files for {DeviceIdentifier}. Message: {Message}", ev.DeviceIdentifier, errMessage);
                return errMessage;
            }

            try
            {
                // LPR
                if (hasLPR)
                {
                    for (int i = 0; i < ev.Files.LPR.Count; i++)
                    {
                        DeviceFileInfo file = ev.Files.LPR[i];
                        string targetPath = GetLprTargetPath(ev, targetPattern, file, i + 1);
                        file.TargetPath = targetPath;
                        string targetDirectory = Path.GetDirectoryName(targetPath);
                        if (!Directory.Exists(targetDirectory)) Directory.CreateDirectory(targetDirectory);
                        File.Copy(file.Path, targetPath, _overrideImportedFiles);
                    }
                }

                // CCR
                if (hasCCR)
                {
                    for (int i = 0; i < ev.Files.CCR.Count; i++)
                    {
                        DeviceFileInfo file = ev.Files.CCR[i];
                        string targetPath = GetCcrTargetPath(ev, targetPattern, file, i + 1);
                        file.TargetPath = targetPath;
                        string targetDirectory = Path.GetDirectoryName(targetPath);
                        if (!Directory.Exists(targetDirectory)) Directory.CreateDirectory(targetDirectory);
                        if (!string.IsNullOrEmpty(file.Base64))
                        {
                            byte[] bytes = Convert.FromBase64String(file.Base64);
                            File.WriteAllBytes(targetPath, bytes);
                            // After the file is saved. It is necessary to clear the field. Otherwise the json sent to data base will get currapted
                            file.Base64 = string.Empty;
                        }
                        else
                        {
                            File.Copy(file.Path, targetPath, _overrideImportedFiles);
                        }
                    }
                }

                // IAW
                if (hasIAW)
                {
                    // ScannerIdentifier
                    for (int i = 0; i < ev.Files.IAW.Count; i++)
                    {
                        DeviceFileInfo file = ev.Files.IAW[i];
                        string targetPath = GetIawTargetPath(ev, targetPattern, file);
                        file.TargetPath = targetPath;
                        string targetDirectory = Path.GetDirectoryName(targetPath);
                        if (!Directory.Exists(targetDirectory)) Directory.CreateDirectory(targetDirectory);

                        if (!string.IsNullOrEmpty(file.Base64))
                        {
                            byte[] bytes = Convert.FromBase64String(file.Base64);
                            File.WriteAllBytes(targetPath, bytes);
                            // After the file is saved. It is necessary to clear the field. Otherwise the json sent to data base will get currapted
                            file.Base64 = string.Empty;
                        }
                        else
                        {
                            File.Copy(file.Path, targetPath, _overrideImportedFiles);
                        }
                    }
                }

                // Manifest
                if (hasManifest)
                {
                    // ScannerIdentifier
                    for (int i = 0; i < ev.Files.Manifest.Count; i++)
                    {
                        DeviceFileInfo file = ev.Files.Manifest[i];
                        string targetPath = GetManifestTargetPath(ev, targetPattern, file);
                        file.TargetPath = targetPath;
                        string targetDirectory = Path.GetDirectoryName(targetPath);
                        if (!Directory.Exists(targetDirectory)) Directory.CreateDirectory(targetDirectory);
                        if (!string.IsNullOrEmpty(file.Base64))
                        {
                            byte[] bytes = Convert.FromBase64String(file.Base64);
                            File.WriteAllBytes(targetPath, bytes);
                            // After the file is saved. It is necessary to clear the field. Otherwise the json sent to data base will get currapted
                            file.Base64 = string.Empty;
                        }
                        else
                        {
                            File.Copy(file.Path, targetPath, _overrideImportedFiles);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error while Importing device files for {DeviceIdentifier}. Message: {Message}", ev.DeviceIdentifier, ex.Message);
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        private string GetLprTargetPath(DeviceEventInfo ev, string targetPattern, DeviceFileInfo file, int index)
        {
            //
            //path_fixed_perfix\ocr\device_identifier\yyyy\MM\dd\HHmmss\hardwarename_ddHHmmssfff_imageindexnozero_Recognition_platenumber.jpg
            string srcPath = file.Path;
            string hardwarename = file.OwnerDeviceIdentifier;
            string yyyy = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string MM = month.Length == 1 ? "0" + month : month;
            string dd = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string HH = hour.Length == 1 ? "0" + hour : hour;
            string HHmmss = DateTime.Now.ToString("HHmmss");
            string ddHHmmssfff = DateTime.Now.ToString("ddHHmmssfff");
            string imageIndex = index.ToString();
            string targetPath = targetPattern
                .Replace("path_fixed_perfix", _appSettings.Trucks_Target_Path_Prefix)
                .Replace("device_identifier", ev.DeviceIdentifier)
                .Replace("yyyy", yyyy)
                .Replace("MM", MM)
                .Replace("ddHHmmssfff", ddHHmmssfff)
                .Replace("dd", dd)
                .Replace("HHmmss", HHmmss)
                .Replace("hardwarename", hardwarename)
                .Replace("imageindexnozero", imageIndex)
                .Replace("platenumber", ev.LPR);
            return targetPath;
        }

        private string GetCcrTargetPath(DeviceEventInfo ev, string targetPattern, DeviceFileInfo file, int index)
        {
            //path_fixed_perfix\ocr\device_identifier\yyyy\MM\dd\HHmmss\file_name
            string srcPath = file.Path;
            string hardwarename = file.OwnerDeviceIdentifier;
            string yyyy = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string MM = month.Length == 1 ? "0" + month : month;
            string dd = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string HH = hour.Length == 1 ? "0" + hour : hour;
            string HHmmss = DateTime.Now.ToString("HHmmss");
            string ddHHmmssfff = DateTime.Now.ToString("ddHHmmssfff");
            string imageIndex = index.ToString();
            string targetPath = targetPattern
                .Replace("path_fixed_perfix", _appSettings.Trucks_Target_Path_Prefix)
                .Replace("device_identifier", ev.DeviceIdentifier)
                .Replace("yyyy", yyyy)
                .Replace("MM", MM)
                .Replace("ddHHmmssfff", ddHHmmssfff)
                .Replace("dd", dd)
                .Replace("HHmmss", HHmmss)
                .Replace("file_name", file.Name);
            return targetPath;
        }

        private string GetIawTargetPath(DeviceEventInfo ev, string targetPattern, DeviceFileInfo file)
        {
            // path_fixed_perfix\nii\nii_identifier\yyyy\MM\dd\inspection_index\scan_id
            string srcPath = file.Path;
            string yyyy = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string MM = month.Length == 1 ? "0" + month : month;
            string dd = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string HH = hour.Length == 1 ? "0" + hour : hour;
            string HHmmss = DateTime.Now.ToString("HHmmss");
            string ddHHmmssfff = DateTime.Now.ToString("ddHHmmssfff");
            string inspectionIndex = ev.ScanId.Substring(ev.ScanId.Length - 4);
            string targetPath = targetPattern
                .Replace("path_fixed_perfix", _appSettings.Trucks_Target_Path_Prefix)
                .Replace("nii_identifier", ev.ScannerIdentifier)
                .Replace("yyyy", yyyy)
                .Replace("MM", MM)
                .Replace("dd", dd)
                .Replace("inspection_index", inspectionIndex)
                .Replace("scan_id", ev.ScanId)
                .Replace("file_name", file.Name);
            return targetPath;
        }

        private string GetManifestTargetPath(DeviceEventInfo ev, string targetPattern, DeviceFileInfo file)
        {
            //path_fixed_perfix\manifest\area\yyyy\MM\dd\file_name.jpg
            string srcPath = file.Path;
            string yyyy = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string MM = month.Length == 1 ? "0" + month : month;
            string dd = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string HH = hour.Length == 1 ? "0" + hour : hour;
            string targetPath = targetPattern
                .Replace("path_fixed_perfix", _appSettings.Trucks_Target_Path_Prefix)
                .Replace("area", ev.AreaIdentifier)
                .Replace("yyyy", yyyy)
                .Replace("MM", MM)
                .Replace("dd", dd)
                .Replace("file_name", file.Name); // file_name should include extension (jpg)
            return targetPath;
        }

        private void SaveDeviceEvent(DeviceEventInfo ev)
        {
            string res = "";
            var freindlyMessage = "Something went wrong while saving device event.";

            try
            {
                SqlParameter[] pArr = { new SqlParameter("json", SqlDbType.VarChar, -1) { Value = JsonConvert.SerializeObject(ev) }/* -1 is MAX */ };

                var resTbl = _dal.GetTable("sp_device_add_event", pArr);

                res = GetResultMessge(res, resTbl);
                if (res != ResultStatuses.Success.ToString())
                    throw new AppException(res, freindlyMessage);

                var tmpRes = AdditionalActions(resTbl, ev);
                if (tmpRes != ResultStatuses.Success.ToString())
                    res = tmpRes;
            }
            catch (Exception ex)
            {
                var msg = $"Error while saving device event for device {ev.DeviceIdentifier}. Message: {ex.Message}";
                throw new AppException(msg, freindlyMessage);
            }

            HandleDeviceEventResult(res, ev.DeviceIdentifier);
        }

        private void HandleDeviceEventResult(string res, string device)
        {
            if (res == ResultStatuses.Success.ToString())
            {
                _logger.LogTrace("Notifying angular to refresh inspections table and dashboard data.");
                NotifyRefreshInspections();
                NotifyRefreshDashboard();
            }
            else
                _logger.LogDebug($"Saving device event for device {device} returned an error. Message: {res}.");
        }

        private static string GetResultMessge(string res, DataTable resTbl)
        {
            if (resTbl.Rows.Count < 1 || resTbl.Rows[0]["Message"].ToString() != ResultStatuses.Success.ToString())
                res = $"Failure. Message: Something went wrong. {resTbl.Rows[0]["Message"].ToString()}";
            else
                res += resTbl.Rows[0]["Message"].ToString();
            return res;
        }

        private string AdditionalActions(DataTable resTbl, DeviceEventInfo ev)
        {
            string res = ResultStatuses.Success.ToString();

            try
            {
                switch (ev.DeviceIdentifier)
                {
                    case "CIA1LPR1":
                    case "CIA2LPR1":
                        res = NotifyCheckinModalPopup(resTbl, ev);
                        break;
                    case "CIA1Scanner1":
                        res = NotifyCheckinPopupConsumeManifest(resTbl, ev);
                        break;
                    case "BCO1LPR1":
                        res = ValidateBeforeCheckoutOCRData(resTbl, ev);
                        break;
                    case "COA1LPR1":
                        res = ValidateAndPerformSiteExit(resTbl, ev);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }
            return res;
        }

        private string ValidateBeforeCheckoutOCRData(DataTable resTbl, DeviceEventInfo ev)
        {
            string res = ResultStatuses.Success.ToString();
            string allowedExit = "False";
            string isOCRDataValid = "False";
            allowedExit = resTbl.Rows[0]["AllowedExit"].ToString();
            isOCRDataValid = resTbl.Rows[0]["IsOCRDataValid"].ToString();

            if (isOCRDataValid != "True")
            {
                _logger.LogInformation($"Vehicle {ev.LPR} is not allowd to leave doe to non matching OCR LPR.");
                // TODO: How to notifiy???
                res += " Befor checkout OCR data is invalid!!!";
            }

            return res;
        }

        private string ValidateAndPerformSiteExit(DataTable resTbl, DeviceEventInfo ev)
        {
            string res = ResultStatuses.Success.ToString();
            string allowedExit = "0";
            allowedExit = resTbl.Rows[0]["AllowedExit"].ToString();
            if (allowedExit == "True")
            {
                _logger.LogDebug("Opening check out barrier");
                OpenCheckoutBarrier(ev);

                Int32.TryParse(resTbl.Rows[0]["InspectionId"].ToString(), out int inspectionId);
                res = PrintCheckoutTag(inspectionId);
            }
            else
            {
                _logger.LogDebug("Vehicle is not allowd to leave. Notifying checkout operator");
                res = NotifyCheckoutForUnauthorizedLeaveVehicle(resTbl, ev);
            }

            return res;
        }

        private string NotifyCheckinModalPopup(DataTable resTbl, DeviceEventInfo ev)
        {
            string res = ResultStatuses.Success.ToString();
            try
            {
                Int32.TryParse(resTbl.Rows[0]["InspectionId"].ToString(), out int inspectionId);
                if (inspectionId == default)
                {
                    res = res + "InspectionId was not found in data.";
                    _logger.LogInformation(res);
                    return res;
                }

                SqlParameter[] pArr1 =
                {
                    new SqlParameter("device_identifier", SqlDbType.VarChar, 20) { Value = ev.DeviceIdentifier },
                };

                var tblUserData = _dal.GetTable("sp_get_user_data_by_device_identifier", pArr1);

                if (tblUserData.Rows.Count < 1)
                {
                    res += "Error: Could not get a user for check-in modal automatic popup.";
                    _logger.LogInformation(res);
                    return res;
                }

                foreach (DataRow row in tblUserData.Rows)
                {
                    Int32.TryParse(row["UserId"].ToString(), out int targetUserId);
                    NotifyCheckinPopup(inspectionId, targetUserId);
                }

            }
            catch (Exception ex)
            {
                res = $"Error while notifying checkin pop up. Message: {ex.Message}";
                _logger.LogError(res);
            }

            return res;
        }

        private string NotifyCheckinPopupConsumeManifest(DataTable resTbl, DeviceEventInfo ev)
        {
            string res = ResultStatuses.Success.ToString();
            try
            {
                Int32.TryParse(resTbl.Rows[0]["InspectionId"].ToString(), out int inspectionId);
                if (inspectionId == default)
                {
                    res = res + "InspectionId was not found in data.";
                    _logger.LogInformation(res);
                    return res;
                }

                SqlParameter[] pArr1 =
                {
                    new SqlParameter("device_identifier", SqlDbType.VarChar, 20) { Value = ev.DeviceIdentifier },
                };

                var tblUserData = _dal.GetTable("sp_get_user_data_by_device_identifier", pArr1);

                if (tblUserData.Rows.Count < 1)
                {
                    res += "Error: Could not get a user for check-in modal automatic popup cunsume manifest.";
                    _logger.LogInformation(res);
                    return res;
                }

                foreach (DataRow row in tblUserData.Rows)
                {
                    Int32.TryParse(row["UserId"].ToString(), out int targetUserId);
                    NotifyCheckinPopupToConsumeManifest(inspectionId, targetUserId);
                }

            }
            catch (Exception ex)
            {
                res = $"Error while notifying checkin pop up to consume manifest. Message: {ex.Message}";
                _logger.LogError(res);
            }

            return res;
        }

        private string NotifyCheckoutForUnauthorizedLeaveVehicle(DataTable resTbl, DeviceEventInfo ev)
        {
            string res = ResultStatuses.Success.ToString();
            try
            {
                Int32.TryParse(resTbl.Rows[0]["InspectionId"].ToString(), out int inspectionId);
                if (inspectionId == default)
                {
                    res = res + "InspectionId was not found in data.";
                    _logger.LogInformation(res);
                    return res;
                }

                SqlParameter[] pArr1 =
                {
                    new SqlParameter("device_identifier", SqlDbType.VarChar, 20) { Value = ev.DeviceIdentifier },
                };

                var tblUserData = _dal.GetTable("sp_get_user_data_by_device_identifier", pArr1);

                if (tblUserData.Rows.Count < 1)
                {
                    res = "Error: Could not get a user for chek-out.";
                    _logger.LogInformation(res);
                }
                else if (tblUserData.Rows.Count > 0)
                {
                    // Default if no use is logged in or no user can be found for check out.
                    int targetId = 6; // SiteManager role
                    var publishType = NotificationPublishType.Role;

                    // Try get a checkout user as target for the notification.
                    Int32.TryParse(tblUserData.Rows[0]["UserId"].ToString(), out int targetUserId);
                    if (targetUserId != default)
                    {
                        targetId = targetUserId;
                        publishType = NotificationPublishType.User;
                    }
                    NotifyCheckoutForUnauthorizedLeaveAttempt(inspectionId, publishType, targetId);

                }


            }
            catch (Exception ex)
            {
                res = $"Error while notifying checkout of unauthorized leave. Message: {ex.Message}";
                _logger.LogError(res);
            }

            return res;
        }

        private string OpenCheckoutBarrier(DeviceEventInfo ev)
        {
            string res = ResultStatuses.Success.ToString();
            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("json", SqlDbType.VarChar, -1) { Value = JsonConvert.SerializeObject(ev) } // -1 is MAX
                };

                var resTbl = _dal.GetTable("sp_inspection_add_opening_checkout_barrier_event", pArr);

                if (resTbl.Rows.Count < 1 || resTbl.Rows[0]["Message"].ToString() != ResultStatuses.Success.ToString())
                {
                    res = "Failure. Message: Something went wrong while adding open barrier event.";
                    return res;
                }
                res = resTbl.Rows[0]["Message"].ToString();

                // TODO: Open barier device
               

            }
            catch (Exception ex)
            {
                res = "Failure. Message: " + ex.Message;
            }

            return res;
        }

        private string PrintCheckoutTag(int inspectionId)
        {
            string sql = $"SELECT * FROM dbo.v_inspections_raw WHERE id = {inspectionId}";
            var dt = _dal.GetTable(sql);
            if (dt.Rows.Count == 0) throw new AppException("Error printing dtiver tag at checkin start process", $"Did not finf inspection checkin tag data for inspection id {inspectionId}");

            var insp = DalHelper.DataRowToObject<Inspection>(dt.Rows[0]);
            dynamic data = new ExpandoObject();
            data.Identifier = insp.Identifier;
            data.Lines = new List<TagPrintObjItem>()
            {
                new TagPrintObjItem(){ Title = "זמן כניסה", Value = insp.Arrival.ToString("yyyy-MM-dd HH:mm:ss:fff") },
                new TagPrintObjItem(){ Title = "זמן יציאה", Value = insp.Leave.ToString("yyyy-MM-dd HH:mm:ss:fff") }
            };

            string json = JsonConvert.SerializeObject(data);

            return PostWorkflowsRequests(json, _appSettings.TagPrintUrl_Checkout);
        }

    
        private void NotifyCheckinPopup(int inspectionId, int targetId = default)
        {
            _logger.LogInformation($"Notifing check in modal pop up for inspection {inspectionId}");

            NotificationVM n = new();
            n.TargetId = targetId;
            n.NotificationTypeId = (int)NotificationType.CheckinPopup;
            var sb = new StringBuilder();
            sb.Append("{ ");
            sb.Append($"\"InspectionId\": \"{inspectionId}\" ");
            sb.Append(" }");
            n.JsonData = sb.ToString();
            _sseSvc.SendEventAsync(n);
        }

        private void NotifyCheckinPopupToConsumeManifest(int inspectionId, int targetUserId)
        {
            _logger.LogInformation($"Notifing check in modal pop up to consume manifest for inspection {inspectionId}");

            NotificationVM n = new();
            n.TargetId = targetUserId;
            n.NotificationTypeId = (int)NotificationType.CheckinPopupConsumeManifest;
            var sb = new StringBuilder();
            sb.Append("{ ");
            sb.Append($"\"InspectionId\": \"{inspectionId}\" ");
            sb.Append(" }");
            n.JsonData = sb.ToString();
            _sseSvc.SendEventAsync(n);
        }


        private void NotifyCheckoutForUnauthorizedLeaveAttempt(int inspectionId, NotificationPublishType publishType, int targetId)
        {
            var inspectionTbl = GetEntitiesById("inspection", inspectionId.ToString());
            string lpr = "";
            string ccr = "";
            if (inspectionTbl.Rows.Count > 0)
            {
                lpr = inspectionTbl.Rows[0]["LPR"].ToString();
                ccr = inspectionTbl.Rows[0]["CCR"].ToString();
            }

            _logger.LogInformation($"Notifing check out, of unauthorized leave attempt, of a vehicle that arrived checkout. Inspection: {inspectionId}");
            Notification n = PrepareUnauthorizedCheckoutAttemptNotification(inspectionId, lpr, ccr, publishType, targetId);

            _notificationsBll.AddNotification(n);
        }

        private Notification PrepareUnauthorizedCheckoutAttemptNotification(int inspectionId, string lpr, string ccr, NotificationPublishType publishType, int targetId)
        {
            Notification n = new();
            n.NotificationPublishTypeId = (int)publishType;
            n.IsReplyable = false;
            n.TargetId = targetId;
            n.NotificationTypeId = (int)NotificationType.CheckoutNonAllowedPopup;
            n.Subject = $"{_appSettings.COANonAllowedLeaveSubject}";
            n.Message = $"{_appSettings.COANonAllowedLeaveMessage}.";
            var sb = new StringBuilder();
            sb.Append("{ ");
            sb.Append($"\"InspectionId\": \"{inspectionId}\" ,");
            sb.Append($"\"LPR\": \"{lpr}\" ,");
            sb.Append($"\"CCR\": \"{ccr}\" ");

            sb.Append(" }");
            n.JsonData = sb.ToString();
            return n;
        }

        private void NotifyRefreshInspections()
        {
            NotificationVM n = new();
            n.NotificationTypeId = (int)NotificationType.Inspections;
            _sseSvc.SendEventAsync(n);
        }

        private void NotifyRefreshDashboard()
        {
            NotificationVM n = new();
            n.NotificationTypeId = (int)NotificationType.Dashboard;
            _sseSvc.SendEventAsync(n);
        }

        private string GetDeviceTargetDirectoryPattern(string deviceIdentifier)
        {
            string res = "";
            var resTbl = _deviceBll.GetDeviceByIdentifier(deviceIdentifier);
            if (resTbl.Rows.Count == 0) return $"Error: Did not find device data for {deviceIdentifier}.";
            res = resTbl.Rows[0]["TargetDirectoryPattern"].ToString();
            return res;
        }

    }
}
