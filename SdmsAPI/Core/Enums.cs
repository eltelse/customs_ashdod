﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Core
{
    public enum NotificationType
    {
        /// <summary>
        /// This notification is sent to a site manager to ask for bypass for a non scannabl truck.
        /// </summary>
        Bypass = 1,
        /// <summary>
        /// Notification for a client to pop up checkin modal
        /// </summary>
        CheckinPopup = 2,
        /// <summary>
        /// Intend to signal inspection table to update itself
        /// </summary>
        Inspections = 3,
        /// <summary>
        /// Intend to signal dashboard to update itself
        /// </summary>
        Dashboard = 4,

        CheckoutNonAllowedPopup = 5,

        CheckinPopupConsumeManifest = 6,
    }

    public enum NotificationPublishType
    {
        User = 1,
        Role = 2
    }

    public enum ResultStatuses
    {
        Success,
        Failure,
        Error
    }

    public enum UserType
    {
        User = 1,
        Service = 2
    }

    public enum InternalErrorCodes
    {
        NOT_SET = 0,
        DataGetError = 1,
        DataAddError = 2,
        DataUpdateError = 3,
        DataDeleteError = 4,
        LoginFailure_InvalidUserOrPass = 5,
        InvalidRefreshToken = 6,
        StartInspectionError = 7,
    }

    public enum CRUDType
    {
        CREATE,
        UPDATE,
        DELETE
    }

    public enum TagPrintingType
    {
        CheckIN,
        CheckOUT
    }

    public enum SortType
    {
        DESC,
        ASC
    }
}