﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using EltelWebApi.Authorization;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class DriverTagController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private DriverTagBLL _bll;

        public DriverTagController(IConfiguration configuration, IWebHostEnvironment env, DriverTagBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [HttpGet]
        [Route("GetDriverTagPopupData/{tagCode}")]
        [AppAuthorizePermission(Permissions = "canViewDriverTag")]
        public JsonResult GetDriverTagPopupData(string tagCode)
        {
            return new JsonResult(_bll.GetDriverTagPopupData(tagCode));
        }

    }
}
