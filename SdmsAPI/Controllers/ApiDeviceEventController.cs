﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SdmsAPI.Core;
using EltelWebApi.Authorization;

namespace SdmsAPI.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ApiDeviceEventController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private ApiDeviceEventBLL _bll;

        public ApiDeviceEventController(IConfiguration configuration, IWebHostEnvironment env, ApiDeviceEventBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canApiPost")]
        public JsonResult Post(DeviceEventInfo ev)
        {
            return new JsonResult(_bll.AddDeviceEvent(ev));
        }



    }
}
