﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using EltelWebApi.Authorization;
using EltelWebApi.Core;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private VehiclesBLL _bll;
        string _entityName = "vehicle";

        public VehicleController(IConfiguration configuration, IWebHostEnvironment env, VehiclesBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetVehiclesDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewVehicle")]
        public JsonResult GetGetVehiclesDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewVehicle")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewVehicle")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewVehicle")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddVehicle")]
        public JsonResult Post(Vehicle vehicle)
        {
            return new JsonResult(_bll.AddVehicle(vehicle));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditVehicle")]
        public JsonResult Put(Vehicle vehicle)
        {
            return new JsonResult(_bll.UpdateVehicle(vehicle));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteVehicle")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteVehicle(id));
        }


        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string filename = postedFile.FileName;
                var physicalPath = _env.ContentRootPath + "/Photos/" + filename;

                using (var stream = new FileStream(physicalPath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }

                return new JsonResult(filename);
            }
            catch (Exception)
            {

                return new JsonResult("anonymous.png");
            }
        }
    }
}

