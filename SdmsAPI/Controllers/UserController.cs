﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Models;
using Microsoft.Extensions.Primitives;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private UsersBLL _bll;
        private EltelWebApi.Authorization.IJwtUtils _jwtUtils;
        string _entityName = "user";

        public UserController(IConfiguration configuration, IWebHostEnvironment env, IDataProtectionProvider provider, UsersBLL bll, EltelWebApi.Authorization.IJwtUtils jwtUtils)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
            _jwtUtils = jwtUtils;
        }

        [Route("GetUserDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewUser")]
        public JsonResult GetUserDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [AppAuthorizePermission(Permissions = "canViewUser")]
        public JsonResult Get()
        {
            Request.Headers.TryGetValue("Authorization", out StringValues tokenSV);
            string token = "";
            if (tokenSV.Count > 0)
                token = tokenSV.ToString();
            token = token.Split(' ', System.StringSplitOptions.RemoveEmptyEntries)[1];

            var isAdmin = _jwtUtils.HasRoleInToken(token, "DevAdmin") || _jwtUtils.HasRoleInToken(token, "Administrator");

            return new JsonResult(_bll.GetUsers(isAdmin));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewRole")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            Request.Headers.TryGetValue("Authorization", out StringValues tokenSV);
            string token = "";
            if (tokenSV.Count > 0)
                token = tokenSV.ToString();
            token = token.Split(' ', System.StringSplitOptions.RemoveEmptyEntries)[1];

            var isAdmin = _jwtUtils.HasRoleInToken(token, "DevAdmin") || _jwtUtils.HasRoleInToken(token, "Administrator");
            request.IsAdmin = isAdmin;
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }


        [HttpGet("{email}")]
        [AppAuthorizePermission(Permissions = "canViewUser")]
        public JsonResult Get(string email)
        {
            return new JsonResult(_bll.GetUserByEmail(email));
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddUser")]
        public JsonResult Post(User user)
        {
            var refreshToken = Request.Cookies["refreshToken"];
            return new JsonResult(_bll.AddUser(user));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditUser")]
        public JsonResult Put(User user)
        {
            var refreshToken = Request.Cookies["refreshToken"];
            return new JsonResult(_bll.UpdateUser(user));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteUser")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteUser(id));
        }

    }
}
