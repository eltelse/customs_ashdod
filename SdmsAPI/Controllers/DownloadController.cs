﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SdmsAPI.Controllers
{
    [Route("[controller]")]
    [Authorize]
    [ApiController]
    public class DownloadController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private LaneBLL _bll;
        ILogger _logger;

        public DownloadController(IConfiguration configuration, IWebHostEnvironment env, LaneBLL bll, ILogger<DownloadController> logger)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
            _logger = logger;
        }

        //GET api/download/12345abc
        [HttpGet("{path}")]
        public IActionResult Download(string path)
        {
            path = HttpUtility.UrlDecode(path);
            string basePath = "";
            string fullPath = Path.Join(basePath + path);
            string ext = Path.GetExtension(fullPath);
            string contentType = "image/jpeg";
            if (ext == ".pdf") contentType = "application/pdf";
            FileStreamResult file;
            try
            {

                Stream stream = new FileStream(fullPath, FileMode.Open);
                file = File(stream, contentType); // returns a FileStreamResult 


            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Could not find file"))
                {
                    _logger.LogError("Error while downloading file {file}. Message: {Message}", fullPath, ex.Message);
                    return NotFound(ex.Message);
                }

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }


            return file;
        }
    }
}
