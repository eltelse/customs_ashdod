﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Models;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class WorkStationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private WorkStationBLL _bll;

        public WorkStationController(IConfiguration configuration, IWebHostEnvironment env, WorkStationBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetWorkStationsDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewWorkStation")]
        public JsonResult GetWorkStationsDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns("WorkStation"));
        }

        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewWorkStation")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities("work_station"));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewWorkStation")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData("work_station", request));
        }


        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewWorkStation")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById("work_station", id));
        }


        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddWorkStation")]
        public JsonResult Post(WorkStation station)
        {
            return new JsonResult(_bll.AddWorkStation(station));
        }


        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditWorkStation")]
        public JsonResult Put(WorkStation station)
        {
            return new JsonResult(_bll.UpdateWorkStation(station));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteWorkStation")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteWorkStation(id));
        }
    }
}

