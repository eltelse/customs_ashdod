﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using System;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class GenericTypeTablesController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private TypeTablesBLL _bll;

        public GenericTypeTablesController(IConfiguration configuration, IWebHostEnvironment env, TypeTablesBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetTypeTableDisplayColumns")]
        [HttpGet("{GetTypeTableDisplayColumns}/{schema}/{table_name}")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetTypeTableDisplayColumns(string schema, string table_name)
        {
            return new JsonResult(_bll.GetDisplayColumns(table_name));
        }

        [HttpGet("{schema}/{table_name}")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult Get(string schema, string table_name)
        {
            return new JsonResult(_bll.GetTypeTableEntries(schema, table_name));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetTypeTablePagedEntries(request));
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddGenericTypeTables")]
        public JsonResult Post(TypeTable tt)
        {
            return new JsonResult(_bll.AddEntry(tt));
        }


        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditGenericTypeTables")]
        public JsonResult Put(TypeTable tt)
        {
            return new JsonResult(_bll.UpdateEntry(tt));
        }

        [HttpDelete("{id}/{schema}/{table_name}")]
        [AppAuthorizePermission(Permissions = "canDeleteGenericTypeTables")]
        public JsonResult Delete(string id, string schema, string table_name)
        {
            TypeTable tt = new TypeTable()
            {
                Id = Int32.Parse(id),
                Schema = schema,
                TableName = table_name
            };
            return new JsonResult(_bll.DeleteEntry(tt));
        }
    }
}
