﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Models;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private PermissionsBLL _bll;
        string _entityName = "permission";

        public PermissionController(IConfiguration configuration, IWebHostEnvironment env, PermissionsBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetPermissionDisplayColumns")]
        [AppAuthorizePermission(Permissions = "canViewPermission")]
        [HttpGet]
        public JsonResult GetPermissionDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [AppAuthorizePermission(Permissions = "canViewPermission")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }

        [Route("GetPermissionSubjects")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewPermission")]
        public JsonResult GetPermissionSubjects(int id)
        {
            return new JsonResult(_bll.GetEntities("permission_subject"));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewPermission")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewPermission")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddPermission")]
        public JsonResult Post(Permission user)
        {
            return new JsonResult(_bll.AddPermission(user));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditPermission")]
        public JsonResult Put(Permission user)
        {
            return new JsonResult(_bll.UpdatePermission(user));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeletePermission")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeletePermission(id));
        }


    }
}
