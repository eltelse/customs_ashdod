﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private NotificationBLL _bll;
        string _entityName = "notification";

        public NotificationController(IConfiguration configuration, IWebHostEnvironment env, NotificationBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }


        [Route("GetNotificationDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewNotification")]
        public JsonResult GetNotificationDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [AppAuthorizePermission(Permissions = "canViewNotification")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewNotification")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewNotification")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [HttpGet("{id}")]
        [Route("GetUserNotifications/{userId}")]
        [AppAuthorizePermission(Permissions = "canViewRecivedNotification")]
        public JsonResult GetUserNotifications(string userId)
        {
            return new JsonResult(_bll.GetUserNotification(userId));
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddNotification")]
        public JsonResult Post(Notification Notification)
        {
            return new JsonResult(_bll.AddNotification(Notification));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditNotification,canDissmissNotification")]
        public JsonResult Put(Notification Notification)
        {
            return new JsonResult(_bll.UpdateNotification(Notification));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteNotification")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteNotification(id));
        }

    }

}
