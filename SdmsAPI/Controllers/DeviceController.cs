﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Authorization;
using Microsoft.AspNetCore.Authorization;
using SdmsAPI.Models.ViewModels;

namespace SdmsAPI.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private DeviceBLL _bll;
        string _entityName = "device";

        public DeviceController(IConfiguration configuration, IWebHostEnvironment env, DeviceBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetDevicesDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewDevice")]
        public JsonResult GetGetDevicesDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewDevice")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }
        
        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewDevice")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canView")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddDevice")]
        public JsonResult Post(Device vehicle)
        {
            return new JsonResult(_bll.AddDevice(vehicle));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditDevice")]
        public JsonResult Put(Device vehicle)
        {
            return new JsonResult(_bll.UpdateDevice(vehicle));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteDevice")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteDevice(id));
        }

    }
}
