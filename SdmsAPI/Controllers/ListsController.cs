﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Authorization;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ListsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private ListsDataBLL _bll;

        public ListsController(IConfiguration configuration, IWebHostEnvironment env, ListsDataBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [HttpGet]
        [Route("GetCCRCorrectionTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables,canAddInspection")]
        public JsonResult GetCCRCorrectionTypes()
        {
            return new JsonResult(_bll.GetCCRCorrectionTypes());
        }

        [HttpGet]
        [Route("GetLPRCorrectionTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables,canAddInspection")]
        public JsonResult GetLPRCorrectionTypes()
        {
            return new JsonResult(_bll.GetLPRCorrectionTypes());
        }

        [HttpGet]
        [Route("GetConclusionTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables,canAddInspection")]
        public JsonResult GetConclusionTypes()
        {
            return new JsonResult(_bll.GetConclusionTypes());
        }

        [HttpGet]
        [Route("GetInspectionTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables,canAddInspection")]
        public JsonResult GetInspectionTypes()
        {
            return new JsonResult(_bll.GetInspectionTypes());
        }

        [HttpGet]
        [Route("GetNotificationTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetNotificationTypes()
        {
            return new JsonResult(_bll.GetNotificationTypes());
        }

        [HttpGet]
        [Route("GetNotificationPublishTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetNotificationPublishTypes()
        {
            return new JsonResult(_bll.GetNotificationPublishTypes());
        }

        /// <summary>
        /// THis one returs a list of users for display or drop down select display and NOT for users CRUD purpose. That is why it is plased here in Lists Controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUsersList")]
        [AppAuthorizePermission(Permissions = "canViewUser")]
        public JsonResult GetUsersList()
        {
            return new JsonResult(_bll.GetUsersList());
        }

        [HttpGet]
        [Route("GetUserTypes")]
        [AppAuthorizePermission(Permissions = "canViewUser")]
        public JsonResult GetUserTypes()
        {
            return new JsonResult(_bll.GetUserTypes());
        }


        [HttpGet]
        [Route("GetVehicleTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetVehicleTypes()
        {
            return new JsonResult(_bll.GetVehicleTypes());
        }

        [HttpGet]
        [Route("GetWorkStationsTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetWorkStationsTypes()
        {
            return new JsonResult(_bll.GetWorkStationTypes());
        }

        [HttpGet]
        [Route("GetAreaTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetAreaTypes()
        {
            return new JsonResult(_bll.GetAreaTypes());
        }

        [HttpGet]
        [Route("GetDeviceTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetDeviceTypes()
        {
            return new JsonResult(_bll.GetDeviceTypes());
        }

        [HttpGet]
        [Route("GetDeviceEventTypes")]
        [AppAuthorizePermission(Permissions = "canViewGenericTypeTables")]
        public JsonResult GetDeviceEventTypes()
        {
            return new JsonResult(_bll.GetDeviceTypes());
        }
    }
}

