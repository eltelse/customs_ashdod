﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private DashboardBLL _bll;

        public DashboardController(IConfiguration configuration, IWebHostEnvironment env, DashboardBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetSummaryInspectionDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewDashboard")]
        public JsonResult getSummaryInspectionDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns("inspection_summary"));
        }

        [Route("GetInspectionEventLogDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewDashboard")]
        public JsonResult GetInspectionEventLogDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns("inspection_event_log"));
        }

        [Route("GetDashboardData")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewDashboard")]
        public JsonResult GetDashboardData()
        {
            return new JsonResult(_bll.GetDashboardData());
        }


        [HttpPost]
        [Route("GetSummaryInspectionsPagedData")]
        [AppAuthorizePermission(Permissions = "canViewDashboard")]
        public IActionResult GetSummaryInspectionsPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData("dashboard_inspection", request));
        }

        [HttpPost]
        [Route("GetSummaryEventsPagedData")]
        [AppAuthorizePermission(Permissions = "canViewDashboard")]
        public IActionResult GetSummaryEventsPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData("inspection_log_event", request));
        }

        
    }

}
