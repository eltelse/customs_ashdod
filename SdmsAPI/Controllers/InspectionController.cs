﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using SdmsAPI.Models.ViewModels;
using System.Linq;
using EltelWebApi.Authorization;
using EltelWebApi.Core;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class InspectionController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private InspectionBLL _bll;
        string _entityName = "inspection";


        public InspectionController(IConfiguration configuration, IWebHostEnvironment env, InspectionBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetInspectionDisplayColumns")]
        [AppAuthorizePermission(Permissions = "canViewInspection")]
        public JsonResult GetInspectionDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [AppAuthorizePermission(Permissions = "canViewInspection")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewInspection")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewInspection")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [HttpGet]
        [Route("GetCheckinData/{id}")]
        [AppAuthorizePermission(Permissions = "canViewCheckinModal")]
        public IActionResult GetCheckinData(string id)
        {
            // Note: JsonResult is used to serialize an object to json/ return jsom fro DataTabel or an object.
            // Note: GetCheckinDataById returns a well formed json string. No need to JsonResult it!!
            //return new JsonResult(_bll.GetCheckinDataById(id));
            return Ok(_bll.GetCheckinDataById(id));
        }

        [HttpGet]
        [Route("GetCheckinManifestData/{id}")]
        [AppAuthorizePermission(Permissions = "canViewCheckinModal")]
        public IActionResult GetCheckinManifestData(string id)
        {
            // Note: JsonResult is used to serialize an object to json/ return jsom fro DataTabel or an object.
            // Note: GetCheckinDataById returns a well formed json string. No need to JsonResult it!!
            //return new JsonResult(_bll.GetCheckinManifestDataById(id));
            return Ok(_bll.GetCheckinManifestDataById(id));
        }


        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddInspection")]
        public JsonResult Post(Inspection Inspection)
        {
            return new JsonResult(_bll.AddInspection(Inspection));
        }

        [HttpGet]
        [Route("AskForBypass/{id}")]
        [AppAuthorizePermission(Permissions = "canAskCheckinInspectionBypass")]
        public JsonResult AskForBypass(string id)
        {
            var token = Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            return new JsonResult(_bll.AskForBypass(id, token));
        }

        /// <summary>
        /// Make a post to the peripheral service running on checkin workstation that will run the scanner and post back the image.
        /// </summary>
        /// <param name="identifier">Inspection identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("AskForManifestScan/{identifier}")]
        [AppAuthorizePermission(Permissions = "canEditInspection")]
        public JsonResult AskForManifestScan(string identifier)
        {
            return new JsonResult(_bll.AskForManifestScan(identifier));
        }
        
        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditInspection")]
        public JsonResult Put(Inspection Inspection)
        {
            return new JsonResult(_bll.UpdateInspection(Inspection));
        }

        [HttpPut]
        [Route("StartInspection")]
        [AppAuthorizePermission(Permissions = "canEditInspection,canStartInspection")]
        public JsonResult StartInspection(CheckinVM obj)
        {
            // In case WorkstationIP was sent it is probably because it is a site manager starting the inspection instead of a checkin operator.
            // If that is the case we need tha IP of the checkin workstation and not the IP of the site manager's workstation, in order to properly determin the NextArea for that truck
            if (string.IsNullOrEmpty(obj.WorkstationIP) && string.IsNullOrEmpty(obj.DEV_IP))
                obj.WorkstationIP = GetIpAddress();

            // TODO: Consider to remove in prod!!! when NOT localhost
            else if (!string.IsNullOrEmpty(obj.DEV_IP))
                obj.WorkstationIP = obj.DEV_IP;

            return new JsonResult(_bll.StartInspection(obj));
        }


        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteInspection")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteInspection(id));
        }

        private string GetIpAddress()
        {
            // Get source ip address for the current request
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

        [HttpGet]
        [Route("GetInspectionTypes")]
        [AppAuthorizePermission(Permissions = "canViewCheckinModalMetaData")]
        public JsonResult GetInspectionTypes()
        {
            return new JsonResult(_bll.GetInspectionTypes());
        }

    }

}
