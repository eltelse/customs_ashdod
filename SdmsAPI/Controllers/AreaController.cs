﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace SdmsAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class AreaController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private AreaBLL _bll;
        string _entityName = "area";

        public AreaController(IConfiguration configuration, IWebHostEnvironment env, AreaBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetAreasDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewArea")]
        public JsonResult GetGetAreasDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewArea")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewArea")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewArea")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [Route("GetCheckinAreas")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewArea")]
        public JsonResult GetCheckinAreas(string id)
        {
            return new JsonResult(_bll.GetCheckinAreas());
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddArea")]
        public JsonResult Post(Area obj)
        {
            return new JsonResult(_bll.AddArea(obj));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditArea")]
        public JsonResult Put(Area obj)
        {
            return new JsonResult(_bll.UpdateArea(obj));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteArea")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteArea(id));
        }

    }
}
