﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Models;
using Microsoft.AspNetCore.Hosting;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Authorization;
using Microsoft.AspNetCore.Authorization;
using SdmsAPI.Models.ViewModels;

namespace SdmsAPI.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class LaneController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private LaneBLL _bll;
        string _entityName = "lane";

        public LaneController(IConfiguration configuration, IWebHostEnvironment env, LaneBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetLanesDisplayColumns")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewLane")]
        public JsonResult GetGetLanesDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewLane")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewLane")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewLane")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddLane")]
        public JsonResult Post(Lane vehicle)
        {
            return new JsonResult(_bll.AddLane(vehicle));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditLane")]
        public JsonResult Put(Lane vehicle)
        {
            return new JsonResult(_bll.UpdateLane(vehicle));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteLane")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteLane(id));
        }

    }
}
