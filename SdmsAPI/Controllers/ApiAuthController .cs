﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using EltelWebApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Helpers;
using EltelWebApi.Models;
using EltelWebApi.Services;

namespace EltelWebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ApiAuthController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private IApiAuthService _authService;
        public ApiAuthController(IOptions<AppSettings> appSettings, IApiAuthService authService)
        {
            _authService = authService;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(ApiLoginAuthRequest loginAuthReq)
        {
            ApiLoginAuthResponse authObj = _authService.Authenticate(new ApiLoginAuthRequest() { client_id = loginAuthReq.client_id, client_secret = loginAuthReq.client_secret }, GetIpAddress());
            ApiLoginAuthResponse apiRespons = new ApiLoginAuthResponse() { AccessToken = authObj.AccessToken, ExpirationSeconds = authObj.ExpirationSeconds };
            return Ok(apiRespons);
        }

        private string GetIpAddress()
        {
            // Get source ip address for the current request
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}
