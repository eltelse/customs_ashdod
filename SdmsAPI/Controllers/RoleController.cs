﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Models;
using EltelWebApi.Authorization;
using SdmsAPI.Models.ViewModels;

namespace EltelWebApi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        private RolesBLL _bll;
        string _entityName = "role";

        public RoleController(IConfiguration configuration, IWebHostEnvironment env, RolesBLL bll)
        {
            _configuration = configuration;
            _env = env;
            _bll = bll;
        }

        [Route("GetRoleDisplayColumns")]
        [AppAuthorizePermission(Permissions = "canViewRole")]
        [HttpGet]
        public JsonResult GetRoleDisplayColumns()
        {
            return new JsonResult(_bll.GetDisplayColumns(_entityName));
        }

        [AppAuthorizePermission(Permissions = "canViewRole")]
        public JsonResult Get()
        {
            return new JsonResult(_bll.GetEntities(_entityName));
        }

        [HttpPost]
        [Route("GetPagedData")]
        [AppAuthorizePermission(Permissions = "canViewRole")]
        public IActionResult GetPagedData(PagedDataRequest request)
        {
            return Ok(_bll.GetEntitiesPagedData(_entityName, request));
        }

        [HttpGet("{id}")]
        [AppAuthorizePermission(Permissions = "canViewRole")]
        public JsonResult Get(string id)
        {
            return new JsonResult(_bll.GetEntitiesById(_entityName, id));
        }

        [Route("GetRolePermissions/{id}")]
        [HttpGet]
        [AppAuthorizePermission(Permissions = "canViewPermission")]
        public JsonResult GetRolePermissions(int id)
        {
            return new JsonResult(_bll.GetRolePermissions(id));
        }

        [Route("UpdateRolePermissions")]
        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditPermission")]
        public JsonResult UpdateRolePermissions(RolePermissionVM userRolePermission)
        {
            return new JsonResult(_bll.UpdateRolePermissions(userRolePermission));
        }


        [HttpPost]
        [AppAuthorizePermission(Permissions = "canAddRole")]
        public JsonResult Post(Role role)
        {
            return new JsonResult(_bll.AddRole(role));
        }

        [HttpPut]
        [AppAuthorizePermission(Permissions = "canEditRole")]
        public JsonResult Put(Role role)
        {
            return new JsonResult(_bll.UpdateRole(role));
        }

        [HttpDelete("{id}")]
        [AppAuthorizePermission(Permissions = "canDeleteRole")]
        public JsonResult Delete(string id)
        {
            return new JsonResult(_bll.DeleteRole(id));
        }

    }
}
