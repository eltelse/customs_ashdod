﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EltelWebApi.Core.BusinessLogic;
using EltelWebApi.Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Security.Claims;
using EltelWebApi.Services;
using EltelWebApi.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace EltelWebApi.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private IAuthService _authService;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IOptions<AppSettings> appSettings, IAuthService authService, ILogger<AuthController> logger)
        {
            _authService = authService;
            _appSettings = appSettings.Value;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(LoginAuthRequest loginAuthReq)
        {
            _logger.LogTrace("Authenticate - Start");

            _logger.LogTrace($"login auth request details: Email - {loginAuthReq.Email}, Pass - ******");

            var workstationIP = GetIpAddress();
            _logger.LogTrace($"WorkstationIP address - {workstationIP}");

            // TODO: Consider to remove in prod!!! when NOT localhost
            if (!string.IsNullOrEmpty(loginAuthReq.DEV_IP))
            {
                _logger.LogTrace($"Getting WorkstationIP address from - loginAuthReq.DEV_IP");
                workstationIP = loginAuthReq.DEV_IP;
                _logger.LogTrace($"WorkstationIP address - {workstationIP}");
            }

            _logger.LogInformation("Calling _authService.Authenticate to get new JWT and refresh token and user data (details and permissions)");
            LoginAuthResponse response = _authService.Authenticate(loginAuthReq, workstationIP);

            _logger.LogTrace($"login auth response details: Status - {response.Status}, FullName - {response.FullName}");

            SetTokenCookie(response.RefreshToken, loginAuthReq.DEV_IP);

            _logger.LogTrace("Authenticate - End. Reurning responce.");

            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("refresh-token")]
        public IActionResult RefreshToken()
        {
            _logger.LogTrace("RefreshToken - Start");

            LoginAuthResponse response = null;
            var refreshToken = Request.Cookies["refreshToken"];

            _logger.LogTrace($"Current RefreshToken - {refreshToken}");

            var dev_ip = Request.Cookies["DEV_IP"];
            var workstationIP = GetIpAddress();

            _logger.LogTrace($"WorkstationIP address - {workstationIP}");

            // TODO: Consider to remove in prod!!! when NOT localhost
            if (!string.IsNullOrEmpty(dev_ip))
            {
                _logger.LogTrace($"Getting WorkstationIP address from - loginAuthReq.DEV_IP");
                workstationIP = dev_ip;
                _logger.LogTrace($"WorkstationIP address - {workstationIP}");
            }

            try
            {
                _logger.LogTrace($"Trying to refresh JWT token with Current RefreshToken");
                response = _authService.RefreshToken(refreshToken, workstationIP);
                SetTokenCookie(response.RefreshToken, dev_ip);
            }
            catch (Exception ex)
            {
                return ValidationProblem(ex.Message);
            }

            return Ok(response);
        }

        [HttpPost("revoke-token")]
        public IActionResult RevokeToken(TokenRevokationRequest revocationReq)
        {
            _logger.LogTrace("RevokeToken - Start");
            // Accept refresh token in request body or cookie
            var token = revocationReq.Token ?? Request.Cookies["refreshToken"];
            var dev_ip = revocationReq.DEV_IP ?? Request.Cookies["DEV_IP"];
            var dev_ip_msg = string.IsNullOrEmpty(dev_ip) ? "Unknown" : dev_ip;
            _logger.LogTrace($" dev_ip is - {dev_ip_msg}");

            if (string.IsNullOrEmpty(token))
                return BadRequest(new { message = "Token is required" });

            var revokedWorkstationIP = GetIpAddress();
            // TODO: Consider to remove in prod!!! when NOT localhost
            if (!string.IsNullOrEmpty(dev_ip))
                revokedWorkstationIP = dev_ip;

            _logger.LogTrace($"revokedWorkstationIP is - {revokedWorkstationIP}");

            _logger.LogTrace($"Calling _authService.RevokeToken");
            _authService.RevokeToken(token, revokedWorkstationIP);

            _logger.LogTrace("RevokeToken - End. Returning OK");
            return Ok(new { message = "Token revoked" });
        }

        [HttpPost("admin-revoke-user-token")]
        public IActionResult AdminRevokeUserToken()
        {
            // TODO: Add logic to revoke a token by userId that should be providede along with the revoker data
            return Ok(new { message = "Token revoked" });
        }



        private void SetTokenCookie(string token, string dev_ip = null)
        {
            // append cookie with refresh token to the http response
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(_appSettings.REFRESH_TOKEN_DAYS),
                SameSite = SameSiteMode.Strict
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
            // TODO: Remove if not needed. This is for dev and testing
            if (!string.IsNullOrEmpty(dev_ip))
                Response.Cookies.Append("DEV_IP", dev_ip, cookieOptions);
        }

        private string GetIpAddress()
        {
            // Get source ip address for the current request
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}

