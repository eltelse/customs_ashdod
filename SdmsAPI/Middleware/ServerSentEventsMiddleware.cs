﻿using EltelWebApi.Authorization;
using EltelWebApi.Helpers;
using EltelWebApi.Services;
using EltelWebApi.SSE;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Middleware
{
    // See: https://www.tpeczek.com/2017/02/server-sent-events-sse-support-for.html
    public class ServerSentEventsMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings _appSettings;
        ILogger _logger;
        public ServerSentEventsMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, ILogger<ServerSentEventsMiddleware> logger)
        {
            _appSettings = appSettings.Value;
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, IServerSentEventsService serverSentEventsService, IJwtUtils jwtUtils)
        {

            if (context.Request.Headers["Accept"] == "text/event-stream")
            {
                context.Response.ContentType = "text/event-stream";
                context.Response.Headers.Add("Access-Control-Allow-Origin", _appSettings.Allowed_Origin_Notification);

                await context.Response.Body.FlushAsync();

                var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
                var userId = jwtUtils.ValidateJwtToken(token);
                int uId = userId.HasValue ? userId.Value : -1;

                // Alternative to above issue - we add uid as query parameter
                if (uId == -1)
                {
                    var id = context.Request.Query.First(q => q.Key == "uid").Value;
                    Int32.TryParse(id, out uId);
                }

                var roleId = context.Request.Query.First(q => q.Key == "rid").Value;
                Int32.TryParse(roleId, out int rId);

                ServerSentEventsClient client = new ServerSentEventsClient(context.Response, uId, rId);
                Guid clientId = serverSentEventsService.AddClient(client);

                context.RequestAborted.WaitHandle.WaitOne();

                serverSentEventsService.RemoveClient(clientId);

                await Task.FromResult(true);
            }
            else
            {
                await _next(context);
            }
        }
    }
    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class SSEMyMiddlewareExtensions
    {
        public static IApplicationBuilder UseSSEMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ServerSentEventsMiddleware>();
        }
    }
}
