﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models
{
    public class Device
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public string Identifier { set; get; }
        public string HostIp { set; get; }
        public string DeviceIp { set; get; }
        public string DirectoryPatternSource { set; get; }
        public string DirectoryPatternTarget { set; get; }
        public int DeviceTypeId { get; set; }
        public int LaneId { get; set; }
        public bool? IsActive { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool IsDeleted { get; set; }
    }
}
