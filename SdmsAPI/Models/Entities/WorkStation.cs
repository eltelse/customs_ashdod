﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models
{
    public class WorkStation
    {
        public int Id { get; set; }
        public string Name { set; get; }
        public string Description { set; get; }
        public string IP { set; get; }
        public string MAC { set; get; }
        public int WorkStationTypeId{ set; get; }
        public string Type { set; get; }
        public string TypeDescription { set; get; }
        public bool? IsActive { set; get; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
