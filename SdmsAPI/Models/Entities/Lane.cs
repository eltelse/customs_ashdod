﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models
{
    public class Lane
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public int AreaId { get; set; }
        public int MaxQueueQuantity { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool IsDeleted { get; set; }
    }
}
