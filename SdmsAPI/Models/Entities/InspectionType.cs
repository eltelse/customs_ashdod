﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models
{
    public class InspectionType
    {
        public string Id { set; get; }
        public string Name {set;get;}
    }
}
