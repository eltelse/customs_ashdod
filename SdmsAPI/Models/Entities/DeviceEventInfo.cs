﻿using System;
using System.Collections.Generic;

namespace SdmsAPI.Core
{
    public class DeviceEventInfo
    {

        public string Id { get; set; }
        public string InspectionIdentifier { get; set; }
        public int DeviceId { get; set; }
        public string DeviceIdentifier { get; set; }
        public int DeviceEventTypeId { get; set; }
        public string LPR { get; set; }
        public int LPRConfidence { get; set; }
        public string CCR { get; set; }
        public int CCRConfidence { get; set; }
        public string ConclusionType { get; set; }
        public int ConclusionTypeId { get; set; }
        public string Timestamp { get; set; }
        public DeviceFilesInfo Files { get; set; }
        public string DriverTagNumber { get; set; }
        public string ScanId { get; set; }
        public string UserId { get; set; }
        public string WorkstationIP { get; set; }
        public int WorkflowTypeId { get; set; }
        public string ScannerIdentifier { get; set; }
        public string AreaIdentifier { get; set; }

        /*
         public Guid Id { get; set; }
        public int LaneId { get; set; }
        public int PlateType { get; set; }
        public AuthorizationType Authorized { get; set; }
        public string Code { get; set; }
        public int Confidence { get; set; }
        public string DetectedCode { get; set; }
        public int DetectedCodeConfidence { get; set; }
        public DateTime Time { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public int ModelConfidence { get; set; }
        public int MakeConfidence { get; set; }
        public int Speed { get; set; }
        public int SpeedConfidence { get; set; }
        public string Jurisdiction { get; set; }
        public int JurisdictionConfidence { get; set; }
        public string CarDirection { get; set; }
        public string CarType { get; set; }
        public int CarTypeConfidence { get; set; }
        public string Orientation { get; set; }
        public int OrientationConfidence { get; set; }
        public int Occupancy { get; set; }
        public int OccupancyConfidence { get; set; }
        public string Class { get; set; }
        public int ClassConfidence { get; set; }
        public string Color { get; set; }
        public int ColorConfidence { get; set; }
        public string Location { get; set; }
        public int LocationConfidence { get; set; }
        public ImageInfo[] Images { get; set; }
        public VideoClipInfo[] VideoClipsInfo { get; set; }
        public Guid[] HotListHitIds { get; set; }
        public HotlistFilterType HotlistHitFilterType { get; set; }
        public ExtensionObject Extensions { get; set; }
        public bool IsFinal { get; set; }
        public int LaneEventCreationIndex { get; set; }
        public string AreaLocationExternalId { get; set; }
        public string Comments { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }

         */
    }

 
    public class DeviceFilesInfo
    {
        public List<DeviceFileInfo> LPR { get; set; }
        public List<DeviceFileInfo> CCR { get; set; }
        public List<DeviceFileInfo> Manifest { get; set; }
        public List<DeviceFileInfo> IAW { get; set; }
    }


    public class DeviceFileInfo 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OwnerDeviceIdentifier { get; set; }
        public string Device { get; set; }
        public string Path { get; set; }
        public string Base64 { get; set; }
        public string TargetPath { get; set; }
        public string Value { get; set; }
        public int Qualifier { get; set; }
        public string Blob { get; set; }
    }
}