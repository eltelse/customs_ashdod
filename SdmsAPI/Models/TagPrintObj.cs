﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models
{
    public class TagPrintObjItem
    {
        public string Title { get; set; }
        public string Value { get; set; }
        public bool IsValueBold { get; set; }
        public bool IsValueHeb { get; set; }
    }
}
