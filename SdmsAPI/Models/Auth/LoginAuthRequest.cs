using System.ComponentModel.DataAnnotations;

namespace EltelWebApi.Models
{
    public class LoginAuthRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string DEV_IP { get; set; }
    }
}