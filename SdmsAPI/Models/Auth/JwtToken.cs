﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models.Auth
{
    public class JwtToken
    {
        public string Token { get; set; }
        public int ExpirationSeconds { get; set; }
    }
}
