using EltelWebApi.Models;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System;

namespace EltelWebApi.Models
{
    public class LoginAuthResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string JwtToken { get; set; }
        public int ExpirationSeconds { get; set; }
        public RoleVM[] Roles { get; set; }
        public PermissionVM[] Permissions { get; set; }
        public string Status { get; set; }

        [JsonIgnore] // refresh token is returned in http only cookie
        public string RefreshToken { get; set; }

        public LoginAuthResponse(UserAuthVM vm, string jwtToken, int expSeconds, string refreshToken)
        {
            Id = vm.Details.Id;
            FirstName = vm.Details.FirstName;
            LastName = vm.Details.LastName;
            FullName = vm.Details.FullName;
            Email = vm.Details.Email;
            JwtToken = jwtToken;
            ExpirationSeconds = expSeconds;
            RefreshToken = refreshToken;
            Roles = vm.Roles;
            Permissions = vm.Permissions;
        }

        public LoginAuthResponse(UserVM vm, string jwtToken, int expSeconds, string refreshToken)
        {
            Id = vm.Id;
            FirstName = vm.FirstName;
            LastName = vm.LastName;
            FullName = vm.FullName;
            Email = vm.Email;
            JwtToken = jwtToken;
            ExpirationSeconds = expSeconds;
            RefreshToken = refreshToken;
        }
    }

}