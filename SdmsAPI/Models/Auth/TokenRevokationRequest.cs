namespace EltelWebApi.Models
{
    public class TokenRevokationRequest
    {
        public string Token { get; set; }
        public string DEV_IP { get; set; }
    }
}