﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models.Auth
{
    public class ApiLoginAuthRequest
    {
        public string client_id { get; set; }
        public string client_secret { get; set; }
    }
}
