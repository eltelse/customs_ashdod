﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models.Auth
{
    public class ApiLoginAuthResponse
    {
        public string AccessToken { get; set; }
        public int ExpirationSeconds { get; set; }

    }
}
