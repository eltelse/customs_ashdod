﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.SSE
{
    public class ServerSentEvent
    {
        public string Id { get; set; }

        public string Type { get; set; }

        public IList<string> Data { get; set; }

        public int UserId { get; set; }
        public int RoleId { get; set; }
        public bool SendAll { get; set; }
    }
}
