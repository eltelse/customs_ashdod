﻿using EltelWebApi.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.SSE
{
    public class ServerSentEventsClient
    {
        private readonly HttpResponse _response;
        public int UserId { get; }
        public int RoleId { get; }

        internal ServerSentEventsClient(HttpResponse response, int userId, int roleId)
        {
            _response = response;
            UserId = userId;
            RoleId = roleId;
        }

        public Task SendEventAsync(ServerSentEvent serverSentEvent)
        {
            return _response.WriteSseEventAsync(serverSentEvent);
        }
    }
}
