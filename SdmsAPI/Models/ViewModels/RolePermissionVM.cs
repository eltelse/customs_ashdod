﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models
{
    public class RolePermissionVM
    {
        public int roleId { get; set; }
        public List<PermissionSubject> permissionSubjects { get; set; }

        public class PermissionSubject
        {
            public Subject subject { get; set; }
        }
        public class Subject
        {
            public int id { get; set; }
            public string name { get; set; }
            public List<PermissionsObj> permissions { get; set; }
        }

        public class PermissionsObj
        {
            public string name { get; set; }
            public int id { get; set; }
            public object isChecked { get; set; }
        }
  
    }
}
