﻿using EltelWebApi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models.ViewModels
{
    public class PagedDataRequest
    {
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public string SortCol { get; set; }
        public SortType SortType { get; set; }
        public string FilterTerm { get; set; }
        public string Schema { get; set; }
        public string TableName { get; set; }
        public bool IsAdmin { get; set; }
    }
}
