﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models.ViewModels
{
    public class AddNotificationResultVM
    {
        public string Message { get; set; }
        public int Id { get; set; }
        public bool AutoPopup { get; set; }
        public bool IsDanger { get; set; }
    }
}
