﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models.ViewModels
{
    public class CheckinTagPrintVm
    {
        public int id { get; set; }
        public string Identifier { get; set; }
        public string DriverTagCode { get; set; }
        public string LPR { get; set; }
        public string CCR { get; set; }
        public string NextAreaDescription { get; set; }
        public string WorkflowType { get; set; }
        public string QueueNumber { get; set; }
        public string Arrival { get; set; }
    }

}
