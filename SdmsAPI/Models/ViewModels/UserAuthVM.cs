﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EltelWebApi.Core;

namespace EltelWebApi.Models
{
    public class UserAuthVM
    {
        public UserVM Details { get; set; }
        public RoleVM[] Roles { get; set; }
        public PermissionVM[] Permissions { get; set; }
        public UserType UserType { get; set; }

    }
}
