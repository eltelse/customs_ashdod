﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models.ViewModels
{
    public class CheckinVM
    {
        public int id { get; set; }
        public string Identifier { get; set; }
        public int WorkflowTypeId { get; set; }
        public string LPR { get; set; }
        public int LPRQualifier { get; set; }
        public int LPRCorrectionTypeId { get; set; }
        public string CCR { get; set; }
        public int CCRQualifier { get; set; }
        public int CCRCorrectionTypeId { get; set; }
        public string ManifestCode { get; set; }
        public string WorkstationIP { get; set; }
        public string Remark { get; set; }
        public Images Images { get; set; }
        public string DEV_IP { get; set; }
    }

    public class Images
    {
        public List<ImageDataItem> LPR { get; set; }
        public List<ImageDataItem> CCR { get; set; }
        public List<ImageDataItem> Manifest { get; set; }
        public List<ImageDataItem> IAW { get; set; }
    }

    public class ImageDataItem
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string src { get; set; }
        public string device { get; set; }
        public string value { get; set; }
        public int qualifier { get; set; }
    }

    
}
