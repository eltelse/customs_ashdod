﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models
{
    public class UserRoleVM
    {
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool Checked { get; set; }
    }


    public class UserRoleConnectionsViewModel
    {
        public int UserId { get; set; }
        public List<UserRoleVM> UserRolesViewModelList { get; set; }
        

        public UserRoleConnectionsViewModel()
        {
            UserRolesViewModelList = new List<UserRoleVM>();
            
        }
    }
}
