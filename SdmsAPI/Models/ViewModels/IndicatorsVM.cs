﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models.ViewModels
{
    public class IndicatorsVM
    {
         public NameValue[] TotalScanned { get; set; }
         public NameValue[] TotalManualChecked { get; set; }
         public NameValue[] TotalLeft { get; set; }
         public NameValue[] TotalCarCarrier { get; set; }
  
    }

    public class NameValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
