﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models.ViewModels
{
    public class AddLprEventResult
    {
        public string Message { get; set; }
        public bool AllowedExit { get; set; }
    }
}
