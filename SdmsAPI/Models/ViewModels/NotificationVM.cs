﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdmsAPI.Models.ViewModels
{
    public class NotificationVM
    {
        public int Id { get; set; }
        public int NotificationTypeId { get; set; }
        public string NotificationType { get; set; }
        public int NotificationPublishTypeId { get; set; }
        public string NotificationPublishType { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string JsonData { get; set; }
        public int SourceUserId { get; set; }
        public string SourceUserFullName { get; set; }
        public int TargetId { get; set; }
        public string TargetName { get; set; }
        public string Reply { get; set; }
        public bool? IsReplyable { get; set; }
        public bool? IsHandled { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool? IsDeleted { get; set; }
        public bool AutoPopup { get; set; }
        public bool IsDanger { get; set; }
    }
}
