﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Models
{
    public class RoleVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
