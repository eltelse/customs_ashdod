﻿using BCryptNet = BCrypt.Net.BCrypt;
using Microsoft.Extensions.Options;
using EltelWebApi.Models.Auth;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EltelWebApi.Authorization;
using EltelWebApi.DataAccess;
using EltelWebApi.Helpers;
using EltelWebApi.Models;
using Newtonsoft.Json;
using EltelWebApi.Core;
using System.Net;

namespace EltelWebApi.Services
{

    public interface IApiAuthService
    {
        ApiLoginAuthResponse Authenticate(ApiLoginAuthRequest loginAuthModel, string ipAddress);
    }


    public class ApiAuthService : IApiAuthService
    {

        private IJwtUtils _jwtUtils;
        private readonly AppSettings _appSettings;
        private readonly IDal _dal;

        public ApiAuthService(
            IDal dal,
            IJwtUtils jwtUtils,
            IOptions<AppSettings> appSettings)
        {
            _dal = dal;
            _jwtUtils = jwtUtils;
            _appSettings = appSettings.Value;
        }

        public ApiLoginAuthResponse Authenticate(ApiLoginAuthRequest loginAuthModel, string ipAddress)
        {
            var user = GetServiceAccountByClientId(loginAuthModel.client_id);

            // validate
            if (user == null || !BCryptNet.Verify(loginAuthModel.client_secret, user.Password))
                throw new AppException("Invalid api client credentials", HttpStatusCode.BadRequest);

            var userAuthVM = GetUserAuthVM(user.Id);
            userAuthVM.UserType = UserType.Service;
            var jwtToken = _jwtUtils.GenerateJwtToken(userAuthVM);

            var res = new ApiLoginAuthResponse() { AccessToken = jwtToken.Token, ExpirationSeconds = jwtToken.ExpirationSeconds };

            return res;
        }

        private User GetServiceAccountByClientId(string clientId)
        {
            try
            {
                SqlParameter[] pArr = { new SqlParameter("email", SqlDbType.VarChar, 250) { Value = clientId } };

                var table = _dal.GetTable("sp_user_validate", pArr);

                if (table.Rows.Count > 0)
                {
                    return DalHelper.DataRowToObject<User>(table.Rows[0]);
                }
                else return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private UserAuthVM GetUserAuthVM(int id)
        {
            SqlParameter[] pArr =
                   {
                        new SqlParameter("id", SqlDbType.Int) { Value = id },
                        new SqlParameter("json", SqlDbType.NVarChar, -1) { Direction = ParameterDirection.Output } // -1 is max
                    };
            var resParams = _dal.ExecuteSPGetResalutValues("sp_get_user_auth_json_object", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
            return JsonConvert.DeserializeObject<UserAuthVM>(json);
        }

    }
}
