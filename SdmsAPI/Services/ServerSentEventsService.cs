﻿using EltelWebApi.Core;
using EltelWebApi.Middleware;
using EltelWebApi.Models;
using EltelWebApi.SSE;
using SdmsAPI.Models.ViewModels;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltelWebApi.Services
{
    public interface IServerSentEventsService
    {
        Guid AddClient(ServerSentEventsClient client);
        void RemoveClient(Guid clientId);
        Task SendEventAsync(ServerSentEvent serverSentEvent);
        Task SendEventAsync(NotificationVM n);

    }


    public class ServerSentEventsService : IServerSentEventsService
    {
        private readonly ConcurrentDictionary<Guid, ServerSentEventsClient> _clients = new();
        
        public Guid AddClient(ServerSentEventsClient client)
        {
            Guid clientGuid = Guid.NewGuid();

            if (!_clients.TryAdd(clientGuid, client))
            {
                return Guid.Empty;
            }

            return clientGuid;
        }

        public void RemoveClient(Guid clientId)
        {
            ServerSentEventsClient client;

            _clients.TryRemove(clientId, out client);
        }

        public Task SendEventAsync(ServerSentEvent serverSentEvent)
        {
            List<Task> clientsTasks = new();
            var list = _clients.Values.Where(q => q.UserId == serverSentEvent.UserId || q.RoleId == serverSentEvent.RoleId || serverSentEvent.SendAll).Select(c => c).ToList();
            foreach (ServerSentEventsClient client in list)
            {
                clientsTasks.Add(client.SendEventAsync(serverSentEvent));
            }

            return Task.WhenAll(clientsTasks);
        }

        public Task SendEventAsync(NotificationVM n)
        {
            var eventType = (NotificationType)n.NotificationTypeId;
            string eventName = eventType.ToString().ToLower();
            var dataItem = GetDataList(n);

            var userId = n.NotificationPublishTypeId == 1 ? n.TargetId : -1;
            var roleId = n.NotificationPublishTypeId == 2 ? n.TargetId : -1;
            var sendAll = userId == -1 && roleId == -1;

            return SendEventAsync(new ServerSentEvent()
            {
                Id = n.Id.ToString(),
                Type = eventName,
                Data = dataItem,
                UserId = userId,
                RoleId = roleId,
                SendAll = sendAll
            });


        }

        private IList<string> GetDataList(NotificationVM n)
        {
            IList<string> lst = new List<string>();
            lst.Add($"\"Id\": \"{n.Id}\", ");
            lst.Add($"\"NotificationTypeId\": \"{n.NotificationTypeId}\", ");
            var notificationType = ((NotificationType)n.NotificationTypeId).ToString();
            lst.Add($"\"NotificationType\": \"{notificationType}\", ");
            lst.Add($"\"AutoPopup\": \"{n.AutoPopup.ToString().ToLower()}\", ");
            lst.Add($"\"IsDanger\": \"{n.IsDanger.ToString().ToLower()}\", ");
            lst.Add($"\"NotificationPublishTypeId\": \"{n.NotificationPublishTypeId}\", ");
            lst.Add($"\"SourceUserId\": \"{n.SourceUserId}\", ");
            lst.Add($"\"SourceUserFullName\": \"{n.SourceUserFullName}\", ");
            lst.Add($"\"Subject\": \"{n.Subject}\", ");
            lst.Add($"\"Message\": \"{n.Message}\", ");
            lst.Add($"\"TargetId\": \"{n.TargetId}\", ");
            lst.Add($"\"TargetName\": \"{n.TargetName}\", ");
            lst.Add($"\"Reply\": \"{n.Reply}\", ");
            var isReplyable = n.IsReplyable.HasValue ? n.IsReplyable.Value.ToString().ToLower() : "true";
            lst.Add($"\"IsReplyable\": \"{isReplyable}\", ");
            var isHandled = n.IsHandled.HasValue ? n.IsHandled.Value.ToString().ToLower() : "false";
            lst.Add($"\"IsHandled\": \"{isHandled}\", ");
            n.JsonData = !string.IsNullOrEmpty(n.JsonData) ? n.JsonData : "{ }";
            if (n.JsonData != default) lst.Add($"\"JsonData\": {n.JsonData}, ");
            lst.Add($"\"NoCommaEndingLine\": \"\" ");

            return lst;

        }

}
}
