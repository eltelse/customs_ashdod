using BCryptNet = BCrypt.Net.BCrypt;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using EltelWebApi.Helpers;
using EltelWebApi.Authorization;
using EltelWebApi.Models;
using EltelWebApi.DataAccess;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using EltelWebApi.Core;
using Microsoft.Extensions.Logging;
using System.Net;

namespace EltelWebApi.Services
{
    public interface IAuthService
    {
        LoginAuthResponse Authenticate(LoginAuthRequest loginAuthModel, string ipAddress);
        LoginAuthResponse RefreshToken(string currentRefreshToken, string ipAddress);
        void RevokeToken(string token, string revokedWorkstationIP);
        //IEnumerable<User> GetUsers();
        //User GetUserById(int id);
    }

    public class AuthService : IAuthService
    {
        private IJwtUtils _jwtUtils;
        private readonly AppSettings _appSettings;
        private readonly IDal _dal;
        ILogger _logger;

        public AuthService(
            IDal dal,
            IJwtUtils jwtUtils,
            IOptions<AppSettings> appSettings,
            ILogger<AuthService> logger)
        {
            _dal = dal;
            _jwtUtils = jwtUtils;
            _appSettings = appSettings.Value;
            _logger = logger;
        }

        public LoginAuthResponse Authenticate(LoginAuthRequest loginAuthModel, string ip)
        {
            _logger.LogTrace("AuthService.Authenticate - Start");
            _logger.LogTrace($"Trying to find user by email - loginAuthModel.Email");
            var user = GetUserByEmail(loginAuthModel.Email);

            // validate
            if (user == null || !BCryptNet.Verify(loginAuthModel.Password, user.Password))
            {
                _logger.LogTrace("User was not found or passwork dows not match");
                string msg = $"Login failure. Invalid username or password for login attempt with the email: {loginAuthModel.Email}";
                _logger.LogTrace(msg);
                _logger.LogTrace("Throwing a NetworkAuthenticationRequired error");
                throw new AppException(msg, "Login failed. Invalid username or password.", (int)InternalErrorCodes.LoginFailure_InvalidUserOrPass, HttpStatusCode.NetworkAuthenticationRequired);
            }


            var userAuthVM = GetUserAuthVM(user.Id);
            userAuthVM.UserType = UserType.User;

            _logger.LogTrace($"User is valid. Full Name - {userAuthVM.Details.FullName}");

            // Authentication successful so generate jwt and refresh tokens
            _logger.LogTrace("Trying to generate a JWT token for the user");
            var jwtToken = _jwtUtils.GenerateJwtToken(userAuthVM);
            _logger.LogTrace("Trying to generate a new refresh token for the user");
            RefreshToken refreshToken = AddRefreshToken(user.Id, ip);
            _logger.LogTrace("Generation of JWT and refresh token succeded");

            // Remove old refresh tokens from user
            _logger.LogTrace("Removing old refresh tokens from the database");
            RemoveOldRefreshTokens(user);
            _logger.LogTrace("Logging user login in database");
            LogUserLogin(user.Id, ip);

            var res = new LoginAuthResponse(userAuthVM, jwtToken.Token, jwtToken.ExpirationSeconds, refreshToken.Token);
            res.Status = ResultStatuses.Success.ToString();
            _logger.LogTrace("AuthService.Authenticate - End. Returning LoginAuthResponse object with user login data");
            return res;
        }

        public LoginAuthResponse RefreshToken(string currentRefreshToken, string ipAddress)
        {
            _logger.LogTrace("AuthService.RefreshToken - Start");
            var currentRefreshTokenMessage = string.IsNullOrEmpty(currentRefreshToken) ? "Unknown" : "Not empty";
            _logger.LogTrace($"Current refresh token is - {currentRefreshTokenMessage}");
            _logger.LogTrace($"Trying to find user by current refresh token.");
            User user = GetUserByRefreshToken(currentRefreshToken);


            RefreshToken refreshTokenObj = null;

            if (!string.IsNullOrEmpty(currentRefreshToken))
            {
                _logger.LogTrace("Finding refresh token data by the current refresh token string");
                refreshTokenObj = GetRefreshToken(currentRefreshToken);
            }

            if (refreshTokenObj == null || user == null)
            {
                _logger.LogTrace("Invalid / Non existing refresh token or user wasn't found by the refresh token. Throwing a BadRequest exception");
                throw new AppException("Invalid / Non existing refresh token", "Invalid / Non existing refresh token", (int)InternalErrorCodes.InvalidRefreshToken, HttpStatusCode.BadRequest);
            }

            _logger.LogTrace($"The user email is - {user.Email}");

            if (refreshTokenObj.IsRevoked)
            {
                _logger.LogTrace("The refresh token object is revoked. Calling RevokeDescendantRefreshTokens to revoke all it's familly");
                // Revoke all descendant tokens in case this token has been compromised
                RevokeDescendantRefreshTokens(refreshTokenObj, user, ipAddress, $"Attempted reuse of revoked ancestor token: {currentRefreshToken}");
                _logger.LogTrace("Calling RevokeDescendantRefreshTokens - Success");
            }

            if (!refreshTokenObj.IsActive)
            {
                _logger.LogTrace("The refresh token object is NOT active. Throwing a BadRequest exception");
                throw new AppException("Invalid token", HttpStatusCode.BadRequest);
            }

            // Replace old refresh token with a new one (rotate token) Also in db
            _logger.LogTrace("Calling RotateRefreshToken to replace the old one with a new refresh token");
            var newRefreshToken = RotateRefreshToken(refreshTokenObj, ipAddress);
            if (newRefreshToken == null)
            {
                _logger.LogTrace("Calling RotateRefreshToken failed and the NEW refresh token returned as NULL. Throwing a BadRequest exception");
                throw new AppException("Calling RotateRefreshToken failed and the NEW refresh token returned as NULL", "Invalid / Non existing NEW refresh token", (int)InternalErrorCodes.InvalidRefreshToken, HttpStatusCode.BadRequest);
            }

            // Remove old refresh tokens from user
            _logger.LogTrace("Removing old refresh tokens from the database");
            RemoveOldRefreshTokens(user);

            // Generate new JWT
            var userAuthVM = GetUserAuthVM(user.Id);
            userAuthVM.UserType = UserType.User;
            _logger.LogTrace("Trying to generate a JWT token for the user. ");
            var jwtToken = _jwtUtils.GenerateJwtToken(userAuthVM); 

            var res = new LoginAuthResponse(userAuthVM, jwtToken.Token, jwtToken.ExpirationSeconds, newRefreshToken.Token);
            res.Status = ResultStatuses.Success.ToString();

            _logger.LogTrace("AuthService.RefreshToken - End with success");

            return res;
        }

        private RefreshToken GetRefreshToken(string token)
        {
            string sql = $"SELECT * FROM [dbo].[v_refresh_tokens] v WHERE v.Token = '{token}'";
            var table = _dal.GetTable(sql);
            if (table.Rows.Count > 0)
                return DalHelper.DataRowToObject<RefreshToken>(table.Rows[0]);

            return null;
        }

        public void RevokeToken(string token, string revokedWorkstationIP)
        {
            _logger.LogTrace("AuthService.RevokeToken - Start");

            _logger.LogTrace("Calling GetUserByRefreshToken");
            var user = GetUserByRefreshToken(token);
            var email_msg = user != null ? user.Email : "NULL";
            _logger.LogTrace($"User email is {email_msg}");

            _logger.LogTrace("Calling GetRefreshToken");
            var refreshToken = GetRefreshToken(token);
            var refreshTokenMsg = refreshToken == null ? "found" : "not found";
            _logger.LogTrace($"Refresh token object was {email_msg}");

            if (refreshToken == null || !refreshToken.IsActive)
            {
                _logger.LogTrace($"Invalid refresh token for user {user.Email}");
                throw new AppException("Invalid refresh token.","Cannot revoke an invalid refresh token", HttpStatusCode.BadRequest);
            }

            // revoke token and save
            _logger.LogTrace("Calling RevokeRefreshToken");
            RevokeRefreshToken(refreshToken, revokedWorkstationIP, "Revoked without replacement");

            _logger.LogTrace("Calling LogUserLogout");
            LogUserLogout(user.Id, revokedWorkstationIP);

            _logger.LogTrace("AuthService.RevokeToken - End");

        }




        // helper methods

        private User GetUserByRefreshToken(string token)
        {
            try
            {
                SqlParameter[] pArr = { new SqlParameter("token", SqlDbType.VarChar, 250) { Value = token } };

                var table = _dal.GetTable("sp_user_get_by_refresh_token", pArr);

                if (table.Rows.Count > 0)
                {
                    return DalHelper.DataRowToObject<User>(table.Rows[0]);
                }
                else return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private RefreshToken AddRefreshToken(int userId, string ip)
        {
            var newRefreshToken = _jwtUtils.GenerateRefreshToken(ip);
            newRefreshToken.UserId = userId;
            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("userId", SqlDbType.Int) { Value = newRefreshToken.UserId },
                    new SqlParameter("token", SqlDbType.VarChar, 255) { Value = newRefreshToken.Token },
                    new SqlParameter("expiration", SqlDbType.DateTime) { Value =  newRefreshToken.Expires},
                    new SqlParameter("creatorIp", SqlDbType.VarChar, 100) { Value = ip },
                    new SqlParameter("revokation_reason", SqlDbType.VarChar, 100) { Value = "Clean up while in new authentication process" }
                };

                // Note that this procedure, because token_old 
                var table = _dal.GetTable("sp_refresh_token_refresh", pArr);

                if (table.Rows.Count > 0 && table.Rows[0]["Status"].ToString() != ResultStatuses.Success.ToString())
                {
                    _logger.LogError("Error while AddRefreshToken. Message: {Message}", table.Rows[0]["Status"].ToString());
                    return null;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error while AddRefreshToken. Message: {Message}", ex.Message);
                return null;
            }

            return newRefreshToken;

        }

        private RefreshToken RotateRefreshToken(RefreshToken refreshToken, string ip)
        {
            var newRefreshToken = _jwtUtils.GenerateRefreshToken(ip);
            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("userId", SqlDbType.Int) { Value = refreshToken.UserId },
                    new SqlParameter("token", SqlDbType.VarChar, 255) { Value = newRefreshToken.Token },
                    new SqlParameter("expiration", SqlDbType.DateTime) { Value =  newRefreshToken.Expires},
                    new SqlParameter("creatorIp", SqlDbType.VarChar, 100) { Value = ip },
                    new SqlParameter("token_old", SqlDbType.VarChar, 255) { Value = refreshToken.Token },
                    new SqlParameter("revokerIp", SqlDbType.VarChar, 100) { Value = ip },
                    new SqlParameter("revokation_reason", SqlDbType.VarChar, 100) { Value = "Replaced by new token" }
                };

                var table = _dal.GetTable("sp_refresh_token_refresh", pArr);

                if (table.Rows.Count > 0 && table.Rows[0]["Status"].ToString() != ResultStatuses.Success.ToString())
                {
                    _logger.LogError("Error while AddRefreshToken. Message: {Message}", table.Rows[0]["Status"].ToString());
                    return null;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error while AddRefreshToken. Message: {Message}", ex.Message);
                return null;
            }



            return newRefreshToken;
        }

        private void RemoveOldRefreshTokens(User user)
        {
            SqlParameter[] pArr =
            {
                    new SqlParameter("userId", SqlDbType.Int) { Value = user.Id },
                    new SqlParameter("days_back", SqlDbType.Int) { Value =  _appSettings.RefreshTokenCleanBackDays},
                };

            _dal.Execute("sp_refresh_token_remove_olds", pArr);
        }

        private void RevokeRefreshToken(RefreshToken refreshToken, string revokedWorkstationIP, string reason)
        {
            // If this token has decendents, call databas procedur to revoke all decendents.

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("token", SqlDbType.VarChar, 255) { Value = refreshToken.Token },
                    new SqlParameter("revoked_workstation_ip", SqlDbType.VarChar, 100) { Value = revokedWorkstationIP },
                    new SqlParameter("revokation_reason", SqlDbType.VarChar, 100) { Value = reason },
                    new SqlParameter("includ_decendents", SqlDbType.Bit) { Value = false }
                };


                var table = _dal.GetTable("sp_refresh_token_revoke", pArr);

                if (table.Rows.Count > 0 && table.Rows[0]["Status"].ToString() != ResultStatuses.Success.ToString())
                {
                    _logger.LogError("Error while RevokeRefreshToken. Message: {Message}", table.Rows[0]["Status"].ToString());
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error while RevokeRefreshToken. Message: {Message}", ex.Message);
            }

        }

        private void RevokeDescendantRefreshTokens(RefreshToken refreshToken, User user, string ip, string reason)
        {
            // If this token has decendents, call databas procedur to revoke all decendents.

            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("userId", SqlDbType.Int) { Value = user.Id },
                    new SqlParameter("token", SqlDbType.VarChar, 255) { Value = refreshToken.Token },
                    new SqlParameter("revokerIp", SqlDbType.VarChar, 100) { Value = ip },
                    new SqlParameter("revokation_reason", SqlDbType.VarChar, 100) { Value = reason }
                };


                var table = _dal.GetTable("sp_refresh_token_revoke", pArr);

                if (table.Rows.Count > 0 && table.Rows[0]["Status"].ToString() != ResultStatuses.Success.ToString())
                {
                    _logger.LogError("Error while RevokeDescendantRefreshTokens. Message: {Message}", table.Rows[0]["Status"].ToString());
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error while RevokeDescendantRefreshTokens. Message: {Message}", ex.Message);
            }
        }

        private User GetUserByEmail(string email)
        {
            try
            {
                SqlParameter[] pArr = { new SqlParameter("email", SqlDbType.VarChar, 250) { Value = email } };

                var table = _dal.GetTable("sp_user_validate", pArr);

                if (table.Rows.Count > 0)
                {
                    return DalHelper.DataRowToObject<User>(table.Rows[0]);
                }
                else return null;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error while trying to get user by email. Message: {ex.Message}");
                return null;
            }
        }

        private UserAuthVM GetUserAuthVM(int id)
        {
            SqlParameter[] pArr =
                   {
                        new SqlParameter("id", SqlDbType.Int) { Value = id },
                        new SqlParameter("json", SqlDbType.NVarChar, -1) { Direction = ParameterDirection.Output } // -1 is max
                    };
            var resParams = _dal.ExecuteSPGetResalutValues("sp_get_user_auth_json_object", pArr, ParameterDirection.Output);
            string json = resParams[0].Value.ToString();
            return JsonConvert.DeserializeObject<UserAuthVM>(json);
        }

        private bool LogUserLogin(int id, string ip)
        {
            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("id", SqlDbType.Int) { Value = id },
                    new SqlParameter("activity_type_id", SqlDbType.Int) { Value = 1 }, //  1 is login, 2 is logout
                    new SqlParameter("ip", SqlDbType.VarChar, 50) { Value = ip }
                };

                var affectedRows = _dal.Execute("sp_log_user_login", pArr);
                if (affectedRows < 1) return false;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while LogUserLogin. Message: {Message}", ex.Message);
            }

            return true;
        }

        private bool LogUserLogout(int id, string ip)
        {
            try
            {
                SqlParameter[] pArr =
                {
                    new SqlParameter("id", SqlDbType.Int) { Value = id },
                    new SqlParameter("activity_type_id", SqlDbType.Int) { Value = 2 }, //  1 is login, 2 is logout
                    new SqlParameter("ip", SqlDbType.VarChar, 50) { Value = ip }
                };

                var affectedRows = _dal.Execute("sp_log_user_login", pArr);
                if (affectedRows < 1) return false;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while LogUserLogout. Message: {Message}", ex.Message);
            }

            return true;
        }
    }
}