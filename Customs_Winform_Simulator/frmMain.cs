﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Customs_Winform_Simulator
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnOCR_Click(object sender, EventArgs e)
        {
            var frm = new frmOCR();
            frm.ShowDialog();
        }

        private void btnLPRCheckin_Click(object sender, EventArgs e)
        {
            var frm = new frmLPRCheckin();
            frm.ShowDialog();
        }

        private void btnLPRRadio_Click(object sender, EventArgs e)
        {
            var frm = new frmLPRRadio();
            frm.ShowDialog();
        }

        private void btnIAWConclusion_Click(object sender, EventArgs e)
        {
            var frm = new frmIAWConclusion();
            frm.ShowDialog();
        }

        private void btnTagreaderMCA_Click(object sender, EventArgs e)
        {
            var frm = new frmTagreaderMCA();
            frm.ShowDialog();
        }

        private void btnMCConclusion_Click(object sender, EventArgs e)
        {
            var frm = new frmMCWConclusion();
            frm.ShowDialog();
        }

        private void btnOCRPreCheckout_Click(object sender, EventArgs e)
        {
            var frm = new frmOCR("BOA1OCR1");
            frm.ShowDialog();
        }

        private void btnCOLpr_Click(object sender, EventArgs e)
        {
            var frm = new frmLPRCOA();
            frm.ShowDialog();
        }

    }
}
