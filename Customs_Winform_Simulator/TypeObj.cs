﻿namespace Customs_Winform_Simulator
{
    public class ListObj
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}