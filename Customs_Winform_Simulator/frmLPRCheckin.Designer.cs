﻿
namespace Customs_Winform_Simulator
{
    partial class frmLPRCheckin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxDevices = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.RichTextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownLPR = new System.Windows.Forms.NumericUpDown();
            this.txtLPR = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewLPR = new System.Windows.Forms.DataGridView();
            this.btnSelectLPR = new System.Windows.Forms.Button();
            this.btnLPRDataSubmit = new System.Windows.Forms.Button();
            this.selectFiles = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLPR)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxDevices
            // 
            this.comboBoxDevices.DisplayMember = "name";
            this.comboBoxDevices.FormattingEnabled = true;
            this.comboBoxDevices.Location = new System.Drawing.Point(75, 27);
            this.comboBoxDevices.Name = "comboBoxDevices";
            this.comboBoxDevices.Size = new System.Drawing.Size(192, 23);
            this.comboBoxDevices.TabIndex = 41;
            this.comboBoxDevices.ValueMember = "id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(717, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 15);
            this.label7.TabIndex = 38;
            this.label7.Text = "Data:";
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtData.Location = new System.Drawing.Point(772, 17);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(604, 565);
            this.txtData.TabIndex = 37;
            this.txtData.Text = "";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(19, 501);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(732, 81);
            this.lblStatus.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 486);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 35;
            this.label6.Text = "Status:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 15);
            this.label5.TabIndex = 34;
            this.label5.Text = "Device:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.numericUpDownLPR);
            this.panel1.Controls.Add(this.txtLPR);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(19, 191);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 121);
            this.panel1.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 15);
            this.label3.TabIndex = 17;
            this.label3.Text = "LPR Qualifier:";
            // 
            // numericUpDownLPR
            // 
            this.numericUpDownLPR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownLPR.Location = new System.Drawing.Point(103, 52);
            this.numericUpDownLPR.Name = "numericUpDownLPR";
            this.numericUpDownLPR.Size = new System.Drawing.Size(125, 23);
            this.numericUpDownLPR.TabIndex = 5;
            this.numericUpDownLPR.Value = new decimal(new int[] {
            95,
            0,
            0,
            0});
            // 
            // txtLPR
            // 
            this.txtLPR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtLPR.Location = new System.Drawing.Point(99, 19);
            this.txtLPR.Name = "txtLPR";
            this.txtLPR.Size = new System.Drawing.Size(129, 23);
            this.txtLPR.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "LPR Number:";
            // 
            // dataGridViewLPR
            // 
            this.dataGridViewLPR.AllowUserToAddRows = false;
            this.dataGridViewLPR.AllowUserToDeleteRows = false;
            this.dataGridViewLPR.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewLPR.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewLPR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLPR.Location = new System.Drawing.Point(153, 71);
            this.dataGridViewLPR.Name = "dataGridViewLPR";
            this.dataGridViewLPR.RowHeadersVisible = false;
            this.dataGridViewLPR.RowTemplate.Height = 25;
            this.dataGridViewLPR.Size = new System.Drawing.Size(598, 103);
            this.dataGridViewLPR.TabIndex = 30;
            // 
            // btnSelectLPR
            // 
            this.btnSelectLPR.Location = new System.Drawing.Point(17, 71);
            this.btnSelectLPR.Name = "btnSelectLPR";
            this.btnSelectLPR.Size = new System.Drawing.Size(130, 104);
            this.btnSelectLPR.TabIndex = 27;
            this.btnSelectLPR.Text = "LPR Images";
            this.btnSelectLPR.UseVisualStyleBackColor = true;
            this.btnSelectLPR.Click += new System.EventHandler(this.btnSelectLPR_Click);
            // 
            // btnLPRDataSubmit
            // 
            this.btnLPRDataSubmit.Location = new System.Drawing.Point(17, 415);
            this.btnLPRDataSubmit.Name = "btnLPRDataSubmit";
            this.btnLPRDataSubmit.Size = new System.Drawing.Size(734, 64);
            this.btnLPRDataSubmit.TabIndex = 31;
            this.btnLPRDataSubmit.Text = "Submit";
            this.btnLPRDataSubmit.UseVisualStyleBackColor = true;
            this.btnLPRDataSubmit.Click += new System.EventHandler(this.btnLPRDataSubmit_Click);
            // 
            // selectFiles
            // 
            this.selectFiles.Filter = "Images (*.jpg,*.jpeg)|*.jpg;*.jpeg";
            this.selectFiles.Multiselect = true;
            // 
            // frmLPRCheckin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1393, 598);
            this.Controls.Add(this.comboBoxDevices);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewLPR);
            this.Controls.Add(this.btnSelectLPR);
            this.Controls.Add(this.btnLPRDataSubmit);
            this.Name = "frmLPRCheckin";
            this.ShowIcon = false;
            this.Text = "LPR Check-in Data Input";
            this.Load += new System.EventHandler(this.frmLPR_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLPR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxDevices;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox txtData;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownLPR;
        private System.Windows.Forms.TextBox txtLPR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewLPR;
        private System.Windows.Forms.Button btnSelectLPR;
        private System.Windows.Forms.Button btnLPRDataSubmit;
        private System.Windows.Forms.OpenFileDialog selectFiles;
    }
}