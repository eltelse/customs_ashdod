using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Customs_Winform_Simulator
{
    static class Program
    {
        public static IConfiguration Configuration;

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var settingsBuilder = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            Configuration = settingsBuilder.Build();

            var hostBuilder = new HostBuilder()
               .ConfigureServices((hostContext, services) =>
               {
                   //Register all your services here
                   services.AddScoped<frmMain>();
                   services.AddScoped<frmOCR>();
                   services.AddScoped<frmTagreaderMCA>();
                   services.AddScoped<frmLPRRadio>();
                   services.AddScoped<frmLPRCheckin>();
                   services.AddScoped<frmIAWConclusion>();

               });
            var host = hostBuilder.Build();


            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (var serviceScope = host.Services.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                var frmMain = services.GetRequiredService<frmMain>();
                Application.Run(frmMain);
            }

        }
    }
}
