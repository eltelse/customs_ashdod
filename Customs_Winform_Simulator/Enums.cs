﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customs_Winform_Simulator
{
    public enum AreaType
    {
        Default = 0,
        BIA,
        BOA,
        CIA,
        COA,
        MCA,
        PCA,
        PSA,
        SCA,
    }
}
