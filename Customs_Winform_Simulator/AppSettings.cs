﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customs_Winform_Simulator
{
    public class AppSettings
    {
        public string DB_CONECTION { get; set; }
        public string API_POST_URL { get; set; }
        public string API_AUTH_URL { get; set; }
    }
}
