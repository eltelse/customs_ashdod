﻿using EltelWebApi.DataAccess;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Customs_Winform_Simulator
{
    public partial class frmTagreaderMCA : Form
    {
        AppSettings _settings;

        public frmTagreaderMCA()
        {
            InitializeComponent();
            _settings = Program.Configuration.GetSection("AppSettings").Get<AppSettings>();
        }

        private void frmTagreaderMCA_Load(object sender, EventArgs e)
        {
            var deviceList = GetDeviceList();
            comboBoxDevices.DataSource = deviceList;
            comboBoxDevices.SelectedItem = deviceList.Find(d => d.name == "MCA1TagReader1 - TagReader"); 
            
            txtDriverTagNumber.Text = DateTime.Now.ToString("yyyyMMdd") + "00301";
            txtDriverTagNumber.Focus();
        }

        private List<ListObj> GetDeviceEventTypes()
        {
            Dal d = new Dal(_settings.DB_CONECTION);
            string sql = @"select * from [dbo].[v_device_event_types] Order By [name]";
            var tbl = d.GetTable(sql);
            List<ListObj> resObj = DalHelper.DataTableToList<ListObj>(tbl);
            return resObj;
        }

        private List<ListObj> GetDeviceList()
        {

            Dal d = new Dal(_settings.DB_CONECTION);
            string sql = @"SELECT [Id] As id, [name] = [Name] + ' - ' + DeviceType, [description] = [Description] FROM [dbo].[v_devices] Order By[Name] +' - ' + DeviceType";
            var tbl = d.GetTable(sql);
            List<ListObj> resObj = DalHelper.DataTableToList<ListObj>(tbl);
            return resObj;
        }

        private void btnTagReaderSubmit_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            txtData.Text = "";

            dynamic de = new ExpandoObject();

            de.DriverTagNumber = txtDriverTagNumber.Text;
            de.DeviceIdentifier = ((ListObj)comboBoxDevices.SelectedItem).name.Split(new char[] { ' ' }, StringSplitOptions.None)[0];
            de.WorkstationIP = txtWorstationIP.Text;
            de.UserId = txtUserId.Text;
            de.LPR = txtLPR.Text;
            de.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

            var json = JsonConvert.SerializeObject(de);
            JToken parsedJson = JToken.Parse(json);
            var beautified = parsedJson.ToString(Formatting.Indented);
            txtData.Text += beautified + Environment.NewLine;
            lblStatus.Text = Utils.PostDeviceEvent(json);
        }
    }
}
