﻿
namespace Customs_Winform_Simulator
{
    partial class frmMCWConclusion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxDevices = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.RichTextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnLPRDataSubmit = new System.Windows.Forms.Button();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDriverTagNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxSuspitionType = new System.Windows.Forms.ComboBox();
            this.txtLPR = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWorstationIP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxDevices
            // 
            this.comboBoxDevices.DisplayMember = "name";
            this.comboBoxDevices.FormattingEnabled = true;
            this.comboBoxDevices.Location = new System.Drawing.Point(75, 27);
            this.comboBoxDevices.Name = "comboBoxDevices";
            this.comboBoxDevices.Size = new System.Drawing.Size(192, 23);
            this.comboBoxDevices.TabIndex = 65;
            this.comboBoxDevices.ValueMember = "id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(717, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 15);
            this.label7.TabIndex = 62;
            this.label7.Text = "Data:";
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtData.Location = new System.Drawing.Point(772, 17);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(604, 565);
            this.txtData.TabIndex = 61;
            this.txtData.Text = "";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(19, 501);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(732, 81);
            this.lblStatus.TabIndex = 60;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 486);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 59;
            this.label6.Text = "Status:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 15);
            this.label5.TabIndex = 58;
            this.label5.Text = "Device:";
            // 
            // btnLPRDataSubmit
            // 
            this.btnLPRDataSubmit.Location = new System.Drawing.Point(17, 415);
            this.btnLPRDataSubmit.Name = "btnLPRDataSubmit";
            this.btnLPRDataSubmit.Size = new System.Drawing.Size(734, 64);
            this.btnLPRDataSubmit.TabIndex = 56;
            this.btnLPRDataSubmit.Text = "Submit";
            this.btnLPRDataSubmit.UseVisualStyleBackColor = true;
            this.btnLPRDataSubmit.Click += new System.EventHandler(this.btnLPRDataSubmit_Click);
            // 
            // txtUserId
            // 
            this.txtUserId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtUserId.Location = new System.Drawing.Point(131, 112);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(136, 23);
            this.txtUserId.TabIndex = 85;
            this.txtUserId.Text = "OreN";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 15);
            this.label9.TabIndex = 86;
            this.label9.Text = "User Id:";
            // 
            // txtDriverTagNumber
            // 
            this.txtDriverTagNumber.Location = new System.Drawing.Point(131, 190);
            this.txtDriverTagNumber.Name = "txtDriverTagNumber";
            this.txtDriverTagNumber.Size = new System.Drawing.Size(136, 23);
            this.txtDriverTagNumber.TabIndex = 81;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 15);
            this.label4.TabIndex = 82;
            this.label4.Text = "Driver Tag Number:";
            // 
            // comboBoxSuspitionType
            // 
            this.comboBoxSuspitionType.DisplayMember = "name";
            this.comboBoxSuspitionType.FormattingEnabled = true;
            this.comboBoxSuspitionType.Location = new System.Drawing.Point(131, 227);
            this.comboBoxSuspitionType.Name = "comboBoxSuspitionType";
            this.comboBoxSuspitionType.Size = new System.Drawing.Size(136, 23);
            this.comboBoxSuspitionType.TabIndex = 75;
            this.comboBoxSuspitionType.ValueMember = "id";
            // 
            // txtLPR
            // 
            this.txtLPR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtLPR.Location = new System.Drawing.Point(131, 148);
            this.txtLPR.Name = "txtLPR";
            this.txtLPR.Size = new System.Drawing.Size(136, 23);
            this.txtLPR.TabIndex = 76;
            this.txtLPR.Text = "1243479";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 230);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.TabIndex = 74;
            this.label1.Text = "Suspicion Type:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 15);
            this.label2.TabIndex = 77;
            this.label2.Text = "LPR Number:";
            // 
            // txtWorstationIP
            // 
            this.txtWorstationIP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtWorstationIP.Location = new System.Drawing.Point(131, 73);
            this.txtWorstationIP.Name = "txtWorstationIP";
            this.txtWorstationIP.Size = new System.Drawing.Size(136, 23);
            this.txtWorstationIP.TabIndex = 79;
            this.txtWorstationIP.Text = "192.111.111.116";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 15);
            this.label3.TabIndex = 80;
            this.label3.Text = "Workstation IP:";
            // 
            // frmMCWConclusion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1393, 598);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtDriverTagNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxSuspitionType);
            this.Controls.Add(this.txtLPR);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtWorstationIP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxDevices);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnLPRDataSubmit);
            this.Name = "frmMCWConclusion";
            this.ShowIcon = false;
            this.Text = "MCWConclusion";
            this.Load += new System.EventHandler(this.frmMCWConclusion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxDevices;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox txtData;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnLPRDataSubmit;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDriverTagNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxSuspitionType;
        private System.Windows.Forms.TextBox txtLPR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWorstationIP;
        private System.Windows.Forms.Label label3;
    }
}