﻿
namespace Customs_Winform_Simulator
{
    partial class frmIAWConclusion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxDevices = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.RichTextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLPR = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxSuspitionType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLPRDataSubmit = new System.Windows.Forms.Button();
            this.txtWorstationIP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDriverTagNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtScanId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGridViewIAW = new System.Windows.Forms.DataGridView();
            this.btnSelectIAW = new System.Windows.Forms.Button();
            this.selectFiles = new System.Windows.Forms.OpenFileDialog();
            this.label10 = new System.Windows.Forms.Label();
            this.lblScannerIdentifier = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIAW)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxDevices
            // 
            this.comboBoxDevices.DisplayMember = "name";
            this.comboBoxDevices.FormattingEnabled = true;
            this.comboBoxDevices.Location = new System.Drawing.Point(75, 19);
            this.comboBoxDevices.Name = "comboBoxDevices";
            this.comboBoxDevices.Size = new System.Drawing.Size(192, 23);
            this.comboBoxDevices.TabIndex = 65;
            this.comboBoxDevices.ValueMember = "id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(717, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 15);
            this.label7.TabIndex = 62;
            this.label7.Text = "Data:";
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtData.Location = new System.Drawing.Point(772, 17);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(604, 565);
            this.txtData.TabIndex = 61;
            this.txtData.Text = "";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(19, 501);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(732, 81);
            this.lblStatus.TabIndex = 60;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 486);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 59;
            this.label6.Text = "Status:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 15);
            this.label5.TabIndex = 58;
            this.label5.Text = "Device:";
            // 
            // txtLPR
            // 
            this.txtLPR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtLPR.Location = new System.Drawing.Point(131, 257);
            this.txtLPR.Name = "txtLPR";
            this.txtLPR.Size = new System.Drawing.Size(136, 23);
            this.txtLPR.TabIndex = 54;
            this.txtLPR.Text = "987654323";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 260);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 15);
            this.label2.TabIndex = 55;
            this.label2.Text = "LPR Number:";
            // 
            // comboBoxSuspitionType
            // 
            this.comboBoxSuspitionType.DisplayMember = "name";
            this.comboBoxSuspitionType.FormattingEnabled = true;
            this.comboBoxSuspitionType.Location = new System.Drawing.Point(131, 380);
            this.comboBoxSuspitionType.Name = "comboBoxSuspitionType";
            this.comboBoxSuspitionType.Size = new System.Drawing.Size(136, 23);
            this.comboBoxSuspitionType.TabIndex = 53;
            this.comboBoxSuspitionType.ValueMember = "id";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 383);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "Suspicion Type:";
            // 
            // btnLPRDataSubmit
            // 
            this.btnLPRDataSubmit.Location = new System.Drawing.Point(17, 415);
            this.btnLPRDataSubmit.Name = "btnLPRDataSubmit";
            this.btnLPRDataSubmit.Size = new System.Drawing.Size(734, 64);
            this.btnLPRDataSubmit.TabIndex = 56;
            this.btnLPRDataSubmit.Text = "Submit";
            this.btnLPRDataSubmit.UseVisualStyleBackColor = true;
            this.btnLPRDataSubmit.Click += new System.EventHandler(this.btnLPRDataSubmit_Click);
            // 
            // txtWorstationIP
            // 
            this.txtWorstationIP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtWorstationIP.Location = new System.Drawing.Point(131, 182);
            this.txtWorstationIP.Name = "txtWorstationIP";
            this.txtWorstationIP.Size = new System.Drawing.Size(136, 23);
            this.txtWorstationIP.TabIndex = 66;
            this.txtWorstationIP.Text = "192.111.111.20";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 15);
            this.label3.TabIndex = 67;
            this.label3.Text = "Workstation IP:";
            // 
            // txtDriverTagNumber
            // 
            this.txtDriverTagNumber.Location = new System.Drawing.Point(131, 299);
            this.txtDriverTagNumber.Name = "txtDriverTagNumber";
            this.txtDriverTagNumber.Size = new System.Drawing.Size(136, 23);
            this.txtDriverTagNumber.TabIndex = 68;
            this.txtDriverTagNumber.Text = "2022063000041";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 302);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 15);
            this.label4.TabIndex = 69;
            this.label4.Text = "Driver Tag Number:";
            // 
            // txtScanId
            // 
            this.txtScanId.Location = new System.Drawing.Point(131, 342);
            this.txtScanId.Name = "txtScanId";
            this.txtScanId.Size = new System.Drawing.Size(136, 23);
            this.txtScanId.TabIndex = 70;
            this.txtScanId.Text = "97201PD01202112220004";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 345);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 15);
            this.label8.TabIndex = 71;
            this.label8.Text = "Scan Id:";
            // 
            // txtUserId
            // 
            this.txtUserId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtUserId.Location = new System.Drawing.Point(131, 221);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(136, 23);
            this.txtUserId.TabIndex = 72;
            this.txtUserId.Text = "OreN";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 224);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 15);
            this.label9.TabIndex = 73;
            this.label9.Text = "User Id:";
            // 
            // dataGridViewIAW
            // 
            this.dataGridViewIAW.AllowUserToAddRows = false;
            this.dataGridViewIAW.AllowUserToDeleteRows = false;
            this.dataGridViewIAW.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewIAW.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewIAW.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIAW.Location = new System.Drawing.Point(153, 60);
            this.dataGridViewIAW.Name = "dataGridViewIAW";
            this.dataGridViewIAW.RowHeadersVisible = false;
            this.dataGridViewIAW.RowTemplate.Height = 25;
            this.dataGridViewIAW.Size = new System.Drawing.Size(598, 103);
            this.dataGridViewIAW.TabIndex = 75;
            // 
            // btnSelectIAW
            // 
            this.btnSelectIAW.Location = new System.Drawing.Point(17, 59);
            this.btnSelectIAW.Name = "btnSelectIAW";
            this.btnSelectIAW.Size = new System.Drawing.Size(130, 104);
            this.btnSelectIAW.TabIndex = 74;
            this.btnSelectIAW.Text = "NII Images";
            this.btnSelectIAW.UseVisualStyleBackColor = true;
            this.btnSelectIAW.Click += new System.EventHandler(this.btnSelectIAW_Click);
            // 
            // selectFiles
            // 
            this.selectFiles.Filter = "Images (*.jpg,*.jpeg)|*.jpg;*.jpeg";
            this.selectFiles.Multiselect = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(308, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 15);
            this.label10.TabIndex = 76;
            this.label10.Text = "Scanner Code/Identifier:";
            // 
            // lblScannerIdentifier
            // 
            this.lblScannerIdentifier.AutoSize = true;
            this.lblScannerIdentifier.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblScannerIdentifier.Location = new System.Drawing.Point(449, 23);
            this.lblScannerIdentifier.Name = "lblScannerIdentifier";
            this.lblScannerIdentifier.Size = new System.Drawing.Size(72, 15);
            this.lblScannerIdentifier.TabIndex = 77;
            this.lblScannerIdentifier.Text = "97201PD01";
            // 
            // frmIAWConclusion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1408, 598);
            this.Controls.Add(this.lblScannerIdentifier);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dataGridViewIAW);
            this.Controls.Add(this.btnSelectIAW);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtScanId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDriverTagNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxSuspitionType);
            this.Controls.Add(this.txtLPR);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtWorstationIP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxDevices);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnLPRDataSubmit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmIAWConclusion";
            this.ShowIcon = false;
            this.Text = "IAWConclusion";
            this.Load += new System.EventHandler(this.frmIAWConclusion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIAW)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxDevices;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox txtData;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLPR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxSuspitionType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLPRDataSubmit;
        private System.Windows.Forms.TextBox txtWorstationIP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDriverTagNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtScanId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dataGridViewIAW;
        private System.Windows.Forms.Button btnSelectIAW;
        private System.Windows.Forms.OpenFileDialog selectFiles;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblScannerIdentifier;
    }
}