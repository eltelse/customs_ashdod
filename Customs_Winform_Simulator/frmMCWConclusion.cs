﻿using EltelWebApi.DataAccess;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Customs_Winform_Simulator
{
    public partial class frmMCWConclusion : Form
    {
        AppSettings _settings;

        public frmMCWConclusion()
        {
            InitializeComponent();
            _settings = Program.Configuration.GetSection("AppSettings").Get<AppSettings>();
        }

        private void frmMCWConclusion_Load(object sender, EventArgs e)
        {
            var deviceList = GetDeviceList();
            comboBoxDevices.DataSource = deviceList;
            comboBoxDevices.SelectedItem = deviceList.Find(d => d.name == "MCA1SYSWS1 - SYSWS");
            var susTypes = GetSuspitionTypes();
            comboBoxSuspitionType.DataSource = susTypes;
            comboBoxSuspitionType.SelectedItem = susTypes.Find(s => s.name == "PASS");

            txtDriverTagNumber.Text = DateTime.Now.ToString("yyyyMMdd") + "00301";
        }

        private List<ListObj> GetSuspitionTypes()
        {
            return new List<ListObj>()
            {
                new ListObj(){ id = 0, name = "NOT SET" },
                new ListObj(){ id = 1, name = "SUSPICIOUS" },
                new ListObj(){ id = 2, name = "PASS" },
                new ListObj(){ id = 3, name = "Bypass" },
            };
        }

        private List<ListObj> GetDeviceEventTypes()
        {
            Dal d = new Dal(_settings.DB_CONECTION);
            string sql = @"select * from [dbo].[v_device_event_types] Order By [name]";
            var tbl = d.GetTable(sql);
            List<ListObj> resObj = DalHelper.DataTableToList<ListObj>(tbl);
            return resObj;
        }

        private List<ListObj> GetDeviceList()
        {

            Dal d = new Dal(_settings.DB_CONECTION);
            string sql = @"SELECT [Id] As id, [name] = [Name] + ' - ' + DeviceType, [description] = [Description] FROM [dbo].[v_devices] Order By[Name] +' - ' + DeviceType";
            var tbl = d.GetTable(sql);
            List<ListObj> resObj = DalHelper.DataTableToList<ListObj>(tbl);
            return resObj;
        }

        private void btnLPRDataSubmit_Click(object sender, EventArgs e)
        {

            lblStatus.Text = "";
            txtData.Text = "";
            
            dynamic de = new ExpandoObject();

            de.DriverTagNumber = txtDriverTagNumber.Text;
            de.DeviceIdentifier = ((ListObj)comboBoxDevices.SelectedItem).name.Split(new char[] { ' ' }, StringSplitOptions.None)[0];
            de.WorkstationIP = txtWorstationIP.Text;
            de.UserId = txtUserId.Text;
            de.LPR = txtLPR.Text;
            de.ConclusionType = ((ListObj)comboBoxSuspitionType.SelectedItem).name;
            de.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

            var json = JsonConvert.SerializeObject(de);
            JToken parsedJson = JToken.Parse(json);
            var beautified = parsedJson.ToString(Formatting.Indented);
            txtData.Text += beautified + Environment.NewLine;
            lblStatus.Text = Utils.PostDeviceEvent(json);
        }
    }
}
