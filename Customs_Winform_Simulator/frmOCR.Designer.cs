﻿
namespace Customs_Winform_Simulator
{
    partial class frmOCR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOCRDataSubmit = new System.Windows.Forms.Button();
            this.selectFiles = new System.Windows.Forms.OpenFileDialog();
            this.btnSelectCCR = new System.Windows.Forms.Button();
            this.btnSelectLPR = new System.Windows.Forms.Button();
            this.dataGridViewCCR = new System.Windows.Forms.DataGridView();
            this.dataGridViewLPR = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownLPR = new System.Windows.Forms.NumericUpDown();
            this.txtLPR = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownCCR = new System.Windows.Forms.NumericUpDown();
            this.txtCCR = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxDevices = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCCR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLPR)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLPR)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCCR)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOCRDataSubmit
            // 
            this.btnOCRDataSubmit.Location = new System.Drawing.Point(22, 410);
            this.btnOCRDataSubmit.Name = "btnOCRDataSubmit";
            this.btnOCRDataSubmit.Size = new System.Drawing.Size(734, 64);
            this.btnOCRDataSubmit.TabIndex = 8;
            this.btnOCRDataSubmit.Text = "Submit";
            this.btnOCRDataSubmit.UseVisualStyleBackColor = true;
            this.btnOCRDataSubmit.Click += new System.EventHandler(this.btnOCRDataSubmit_Click);
            // 
            // selectFiles
            // 
            this.selectFiles.Filter = "Images (*.jpg,*.jpeg)|*.jpg;*.jpeg";
            this.selectFiles.Multiselect = true;
            // 
            // btnSelectCCR
            // 
            this.btnSelectCCR.Location = new System.Drawing.Point(22, 174);
            this.btnSelectCCR.Name = "btnSelectCCR";
            this.btnSelectCCR.Size = new System.Drawing.Size(130, 104);
            this.btnSelectCCR.TabIndex = 3;
            this.btnSelectCCR.Text = "CCR Images";
            this.btnSelectCCR.UseVisualStyleBackColor = true;
            this.btnSelectCCR.Click += new System.EventHandler(this.btnSelectCCR_Click);
            // 
            // btnSelectLPR
            // 
            this.btnSelectLPR.Location = new System.Drawing.Point(22, 66);
            this.btnSelectLPR.Name = "btnSelectLPR";
            this.btnSelectLPR.Size = new System.Drawing.Size(130, 104);
            this.btnSelectLPR.TabIndex = 2;
            this.btnSelectLPR.Text = "LPR Images";
            this.btnSelectLPR.UseVisualStyleBackColor = true;
            this.btnSelectLPR.Click += new System.EventHandler(this.btnSelectLPR_Click);
            // 
            // dataGridViewCCR
            // 
            this.dataGridViewCCR.AllowUserToAddRows = false;
            this.dataGridViewCCR.AllowUserToDeleteRows = false;
            this.dataGridViewCCR.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCCR.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewCCR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCCR.Location = new System.Drawing.Point(158, 175);
            this.dataGridViewCCR.Name = "dataGridViewCCR";
            this.dataGridViewCCR.RowHeadersVisible = false;
            this.dataGridViewCCR.RowTemplate.Height = 25;
            this.dataGridViewCCR.Size = new System.Drawing.Size(598, 103);
            this.dataGridViewCCR.TabIndex = 5;
            // 
            // dataGridViewLPR
            // 
            this.dataGridViewLPR.AllowUserToAddRows = false;
            this.dataGridViewLPR.AllowUserToDeleteRows = false;
            this.dataGridViewLPR.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewLPR.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewLPR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLPR.Location = new System.Drawing.Point(158, 66);
            this.dataGridViewLPR.Name = "dataGridViewLPR";
            this.dataGridViewLPR.RowHeadersVisible = false;
            this.dataGridViewLPR.RowTemplate.Height = 25;
            this.dataGridViewLPR.Size = new System.Drawing.Size(598, 103);
            this.dataGridViewLPR.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.numericUpDownLPR);
            this.panel1.Controls.Add(this.txtLPR);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(107, 284);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 121);
            this.panel1.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 15);
            this.label3.TabIndex = 17;
            this.label3.Text = "LPR Qualifier:";
            // 
            // numericUpDownLPR
            // 
            this.numericUpDownLPR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownLPR.Location = new System.Drawing.Point(103, 52);
            this.numericUpDownLPR.Name = "numericUpDownLPR";
            this.numericUpDownLPR.Size = new System.Drawing.Size(125, 23);
            this.numericUpDownLPR.TabIndex = 5;
            this.numericUpDownLPR.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // txtLPR
            // 
            this.txtLPR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtLPR.Location = new System.Drawing.Point(99, 19);
            this.txtLPR.Name = "txtLPR";
            this.txtLPR.Size = new System.Drawing.Size(129, 23);
            this.txtLPR.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "LPR Number:";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.numericUpDownCCR);
            this.panel2.Controls.Add(this.txtCCR);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(439, 284);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(253, 121);
            this.panel2.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "CCR Qualifier:";
            // 
            // numericUpDownCCR
            // 
            this.numericUpDownCCR.Location = new System.Drawing.Point(109, 52);
            this.numericUpDownCCR.Name = "numericUpDownCCR";
            this.numericUpDownCCR.Size = new System.Drawing.Size(110, 23);
            this.numericUpDownCCR.TabIndex = 7;
            this.numericUpDownCCR.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // txtCCR
            // 
            this.txtCCR.Location = new System.Drawing.Point(88, 16);
            this.txtCCR.Name = "txtCCR";
            this.txtCCR.Size = new System.Drawing.Size(131, 23);
            this.txtCCR.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "CCR Code:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "Device:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 481);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 19;
            this.label6.Text = "Status:";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(24, 496);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(732, 81);
            this.lblStatus.TabIndex = 20;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtData.Location = new System.Drawing.Point(777, 12);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(604, 565);
            this.txtData.TabIndex = 21;
            this.txtData.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(722, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 15);
            this.label7.TabIndex = 22;
            this.label7.Text = "Data:";
            // 
            // comboBoxDevices
            // 
            this.comboBoxDevices.DisplayMember = "name";
            this.comboBoxDevices.FormattingEnabled = true;
            this.comboBoxDevices.Location = new System.Drawing.Point(80, 22);
            this.comboBoxDevices.Name = "comboBoxDevices";
            this.comboBoxDevices.Size = new System.Drawing.Size(192, 23);
            this.comboBoxDevices.TabIndex = 26;
            this.comboBoxDevices.ValueMember = "id";
            // 
            // frmOCR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1393, 598);
            this.Controls.Add(this.comboBoxDevices);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewLPR);
            this.Controls.Add(this.dataGridViewCCR);
            this.Controls.Add(this.btnSelectLPR);
            this.Controls.Add(this.btnSelectCCR);
            this.Controls.Add(this.btnOCRDataSubmit);
            this.Name = "frmOCR";
            this.ShowIcon = false;
            this.Text = "OCR Data Input";
            this.Load += new System.EventHandler(this.frmOCR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCCR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLPR)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLPR)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCCR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOCRDataSubmit;
        private System.Windows.Forms.OpenFileDialog selectFiles;
        private System.Windows.Forms.Button btnSelectCCR;
        private System.Windows.Forms.Button btnSelectLPR;
        private System.Windows.Forms.DataGridView dataGridViewCCR;
        private System.Windows.Forms.DataGridView dataGridViewLPR;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownLPR;
        private System.Windows.Forms.TextBox txtLPR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDownCCR;
        private System.Windows.Forms.TextBox txtCCR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.RichTextBox txtData;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxDevices;
    }
}