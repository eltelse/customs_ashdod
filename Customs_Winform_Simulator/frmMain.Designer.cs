﻿
namespace Customs_Winform_Simulator
{
    partial class frmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOCRPreCheckin = new System.Windows.Forms.Button();
            this.btnLPRCheckin = new System.Windows.Forms.Button();
            this.btnLPRRadio = new System.Windows.Forms.Button();
            this.btnIAWConclusion = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnTagreaderMCA = new System.Windows.Forms.Button();
            this.btnMCConclusion = new System.Windows.Forms.Button();
            this.btnCOLpr = new System.Windows.Forms.Button();
            this.btnOCRPreCheckout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOCRPreCheckin
            // 
            this.btnOCRPreCheckin.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOCRPreCheckin.Location = new System.Drawing.Point(12, 12);
            this.btnOCRPreCheckin.Name = "btnOCRPreCheckin";
            this.btnOCRPreCheckin.Size = new System.Drawing.Size(322, 65);
            this.btnOCRPreCheckin.TabIndex = 0;
            this.btnOCRPreCheckin.Text = "OCR - Pre Checkin Area";
            this.btnOCRPreCheckin.UseVisualStyleBackColor = false;
            this.btnOCRPreCheckin.Click += new System.EventHandler(this.btnOCR_Click);
            // 
            // btnLPRCheckin
            // 
            this.btnLPRCheckin.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLPRCheckin.Location = new System.Drawing.Point(12, 83);
            this.btnLPRCheckin.Name = "btnLPRCheckin";
            this.btnLPRCheckin.Size = new System.Drawing.Size(322, 65);
            this.btnLPRCheckin.TabIndex = 1;
            this.btnLPRCheckin.Text = "LPR - Checkin Area";
            this.btnLPRCheckin.UseVisualStyleBackColor = false;
            this.btnLPRCheckin.Click += new System.EventHandler(this.btnLPRCheckin_Click);
            // 
            // btnLPRRadio
            // 
            this.btnLPRRadio.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLPRRadio.Location = new System.Drawing.Point(12, 225);
            this.btnLPRRadio.Name = "btnLPRRadio";
            this.btnLPRRadio.Size = new System.Drawing.Size(322, 65);
            this.btnLPRRadio.TabIndex = 2;
            this.btnLPRRadio.Text = "LPR - Radiography Start";
            this.btnLPRRadio.UseVisualStyleBackColor = false;
            this.btnLPRRadio.Click += new System.EventHandler(this.btnLPRRadio_Click);
            // 
            // btnIAWConclusion
            // 
            this.btnIAWConclusion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnIAWConclusion.Location = new System.Drawing.Point(12, 296);
            this.btnIAWConclusion.Name = "btnIAWConclusion";
            this.btnIAWConclusion.Size = new System.Drawing.Size(322, 65);
            this.btnIAWConclusion.TabIndex = 4;
            this.btnIAWConclusion.Text = "IAW Conclusion";
            this.btnIAWConclusion.UseVisualStyleBackColor = false;
            this.btnIAWConclusion.Click += new System.EventHandler(this.btnIAWConclusion_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(12, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(322, 65);
            this.button1.TabIndex = 6;
            this.button1.Text = "Popup check-in window from app (if not by itself), edit and start inspection.";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btnTagreaderMCA
            // 
            this.btnTagreaderMCA.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnTagreaderMCA.Location = new System.Drawing.Point(12, 367);
            this.btnTagreaderMCA.Name = "btnTagreaderMCA";
            this.btnTagreaderMCA.Size = new System.Drawing.Size(322, 65);
            this.btnTagreaderMCA.TabIndex = 7;
            this.btnTagreaderMCA.Text = "Tag Reader Manual Check";
            this.btnTagreaderMCA.UseVisualStyleBackColor = false;
            this.btnTagreaderMCA.Click += new System.EventHandler(this.btnTagreaderMCA_Click);
            // 
            // btnMCConclusion
            // 
            this.btnMCConclusion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnMCConclusion.Location = new System.Drawing.Point(12, 438);
            this.btnMCConclusion.Name = "btnMCConclusion";
            this.btnMCConclusion.Size = new System.Drawing.Size(322, 65);
            this.btnMCConclusion.TabIndex = 8;
            this.btnMCConclusion.Text = "MC Conclusion";
            this.btnMCConclusion.UseVisualStyleBackColor = false;
            this.btnMCConclusion.Click += new System.EventHandler(this.btnMCConclusion_Click);
            // 
            // btnCOLpr
            // 
            this.btnCOLpr.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnCOLpr.Location = new System.Drawing.Point(12, 580);
            this.btnCOLpr.Name = "btnCOLpr";
            this.btnCOLpr.Size = new System.Drawing.Size(322, 65);
            this.btnCOLpr.TabIndex = 9;
            this.btnCOLpr.Text = "LPR - COA";
            this.btnCOLpr.UseVisualStyleBackColor = false;
            this.btnCOLpr.Click += new System.EventHandler(this.btnCOLpr_Click);
            // 
            // btnOCRPreCheckout
            // 
            this.btnOCRPreCheckout.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOCRPreCheckout.Location = new System.Drawing.Point(12, 509);
            this.btnOCRPreCheckout.Name = "btnOCRPreCheckout";
            this.btnOCRPreCheckout.Size = new System.Drawing.Size(322, 65);
            this.btnOCRPreCheckout.TabIndex = 10;
            this.btnOCRPreCheckout.Text = "OCR - Pre Checkout Area";
            this.btnOCRPreCheckout.UseVisualStyleBackColor = false;
            this.btnOCRPreCheckout.Click += new System.EventHandler(this.btnOCRPreCheckout_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(346, 728);
            this.Controls.Add(this.btnOCRPreCheckout);
            this.Controls.Add(this.btnCOLpr);
            this.Controls.Add(this.btnMCConclusion);
            this.Controls.Add(this.btnTagreaderMCA);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnIAWConclusion);
            this.Controls.Add(this.btnLPRRadio);
            this.Controls.Add(this.btnLPRCheckin);
            this.Controls.Add(this.btnOCRPreCheckin);
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.Text = "Ashdod Customs Inspection Simulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOCRPreCheckin;
        private System.Windows.Forms.Button btnLPRCheckin;
        private System.Windows.Forms.Button btnLPRRadio;
        private System.Windows.Forms.Button btnIAWConclusion;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnTagreaderMCA;
        private System.Windows.Forms.Button btnMCConclusion;
        private System.Windows.Forms.Button btnCOLpr;
        private System.Windows.Forms.Button btnOCRPreCheckout;
    }
}

