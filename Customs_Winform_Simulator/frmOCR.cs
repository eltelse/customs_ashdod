﻿using EltelWebApi.DataAccess;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Customs_Winform_Simulator
{
    public partial class frmOCR : Form
    {
        AppSettings _settings;
        string _deviceIdentifier = "BIA1OCR1";
        public frmOCR(string deviceIdentifier = null)
        {
            InitializeComponent();
            _settings = Program.Configuration.GetSection("AppSettings").Get<AppSettings>();
            if (!string.IsNullOrEmpty(deviceIdentifier))
            {
                _deviceIdentifier = deviceIdentifier;
            }
        }

        private void btnSelectCCR_Click(object sender, EventArgs e)
        {
            selectFiles.ShowDialog();
            var files = selectFiles.FileNames;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn() { ColumnName = "Path", DataType = typeof(string) });
            dt.Columns.Add(new DataColumn() { ColumnName = "OwnerDeviceIdentifier", DataType = typeof(string) });

            for (int i = 0; i < files.Length; i++)
            {
                DataRow row = dt.NewRow();
                row.ItemArray = new object[] { files[i] };
                dt.Rows.Add(row);
            }

            dataGridViewCCR.DataSource = dt;


        }

        private void btnSelectLPR_Click(object sender, EventArgs e)
        {
            selectFiles.ShowDialog();
            var files = selectFiles.FileNames;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn() { ColumnName = "Path", DataType = typeof(string) });
            dt.Columns.Add(new DataColumn() { ColumnName = "OwnerDeviceIdentifier", DataType = typeof(string) });


            for (int i = 0; i < files.Length; i++)
            {
                DataRow row = dt.NewRow();
                row.ItemArray = new object[] { files[i] };
                dt.Rows.Add(row);
            }

            dataGridViewLPR.DataSource = dt;
        }

        private void frmOCR_Load(object sender, EventArgs e)
        {
            dataGridViewCCR.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);
            dataGridViewLPR.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);
            var deviceList = GetDeviceList();
            comboBoxDevices.DataSource = deviceList;
            comboBoxDevices.SelectedItem = deviceList.Find(d => d.name == $"{_deviceIdentifier} - OCR");
        }

        private void btnOCRDataSubmit_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            txtData.Text = "";
            dynamic de = new ExpandoObject();

            de.DeviceIdentifier = ((ListObj)comboBoxDevices.SelectedItem).name.Split(new char[] { ' ' }, StringSplitOptions.None)[0];
            de.LPR = txtLPR.Text;
            de.LPRConfidence = (int)numericUpDownLPR.Value;
            de.CCR = txtCCR.Text;
            de.CCRConfidence = (int)numericUpDownCCR.Value;
            de.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            
            dynamic filesObj = new ExpandoObject();
            filesObj.LPR = new List<ExpandoObject>();
            filesObj.CCR = new List<ExpandoObject>();
            de.Files = filesObj;

            for (int i = 0; i < dataGridViewLPR.Rows.Count; i++)
            {
                var row = dataGridViewLPR.Rows[i];

                string path = row.Cells["Path"].Value.ToString();
                string name = Path.GetFileName(path);
                string deviceOwner = null;
                deviceOwner = string.IsNullOrEmpty(row.Cells["OwnerDeviceIdentifier"].Value.ToString()) ? deviceOwner : row.Cells["OwnerDeviceIdentifier"].Value.ToString();

                dynamic fileObj = new ExpandoObject();
                fileObj.Name = name;
                fileObj.OwnerDeviceIdentifier = deviceOwner;
                fileObj.Path = path;

                de.Files.LPR.Add(fileObj);
            }

            for (int i = 0; i < dataGridViewCCR.Rows.Count; i++)
            {
                var row = dataGridViewCCR.Rows[i];

                string path = row.Cells["Path"].Value.ToString();
                string name = Path.GetFileName(path);
                string deviceOwner = null;
                deviceOwner = string.IsNullOrEmpty(row.Cells["OwnerDeviceIdentifier"].Value.ToString()) ? deviceOwner : row.Cells["OwnerDeviceIdentifier"].Value.ToString();

                dynamic fileObj = new ExpandoObject();
                fileObj.Name = name;
                fileObj.OwnerDeviceIdentifier = deviceOwner;
                fileObj.Path = path;

                de.Files.CCR.Add(fileObj);
            }

            var json = JsonConvert.SerializeObject(de);
            JToken parsedJson = JToken.Parse(json);
            var beautified = parsedJson.ToString(Formatting.Indented);
            txtData.Text += beautified + Environment.NewLine;
            lblStatus.Text = Utils.PostDeviceEvent(json);
        }

        private List<ListObj> GetDeviceEventTypes()
        {
            Dal d = new Dal(_settings.DB_CONECTION);
            string sql = @"select * from [dbo].[v_device_event_types] Order By [name]";
            var tbl = d.GetTable(sql);
            List<ListObj> resObj = DalHelper.DataTableToList<ListObj>(tbl);
            return resObj;
        }

        private List<ListObj> GetDeviceList()
        {

            Dal d = new Dal(_settings.DB_CONECTION);
            string sql = @"SELECT[Id] As id, [name] = [Name] + ' - ' + DeviceType, [description] = [Description] FROM [dbo].[v_devices] Order By[Name] +' - ' + DeviceType";
            var tbl = d.GetTable(sql);
            List<ListObj> resObj = DalHelper.DataTableToList<ListObj>(tbl);
            return resObj;
        }


    }
    class AuthResult
    {
        public string AccessToken { get; set; }
        public string ExpirationSeconds { get; set; }
    }
}
