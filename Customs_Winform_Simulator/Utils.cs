﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Customs_Winform_Simulator
{
    static class Utils
    {
        static AppSettings _settings = Program.Configuration.GetSection("AppSettings").Get<AppSettings>();

        public static string PostDeviceEvent(string json)
        {
            string res = "Success";
            try
            {
                string accessToken = GetJwt();
                string url = _settings.API_POST_URL;

                // See: https://stackoverflow.com/questions/52939211/the-ssl-connection-could-not-be-established
                HttpClientHandler ignorCertClientHandler = new HttpClientHandler();
                ignorCertClientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                
                using (var client = new HttpClient(ignorCertClientHandler))
                {
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
                    client.DefaultRequestHeaders.Add("Origin", "https://localhost:4200");
                    using (var request = new HttpRequestMessage(HttpMethod.Post, url))
                    {
                        var content = new StringContent(json);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        var response = client.PostAsync(url, content).Result;

                        response.EnsureSuccessStatusCode();

                        res = response.Content.ReadAsStringAsync().Result;

                    }

                }
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }

        public static string GetJwt()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(_settings.API_AUTH_URL);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            dynamic obj = new ExpandoObject();
            obj.client_id = "nuktec@nuktec.com";
            obj.client_secret = "Ia2VZps3aivggONVEeGx8fH*HDdlN901p";

            var json = JsonConvert.SerializeObject(obj);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            AuthResult resObj = JsonConvert.DeserializeObject<AuthResult>(result);
            return resObj.AccessToken;
        }

    }
}
